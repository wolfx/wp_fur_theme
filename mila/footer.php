<?php 

global $prefix;

?>

	</div> <!-- END #main-inner  -->

</section> <!-- END #main  -->

<footer>
    <div id="footer-inner" class="wrapper clearfix">
    	<a href="" id="totop" title="Back to top">Back To Top</a>
    </div>
</footer> <!-- END #footer -->

</div> <!-- END #page -->

<?php wp_footer(); ?>

</body>
</html>