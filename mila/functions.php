<?php 	


/*-----------------------------------------------------------------------------------

	Theme Setup

-----------------------------------------------------------------------------------*/
$prefix = '_sr'; 


/*-----------------------------------------------------------------------------------*/
/*	Set Max Content Width
/*-----------------------------------------------------------------------------------*/
if( ! isset( $content_width ) ) $content_width = 940;



/*-----------------------------------------------------------------------------------*/
/*	Theme Supports
/*-----------------------------------------------------------------------------------*/
add_theme_support( 'post-thumbnails' );
add_theme_support( 'automatic-feed-links' );
add_editor_style();
add_theme_support( 'custom-background' );

add_image_size( 'blog-thumb', 400);
add_image_size( 'portfolio-thumb', 400, 250, true );
add_image_size( 'fullwidth-thumb', 1000);



/*-----------------------------------------------------------------------------------*/
/*	Load Text Domain
/*-----------------------------------------------------------------------------------*/
load_theme_textdomain('sr_mila_theme', get_template_directory(). '/languages');



/*-----------------------------------------------------------------------------------*/
/*	Post Formats
/*-----------------------------------------------------------------------------------*/
$formats = array( 
			'aside',
			'audio',
			'gallery', 
			'image', 
			'link', 
			'quote', 
			'video');

add_theme_support( 'post-formats', $formats ); 



/*-----------------------------------------------------------------------------------*/
/*	Register Custom Menus 
/*-----------------------------------------------------------------------------------*/
if( !function_exists( 'sr_register_menu' ) ) {
    function sr_register_menu() {
		register_nav_menus(
			array(
				'primary-menu' => __('Primary Menu', 'sr_mila_theme'),
				'category-menu-portfolio' => __('Category Menu (Portfolio)', 'sr_mila_theme'),
				'category-menu-blog' => __('Category Menu (Blog)', 'sr_mila_theme')
			));	
    }
}
add_action('init', 'sr_register_menu');



/*-----------------------------------------------------------------------------------*/
/*	Register and Enqueue front-end scripts
/*-----------------------------------------------------------------------------------*/

if( !function_exists( 'sr_enqueue_scripts' ) ) {
    function sr_enqueue_scripts() {
		global $prefix;


		// Register scripts
		wp_register_script('modernizr', get_template_directory_uri() . '/files/js/jquery.modernizr.min.js', 'jquery', '1.0', false);
		wp_register_script('isotope', get_template_directory_uri() . '/files/js/jquery.isotope.min.js', 'jquery', '1.5.03', true);
		wp_register_script('flexslider', get_template_directory_uri() . '/files/js/jquery.flexslider-min.js', 'jquery', '2.1', true);
		wp_register_script('easing', get_template_directory_uri() . '/files/js/jquery.easing.1.3.js', 'jquery', '1.0', true);
		wp_register_script('easing-compatibility', get_template_directory_uri() . '/files/js/jquery.easing.compatibility.js', 'jquery', '1.0', true);
		wp_register_script('fancybox', get_template_directory_uri() . '/files/js/jquery.fancybox.pack.js', 'jquery', '1.3.4', true);
		wp_register_script('elegantcarousel', get_template_directory_uri() . '/files/js/jquery.elegantcarousel.min.js', 'jquery', '1.4', true);
		wp_register_script('jplayer', get_template_directory_uri() . '/files/jplayer/jquery.jplayer.min.js', 'jquery', '2.1.0', true);
		wp_register_script('form', get_template_directory_uri() . '/files/js/form.js', 'jquery', '1.0', true);
		wp_register_script('loader', get_template_directory_uri() . '/files/js/loader.js', 'jquery', '1.0', true);
		wp_register_script('script', get_template_directory_uri() . '/files/js/script.js', 'jquery', '1.0', true);
        wp_register_script('layer', get_template_directory_uri() . '/files/js/layer/layer.js', 'jquery', '1.0', true);
        wp_register_script('wolfx', get_template_directory_uri() . '/files/js/wolfx.js', 'jquery', '1.0', true);
		
		// Register style
		wp_register_style('default-style', get_stylesheet_uri() , 'default-style', '1.0');
		wp_register_style('dark-style', get_template_directory_uri() . '/files/css/dark.css' , 'dark-style', '1.0');
		wp_register_style('isotope-style', get_template_directory_uri() . '/files/css/isotope.css', 'isotope-style', '1.0');
		wp_register_style('flexslider-style', get_template_directory_uri() . '/files/css/flexslider.css', 'flexslider-style', '1.0');
		wp_register_style('fancybox-style', get_template_directory_uri() . '/files/css/fancybox.css', 'fancybox-style', '1.0');
		wp_register_style('jplayer-style', get_template_directory_uri() . '/files/jplayer/jplayer.css', 'jplayer-style', '1.0');
		wp_register_style('elegantcarousel-style', get_template_directory_uri() . '/files/css/elegantcarousel.css', 'elegantcarousel-style', '1.3');
		wp_register_style('mqueries-style', get_template_directory_uri() . '/files/css/mqueries.css', 'mqueries-style', '1.0');
        wp_register_style('layer-style', get_template_directory_uri() . '/files/js/layer/skin/layer.css', 'layer-style', '1.0');
         wp_register_style('wolfx-style', get_template_directory_uri() . '/files/css/wolfx.css', 'wolfx-style', '1.0');
		wp_register_style('custom-style', get_template_directory_uri() . '/custom-style.php', 'custom-style', '1.0');


		// Enqueue scripts
    	wp_enqueue_script('jquery');
    	wp_enqueue_script('modernizr');
        wp_enqueue_script('easing');
    	wp_enqueue_script('easing-compatibility');
    	wp_enqueue_script('fancybox');
    	wp_enqueue_script('elegantcarousel');
        wp_enqueue_script('layer');
        wp_enqueue_script('wolfx');
		
		// Enqueue styles
		wp_enqueue_style('default-style');
		wp_enqueue_style('fancybox-style');
		wp_enqueue_style('elegantcarousel-style');
        wp_enqueue_style('layer-style');
		wp_enqueue_style('wolfx-style');		
		
		// load scripts on appropriate pages
    	if( is_page_template( 'template-portfolio.php' ) || is_single() || is_home() || is_category()  || is_tag() || is_search() || is_archive() ) {
    	    wp_enqueue_script('flexslider'); 
			wp_enqueue_style('flexslider-style');
			wp_enqueue_script('jplayer'); 
			wp_enqueue_style('jplayer-style');
    	}
				
		// load scripts on appropriate pages
		wp_enqueue_script('isotope');
		wp_enqueue_style('isotope-style');
		
		wp_enqueue_script( 'form' );
		wp_enqueue_script( 'comment-reply' );
		
		//localize settings
			if ( is_page_template( 'template-gallery.php' ) ) { 
				if (get_option($prefix.'_gallerythumbnailsize') == '') { $itemwidth = 200;  } else { $itemwidth = get_option($prefix.'_gallerythumbnailsize'); }
			} else if ( is_page_template( 'template-portfolio.php' ) ) { 
				if (get_option($prefix.'_portfoliothumbnailsize') == '') { $itemwidth = 400;  } else { $itemwidth = get_option($prefix.'_portfoliothumbnailsize'); }
			} else { $itemwidth = 400; }
			if ( is_page_template( 'template-portfolio.php' ) || get_option($prefix.'_blogentriesdisplay') == 'featuredimage' ) { $transforms = 'true'; } else { $transforms = 'false'; }
			$contentwidth = 50; $asidewidth = 50;
			if ( get_option($prefix.'_contentwidth') == '65' ) { $contentwidth = 65; $asidewidth = 35; }
			else if ( get_option($prefix.'_contentwidth') == '75' ) { $contentwidth = 75; $asidewidth = 25; }
			
			$settings_vars = array(
				'ajaxurl' => admin_url('admin-ajax.php'),
				'transforms' => $transforms,			
				'width' => $itemwidth,				
				'contentwidth' => $contentwidth,				
				'asidewidth' => $asidewidth				
			);
			wp_localize_script( 'script', 'srvars', $settings_vars );
		
    	wp_enqueue_script('script');
    	
		// include ajax loader if true
		if (!get_option($prefix.'_ajax')) {
			wp_enqueue_script('loader');
		}
		
		// include dark style if true
		if (get_option($prefix.'_style') == 'dark') { 
			wp_enqueue_style('dark-style');
		}
		
		// include mqueries if true
		if (!get_option($prefix.'_disableresponsive')) { 
			wp_enqueue_style('mqueries-style');
		}
		
		wp_enqueue_style('custom-style');
		
    	
    }
    add_action('wp_enqueue_scripts', 'sr_enqueue_scripts');
}



/*-----------------------------------------------------------------------------------*/
/*	Include Theme Admin
/*-----------------------------------------------------------------------------------*/
// Adding Theme Admin
include("theme-admin/theme-admin.php");





/*-----------------------------------------------------------------------------------*/

/* Your Custom Functions
/* Place your custom functions below

/*-----------------------------------------------------------------------------------*/

function get_ssl_avatar($avatar) {
   $avatar = preg_replace('/.*\/avatar\/(.*)\?s=([\d]+)&.*/','<img src="https://secure.gravatar.com/avatar/$1?s=$2" class="avatar avatar-$2" height="$2" width="$2">',$avatar);
   return $avatar;
}
add_filter('get_avatar', 'get_ssl_avatar');



?>