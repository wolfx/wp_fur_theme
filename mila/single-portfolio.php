<?php

//get global prefix
global $prefix;

//get template header
get_header();

?>

                 
         <div id="content" class="maincontent">
         	<div class="content-inner">         	
        
             <?php   get_template_part( 'includes/singlepost', 'portfolio'); ?>
                
			</div>
         </div>


		<div id="portfolio" class="mainside">
            <div id="portfolio-grid" class="masonry clearfix">
            
                <?php
                
				/***********************
				QUERY PORTFOLIO ITEMS
				***********************/
				$sr_portfoliocount = get_option($prefix.'_portfoliocount');
					
				$query = new WP_Query(array(
					'posts_per_page'=> $sr_portfoliocount,
					'paged' => ( get_query_var('paged') ? get_query_var('paged') : 1 ),
					'portfolio_category' => get_query_var('portfolio_category'),
					'm' => get_query_var('m'),		   
					'w' => get_query_var('w'),
					'post_type' => array('portfolio')
				) );
				
                get_template_part( 'includes/loop', 'portfolio');
				wp_reset_postdata();
                ?>
                 
            </div>
            
            <?php $max_num_page = $query->max_num_pages; loadmore('portfolio', $max_num_page); ?>
            
         </div>
         
        <div class="mainside-bg"></div>


<?php get_footer(); ?>