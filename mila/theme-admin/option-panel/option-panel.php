<?php


/*-----------------------------------------------------------------------------------

	Option Page

-----------------------------------------------------------------------------------*/

$themename = "Mila";
global $prefix;



/*-----------------------------------------------------------------------------------*/
/*	Sections & Options
/*-----------------------------------------------------------------------------------*/
$sections = array (
	
	array( "name" => __("General", 'sr_mila_theme'),
		   "class" => "general",
		   "href" => "general"
		  ),
	
	array( "name" => __("Styling", 'sr_mila_theme'),
		   "class" => "styling",
		   "href" => "styling"
		  ),
	
	array( "name" => __("Blog", 'sr_mila_theme'),
		   "class" => "blog",
		   "href" => "blog"
		  ),
	
	array( "name" => __("Portfolio", 'sr_mila_theme'),
		   "class" => "portfolio",
		   "href" => "portfolio"
		  ),
	
	array( "name" => __("Typography", 'sr_mila_theme'),
		   "class" => "typography",
		   "href" => "typography"
		  ),
	
	array( "name" => __("Comments", 'sr_mila_theme'),
		   "class" => "comments",
		   "href" => "comments"
		  ),
	
	array( "name" => __("Spacings / Margins", 'sr_mila_theme'),
		   "class" => "spacings",
		   "href" => "spacings"
		  ),
	
	array( "name" => __("Responsive", 'sr_mila_theme'),
		   "class" => "responsive",
		   "href" => "responsive"
		  )
	
);

$options = array(
	
	array( "name" => __("General", 'sr_mila_theme'),
		   "id" => "general",
		   "type" => "sectionstart",
		   "desc" => ""
		  ),
			
			array( "label" => __("Branding", 'sr_mila_theme'),
				   "id" => $prefix."_general_branding",
				   "type" => "groupstart"
				  ),
	
				array( "label" => __("Logo", 'sr_mila_theme'),
					   "desc" => __("Add a Custom Logo from your Media Library", 'sr_mila_theme'),
					   "id" => $prefix."_logo",
					   "type" => "image"
					  ),
				
				array( "label" => __("Favicon", 'sr_mila_theme'),
					   "desc" => __("Add a 16px x 16px Png/Gif image that will represent your website's favicon.", 'sr_mila_theme'),
					   "id" => $prefix."_favicon",
					   "type" => "image"
					  ),
				
				array( "label" => __("Custom Login Logo", 'sr_mila_theme'),
					   "desc" => __("Add a custom logo for the Wordpress login screen.", 'sr_mila_theme'),
					   "id" => $prefix."_loginlogo",
					   "type" => "image"
					  ),
				
			array( "label" => "Branding END",
				   "id" => $prefix."_general_branding",
				   "type" => "groupend"
				  ),
			
			array( "label" => __("Footer", 'sr_mila_theme'),
				   "id" => $prefix."_general_footer",
				   "type" => "groupstart"
				  ),
			
				array( "label" => __("Back To Top", 'sr_mila_theme'),
					   "desc" => __("Display the back to top button", 'sr_mila_theme'),
					   "id" => $prefix."_disablebacktotop",
					   "type" => "checkbox",
					  ),
						
				array( "label" => __("Footer", 'sr_mila_theme').' <i>('.__("Copyright", 'sr_mila_theme').')</i>',
					   "desc" => __("Write your Copyright infos.", 'sr_mila_theme'),
					   "id" => $prefix."_footer",
					   "type" => "text"
					  ),
								
				array( "label" => __("Tracking Code", 'sr_mila_theme'),
					   "desc" => __("Paste your Google analytics (or other) code here.", 'sr_mila_theme'),
					   "id" => $prefix."_analytics",
					   "type" => "textarea"
					  ),
				
			array( "label" => "Footer END",
				   "id" => $prefix."_general_footer",
				   "type" => "groupend"
				  ),
	
	array ( "type" => "sectionend",
		   	"id" => "sectionend"),
	
	
	
	array( "name" => __("Styling", 'sr_mila_theme'),
		   "id" => "styling",
		   "type" => "sectionstart",
		   "desc" => ""
		  ),
		
			array( "label" => __("Layout", 'sr_mila_theme'),
				   "id" => $prefix."_styling_layout",
				   "type" => "groupstart"
				  ),
				
				array( "label" => __("Style", 'sr_mila_theme'),
					   "desc" => __("Choose your Style", 'sr_mila_theme'),
					   "id" => $prefix."_style",
					   "type" => "selectbox",
					   "option" => array( 
							array(	"name" => __("Light", 'sr_mila_theme'), 
									"value"=> "light"),		 
							array(	"name" => __("Dark", 'sr_mila_theme'), 
									"value" => "dark")
							),
					   "default" => "light"
					  ),
				
				array( "label" => __("Main Color", 'sr_mila_theme'),
					   "desc" => __("Please make sure that you use a correct color code with the hashtag '#' (Ex: #000000).", 'sr_mila_theme'),
					   "id" => $prefix."_customcolor",
					   "type" => "text",
					   "default" => "#00a276"
					  ),
				
				array( "label" => __("Text Color for Main Colored Background", 'sr_mila_theme'),
					   "desc" => __("Choose your color style for all content/objects that have your main color as background.", 'sr_mila_theme'),
					   "id" => $prefix."_maincolorstyle",
					   "type" => "selectbox",
					   "option" => array( 
							array(	"name" => __("Light", 'sr_mila_theme'), 
									"value"=> "light"),		 
							array(	"name" => __("Dark", 'sr_mila_theme'), 
									"value" => "dark")
							),
					   "default" => "light"
					  ),
								
				array( "label" => __("Direction for animation", 'sr_mila_theme'),
					   "desc" => __("Choose the direction for the animation.", 'sr_mila_theme'),
					   "id" => $prefix."_sidebar",
					   "type" => "radiocustom",
					   "option" => array( 
							array(	"name" => __("Right", 'sr_mila_theme'), 
									"value" => "sidebar_right"),
							array( 	"name"=> __("Left", 'sr_mila_theme'), 
									"value"=> "sidebar_left")
							)
					  ),
				
				array( "label" => __("Content width", 'sr_mila_theme'),
					   "desc" => __("Choose your content width", 'sr_mila_theme'),
					   "id" => $prefix."_contentwidth",
					   "type" => "selectbox",
					   "option" => array( 
							array(	"name" => __("Content (1/2) - Thumbnails (1/2)", 'sr_mila_theme'), 
									"value"=> "50"),		 
							array(	"name" => __("Content (2/3) - Thumbnails (1/3)", 'sr_mila_theme'), 
									"value"=> "65"),
							array(	"name" => __("Content (3/4) - Thumbnails (1/4)", 'sr_mila_theme'), 
									"value"=> "75")
							),
					   "default" => "50"
					  ),
				
				array( "label" => __("Fixed Footer Position", 'sr_mila_theme'),
					   "desc" => __("The Footer will be fixed.", 'sr_mila_theme'),
					   "id" => $prefix."_footerfixed",
					   "type" => "checkbox"
					  ),
				
				array( "label" => __("Smooth Loader (Ajax)", 'sr_mila_theme'),
					   "desc" => __("This loads the portfolio and blog post smoothly without redirecting the page.", 'sr_mila_theme'),
					   "id" => $prefix."_ajax",
					   "type" => "checkbox"
					  ),
				
				array( "label" => __("Page Preloader ", 'sr_mila_theme'),
					   "desc" => __("This adds a preloader animation for each page", 'sr_mila_theme'),
					   "id" => $prefix."_preloader",
					   "type" => "checkbox"
					  ),
							
			array( "label" => __("Layout", 'sr_mila_theme'),
				   "id" => $prefix."_styling_layoutr",
				   "type" => "groupend"
				  ),
			
			array( "label" => __("Header", 'sr_mila_theme'),
				   "id" => $prefix."_styling_header",
				   "type" => "groupstart"
				  ),
			
				array( "label" => __("Header Background Color", 'sr_mila_theme'),
					   "desc" => __("Please make sure that you use a correct color code with the hashtag '#' (Ex: #000000).", 'sr_mila_theme'),
					   "id" => $prefix."_headercolor",
					   "type" => "text",
					   "default" => "#00a276"
					  ),
			
				array( "label" => __("Navigation/Menu Color", 'sr_mila_theme'),
					   "desc" => __("Choose your color style for the main menu", 'sr_mila_theme'),
					   "id" => $prefix."_menustyle",
					   "type" => "selectbox",
					   "option" => array( 
							array(	"name" => __("Light", 'sr_mila_theme'), 
									"value"=> "light"),		 
							array(	"name" => __("Dark", 'sr_mila_theme'), 
									"value" => "dark")
							),
					   "default" => "light"
					  ),
			
			array( "label" => __("Header", 'sr_mila_theme'),
				   "id" => $prefix."_styling_header",
				   "type" => "groupend"
				  ),
			
			array( "label" => __("Gallery", 'sr_mila_theme'),
				   "id" => $prefix."_styling_gallery",
				   "type" => "groupstart"
				  ),
				
				array( "label" => __("Gallery Thumbnail size", 'sr_mila_theme'),
					   "desc" => __("Select a thumbnail size for the gallery items.", 'sr_mila_theme').'',
					   "id" => $prefix."_gallerythumbnailsize",
					   "type" => "selectbox",
					   "option" => array( 
							array(	"name" => __("Small", 'sr_mila_theme'), 
									"value"=> "200"),		 
							array(	"name" => __("Medium", 'sr_mila_theme'), 
									"value" => "300"),
							array(	"name" => __("Big", 'sr_mila_theme'), 
									"value" => "400")
							),
					   "default" => "200"
					  ),
			
			array( "label" => __("Gallery", 'sr_mila_theme'),
				   "id" => $prefix."_styling_gallery",
				   "type" => "groupend"
				  ),
			
			array( "label" => __("Custom", 'sr_mila_theme'),
				   "id" => $prefix."_styling_custom",
				   "type" => "groupstart"
				  ),
			
				array( "label" => __("Custom CSS", 'sr_mila_theme'),
					   "desc" => __("Write your own CSS-Code.", 'sr_mila_theme'),
					   "id" => $prefix."_customcss",
					   "type" => "textarea"
					  ),
			
			array( "label" => __("Custom", 'sr_mila_theme'),
				   "id" => $prefix."_styling_custom",
				   "type" => "groupend"
				  ),
			
				
	array ( "type" => "sectionend" ,
		   	"id" => "sectionend"),

	
	
	
	array( "name" => __("Blog", 'sr_mila_theme'),
		   "id" => "blog",
		   "type" => "sectionstart",
		   "desc" => ""
		  ),
	
			array( "label" => __("Entries", 'sr_mila_theme'),
				   "id" => $prefix."_blogentriesgroup",
				   "type" => "groupstart"
				  ),
			
				array( "label" => __("Filter", 'sr_mila_theme'),
				   	   "desc" => __("Display the blog filter. Please make sure to add a Category Menu (Blog).", 'sr_mila_theme'),
					   "id" => $prefix."_blogdisablefilter",
					   "type" => "checkbox"
					  ),
								
				array( "label" => __("Excerpt length", 'sr_mila_theme'),
					   "desc" => __("Add your own custom blog excerpt length.", 'sr_mila_theme'),
					   "id" => $prefix."_blogentriesexcerpt",
					   "type" => "text",
					   "default" => "20"
					  ),
				
				array( "label" => __("Read More Button", 'sr_mila_theme'),
					   "desc" => __("Enable or disable the read more button on blog entries.", 'sr_mila_theme'),
					   "id" => $prefix."_blogentriesreadmore",
					   "type" => "checkbox"
					  ),
				
				array( "label" => __("Display", 'sr_mila_theme'),
					   "desc" => __("Select a display option.", 'sr_mila_theme').'',
					   "id" => $prefix."_blogentriesdisplay",
					   "type" => "selectbox",
					   "option" => array( 
							array(	"name" => __("Post Format", 'sr_mila_theme'), 
									"value"=> "postformat"),		 
							array(	"name" => __("Featured Image", 'sr_mila_theme'), 
									"value" => "featuredimage")
							),
					   "default" => "postformat"
					  ),
				
				array( "label" => __("Style", 'sr_mila_theme'),
					   "desc" => __("Select <b>'portfolio'</b> and they will be showed like the portfolio thumbnails in masonry style (only thumbnail image without any other infos like title, date, etc.)", 'sr_mila_theme').'',
					   "id" => $prefix."_blogentriesstyle",
					   "type" => "selectbox",
					   "option" => array( 
							array(	"name" => __("Blog (Normal)", 'sr_mila_theme'), 
									"value"=> "blog"),		 
							array(	"name" => __("Portfolio", 'sr_mila_theme'), 
									"value" => "portfolio")
							),
					   "default" => "blog"
					  ),
												
			array( "label" => "Entries END",
				   "id" => $prefix."_blogentriesgroup",
				   "type" => "groupend"
				  ),
			
			array( "label" => __("Posts (Single)", 'sr_mila_theme'),
				   "id" => $prefix."_blogpostsgroup",
				   "type" => "groupstart"
				  ),
			
				array( "label" => __("Show Date", 'sr_mila_theme'),
					   "desc" => __("Shows the post Date.", 'sr_mila_theme'),
					   "id" => $prefix."_blogpostsdisabledate",
					   "type" => "checkbox"
					  ),
				
				array( "label" => __("Show Category", 'sr_mila_theme'),
					   "desc" => __("Shows the category.", 'sr_mila_theme'),
					   "id" => $prefix."_blogpostsdisablecategory",
					   "type" => "checkbox"
					  ),
				
				array( "label" => __("Show Views count", 'sr_mila_theme'),
					   "desc" => __("Activate and shows the views count for all posts.", 'sr_mila_theme'),
					   "id" => $prefix."_blogpostsdisableviews",
					   "type" => "checkbox"
					  ),
				
				array( "label" => __("Show Likes count", 'sr_mila_theme'),
					   "desc" => __("Activate and shows the likes count for all posts.", 'sr_mila_theme'),
					   "id" => $prefix."_blogpostsdisablelikes",
					   "type" => "checkbox"
					  ),
								
				array( "label" => __("Share", 'sr_mila_theme'),
					   "desc" => __("Activate the Share button for the posts. (Facebook, Twitter, Google, Pinterest).", 'sr_mila_theme'),
					   "id" => $prefix."_blogpostsdisableshare",
					   "type" => "checkbox"
					  ),
				
				array( "label" => __("Pagination", 'sr_mila_theme'),
					   "desc" => __("Activate the pagination Next/Previous for the Post detail page", 'sr_mila_theme'),
					   "id" => $prefix."_blogpostsdisablepagination",
					   "type" => "checkbox"
					  ),
																				
				array( "label" => __("Fancybox", 'sr_mila_theme'),
					   "desc" => __("Activate Fancybox for the gallery posts", 'sr_mila_theme'),
					   "id" => $prefix."_blogpostsdisablefancybox",
					   "type" => "checkbox"
					  ),
				
				array( "label" => __("Display Gallery format", 'sr_mila_theme'),
					   "desc" => __("If you choose 'list' the gallery pictures will be displayed underneath instead of a slider", 'sr_mila_theme'),
					   "id" => $prefix."_blogpostgallerydisplay",
					   "type" => "selectbox",
					   "option" => array( 
							array(	"name" => __("Slider", 'sr_mila_theme'), 
									"value"=> "slider"),
							array(	"name" => __("List", 'sr_mila_theme'), 
									"value"=> "list")
							),
					   "default" => "slider"
					  ),
									  
				array( "label" => __("Reset Views/Likes", 'sr_mila_theme'),
					   "desc" => "<b>".__("ATTENTION: This resets all views/likes to 0 for all posts.", 'sr_mila_theme')."</b>",
					   "id" => $prefix."_blogreset",
					   "type" => "reset"
					  ),
														
			array( "label" => "Posts END",
				   "id" => $prefix."_blogpostsgroup",
				   "type" => "groupend"
				  ),
	
	array ( "type" => "sectionend",
		   	"id" => "sectionend"),
	
	
	array( "name" => __("Portfolio", 'sr_mila_theme'),
		   "id" => "portfolio",
		   "type" => "sectionstart",
		   "desc" => ""
		  ),
	
			array( "label" => __("Portfolio Page", 'sr_mila_theme'),
				   "desc" => __("Please choose the portfolio page which uses the portfolio template.", 'sr_mila_theme'),
				   "id" => $prefix."_portfoliopage",
				   "type" => "pagelist"
				  ),
			
			array( "label" => __("Layout", 'sr_mila_theme'),
				   "id" => $prefix."_portfolio-layout",
				   "type" => "groupstart"
				  ),
						
				array( "label" => __("Crop Images", 'sr_mila_theme'),
				   	   "desc" => __("This crops the portfolio thumbnails to the same sizes.", 'sr_mila_theme'),
					   "id" => $prefix."_portfoliocrop",
					   "type" => "checkbox"
					  ),
				
				array( "label" => __("Count", 'sr_mila_theme'),
				   	   "desc" => __("Enter a valid number. Leave Empty if youw ant to show all items. <b>Enter -1 for show all items.</b>", 'sr_mila_theme'),
					   "id" => $prefix."_portfoliocount",
					   "type" => "text"
					  ),
				
				array( "label" => __("Portfolio Thumbnail size", 'sr_mila_theme'),
					   "desc" => __("Select a thumbnail size for the portfolio items.", 'sr_mila_theme').'',
					   "id" => $prefix."_portfoliothumbnailsize",
					   "type" => "selectbox",
					   "option" => array( 
							array(	"name" => __("Small", 'sr_mila_theme'), 
									"value"=> "200"),		 
							array(	"name" => __("Medium", 'sr_mila_theme'), 
									"value" => "300"),
							array(	"name" => __("Big", 'sr_mila_theme'), 
									"value" => "400"),
							),
					   "default" => "400"
					  ),
								
				array( "label" => __("Filter", 'sr_mila_theme'),
				   	   "desc" => __("Display the portfolio filter. Please make sure to add a Category Menu (Portfolio).", 'sr_mila_theme'),
					   "id" => $prefix."_portfoliodisablefilter",
					   "type" => "checkbox"
					  ),
				
			array( "label" => "Layout",
				   "id" => $prefix."_portfolio-layout",
				   "type" => "groupend"
				  ),
				
			array( "label" => __("Settings", 'sr_mila_theme'),
				   "id" => $prefix."_portfolio-layout",
				   "type" => "groupstart"
				  ),
				
				array( "label" => __("Share", 'sr_mila_theme'),
				   	   "desc" => __("Enable the share feature.", 'sr_mila_theme'),
					   "id" => $prefix."_portfoliodisableshare",
					   "type" => "checkbox"
					  ),
				
				array( "label" => __("Single Pagination", 'sr_mila_theme'),
				   	   "desc" => __("Enable the pagination between the portfolio items.", 'sr_mila_theme'),
					   "id" => $prefix."_portfoliodisablepagination",
					   "type" => "checkbox"
					  ),
								
				array( "label" => __("Fancybox", 'sr_mila_theme'),
				   	   "desc" => __("Enable the fancybox for bigger view.", 'sr_mila_theme'),
					   "id" => $prefix."_portfoliodisablefancybox",
					   "type" => "checkbox"
					  ),
				
				array( "label" => __("Display", 'sr_mila_theme'),
					   "desc" => __("If you choose 'list' the gallery pictures will be displayed underneath instead of a slider", 'sr_mila_theme'),
					   "id" => $prefix."_portfoliodisplay",
					   "type" => "selectbox",
					   "option" => array( 
							array(	"name" => __("Slider", 'sr_mila_theme'), 
									"value"=> "slider"),
							array(	"name" => __("List", 'sr_mila_theme'), 
									"value"=> "list")
							),
					   "default" => "slider"
					  ),
				
			array( "label" => "Layout",
				   "id" => $prefix."_portfolio-layout",
				   "type" => "groupend"
				  ),
				
	array ( "type" => "sectionend",
		   	"id" => "sectionend"),
	
	array( "name" => __("Typography", 'sr_mila_theme'),
		   "id" => "typography",
		   "type" => "sectionstart",
		   "desc" => ""
		  ),
	
			array( "label" => __("Body", 'sr_mila_theme'),
				   "id" => $prefix."_typography-body",
				   "type" => "groupstart"
				  ),
							
				array( "label" => __("Body Font", 'sr_mila_theme'),
				   	   "desc" => "",
					   "id" => $prefix."_bodyfont",
					   "type" => "fonts"
					  ),
				
				array( "label" => __("Body Font Size", 'sr_mila_theme'),
				   	   "desc" => __("Choose the body font size.", 'sr_mila_theme'),
					   "id" => $prefix."_bodyfontsize",
					   "type" => "font-size",
					   "default" => "13px"
					  ),
				
				array( "label" => __("Body Font Line-Height", 'sr_mila_theme'),
				   	   "desc" => __("Choose the body font line-height.", 'sr_mila_theme'),
					   "id" => $prefix."_bodyfontlineheight",
					   "type" => "font-lineheight",
					   "default" => "21px"
					  ),
			
			array( "label" => "Body",
				   "id" => $prefix."_typography-body",
				   "type" => "groupend"
				  ),
			
			array( "label" => __("Headings", 'sr_mila_theme'),
				   "id" => $prefix."_typography-headings",
				   "type" => "groupstart"
				  ),
				
				array( "label" => __("Heading Font", 'sr_mila_theme'),
				   	   "desc" => "",
					   "id" => $prefix."_headingfont",
					   "type" => "fonts"
					  ),
				
				array( "label" => __("Heading Font Weight", 'sr_mila_theme'),
				   	   "desc" => "",
					   "id" => $prefix."_headingfontweight",
					   "type" => "selectbox",
					   "option" => array( 
							array(	"name" => __("Normal", 'sr_mila_theme'), 
									"value" => "normal"),
							array(	"name" => __("Bold", 'sr_mila_theme'), 
									"value"=> "bold")
							),
					   "default" => "bold"
					  ),
				
			array( "label" => "Headings",
				   "id" => $prefix."_typography-headings",
				   "type" => "groupend"
				  ),
			
			array( "label" => __("Navigation", 'sr_mila_theme'),
				   "id" => $prefix."_typography-navigation",
				   "type" => "groupstart"
				  ),
				
				array( "label" => __("Navigation Font", 'sr_mila_theme'),
				   	   "desc" => "",
					   "id" => $prefix."_navigationfont",
					   "type" => "fonts"
					  ),
				
				array( "label" => __("Navigation Font Size", 'sr_mila_theme'),
				   	   "desc" => __("Choose the navigation font size.", 'sr_mila_theme'),
					   "id" => $prefix."_navigationfontsize",
					   "type" => "font-size",
					   "default" => "14px"
					  ),
				
				array( "label" => __("Navigation Font Weight", 'sr_mila_theme'),
				   	   "desc" => "",
					   "id" => $prefix."_navigationfontweight",
					   "type" => "selectbox",
					   "option" => array( 
							array(	"name" => __("Normal", 'sr_mila_theme'), 
									"value" => "normal"),
							array(	"name" => __("Bold", 'sr_mila_theme'), 
									"value"=> "bold")
							),
					   "default" => "bold"
					  ),
											
			array( "label" => "Navigation",
				   "id" => $prefix."_typography-navigation",
				   "type" => "groupend"
				  ),
	
	array ( "type" => "sectionend",
		   	"id" => "sectionend"),
	
	
	array( "name" => __("Comments", 'sr_mila_theme'),
		   "id" => "comments",
		   "type" => "sectionstart",
		   "desc" => ""
		  ),
						
				array( "label" => __("Blog Posts Comments", 'sr_mila_theme'),
				   	   "desc" => "Disable/Enable Comments On Blog Posts",
					   "id" => $prefix."_commentsblog",
					   "type" => "selectbox",
					   "option" => array( 
							array(	"name" => __("Enabled", 'sr_mila_theme'), 
									"value" => "enabled"),
							array(	"name" => __("Disabled", 'sr_mila_theme'), 
									"value"=> "disabled")
							),
					   "default" => "enabled"
					  ),
				
				array( "label" => __("Page Comments", 'sr_mila_theme'),
				   	   "desc" => "Disable/Enable Comments On Pages",
					   "id" => $prefix."_commentspage",
					   "type" => "selectbox",
					   "option" => array( 
							array(	"name" => __("Enabled", 'sr_mila_theme'), 
									"value" => "enabled"),
							array(	"name" => __("Disabled", 'sr_mila_theme'), 
									"value"=> "disabled")
							),
					   "default" => "disabled"
					  ),
				
				array( "label" => __("Portfolio Comments", 'sr_mila_theme'),
				   	   "desc" => "Disable/Enable Comments On Portfolio items",
					   "id" => $prefix."_commentsportfolio",
					   "type" => "selectbox",
					   "option" => array( 
							array(	"name" => __("Enabled", 'sr_mila_theme'), 
									"value" => "enabled"),
							array(	"name" => __("Disabled", 'sr_mila_theme'), 
									"value"=> "disabled")
							),
					   "default" => "disabled"
					  ),
				
	array ( "type" => "sectionend",
		   	"id" => "sectionend"),
		
	
	array( "name" => __("Spacings / Margins", 'sr_mila_theme'),
		   "id" => "spacings",
		   "type" => "sectionstart",
		   "desc" => ""
		  ),
			
			array( "label" => __("Header Spacings", 'sr_mila_theme'),
				   "id" => $prefix."spacings-header",
				   "type" => "groupstart"
				  ),
			
				array( "label" => __("Header Padding", 'sr_mila_theme'),
	   				   "desc" => __("Enter a Header padding for top and bottom.", 'sr_mila_theme').'<br><b>'.__("Default is", 'sr_mila_theme').' 30</b>',
					   "id" => $prefix."_spacingheader",
					   "type" => "text",
					   "default" => "30"
					  ),
				
				array( "label" => __("Menu Top", 'sr_mila_theme').' <i>'.__("(if enabled)", 'sr_mila_theme').'</i>',
	   				   "desc" => __("Enter a top margin for the Menu.", 'sr_mila_theme').'<br><b>'.__("Default is", 'sr_mila_theme').' 0</b>',
					   "id" => $prefix."_spacingmenu",
					   "type" => "text",
					   "default" => "0"
					  ),
			
			array( "label" => "Header Spacings",
				   "id" => $prefix."spacings-header",
				   "type" => "groupend"
				  ),
			
			
			array( "label" => __("Footer Spacings", 'sr_mila_theme'),
				   "id" => $prefix."spacings-sidebarwidegets",
				   "type" => "groupstart"
				  ),
			
				array( "label" => __("Footer Padding", 'sr_mila_theme'),
	   				   "desc" => __("Enter a Footer padding for top and bottom.", 'sr_mila_theme').'<br><b>'.__("Default is", 'sr_mila_theme').' 15</b>',
					   "id" => $prefix."_spacingfooter",
					   "type" => "text",
					   "default" => "15"
					  ),
							
			array( "label" => "Sidebar Widgets Spacings",
				   "id" => $prefix."spacings-sidebarwidegets",
				   "type" => "groupend"
				  ),
	
	array ( "type" => "sectionend",
		   	"id" => "sectionend"),
	
	
	
	array( "name" => __("Responsive", 'sr_mila_theme'),
		   "id" => "responsive",
		   "type" => "sectionstart",
		   "desc" => ""
		  ),
	
				array( "label" => __("Responsive Layout", 'sr_mila_theme'),
					   "desc" => __("Activate/Deactivate the responsive resize feature.", 'sr_mila_theme'),
					   "id" => $prefix."_disableresponsive",
					   "type" => "checkbox"
					  ),
					
	array ( "type" => "sectionend",
		   	"id" => "sectionend")
	
);




/*-----------------------------------------------------------------------------------*/
/*	Add Page/Subpage & generate save/reset
/*-----------------------------------------------------------------------------------*/

function sr_theme_add_admin() {
 
global $themename, $prefix, $options;
 
if ( isset($_GET['page']) && $_GET['page'] == basename(__FILE__) ) {
 
	if ( isset($_REQUEST['action'])  &&  $_REQUEST['action'] == 'save' ) {
 
				
		foreach ($options as $value) {
			if( isset( $_REQUEST[ $value['id'] ] ) ) { update_option( $value['id'], $_REQUEST[ $value['id'] ]  ); } else { delete_option( $value['id'] ); }
			
			if( isset(  $value['option'] ) && $value['option'] ) {
				foreach ($value['option'] as $var) {
					if ( isset(  $var['id']) ) {
						if ( isset( $_REQUEST[ $var['id'] ] ) ) { update_option( $var['id'], $_REQUEST[ $var['id'] ]  ); } else { delete_option( $var['id'] ); }
					}
				}
			}
		}
		header("Location: themes.php?page=option-panel.php&saved=true");
		die;
		 
	} 
	else if( isset($_REQUEST['action'])  &&  $_REQUEST['action'] == 'reset' ) {
		 
		foreach ($options as $value) {
			delete_option( $value['id'] ); 
			
			foreach ($value['option'] as $var) {
				delete_option( $var['id'] ); 
			}
		}
		 
		header("Location: themes.php?page=option-panel.php&reset=true");
		die;
	 
	}
}
 
add_theme_page($themename, 'Theme Options', 'administrator', basename(__FILE__), 'sr_theme_interface');
}

add_action('admin_menu', 'sr_theme_add_admin');




/*-----------------------------------------------------------------------------------*/
/*	Building interface
/*-----------------------------------------------------------------------------------*/
function sr_theme_interface() {
 
global $themename, $prefix, $options, $sections, $option_new;
$i=0;
 

echo '	<div class="sr_wrap">
		<form method="post">
		
		<div class="sr_header clear">
			<h1>'.$themename.'</h1> <div class="icon32" id="icon-options-general"></div>
			<input name="save" type="submit" value="Save all changes" class="submit-option button"/>
		</div>
		';
    
	if ( isset($_REQUEST['saved']) && $_REQUEST['saved'] != "") { echo '<div class="message_ok fade"><p><strong>'.$themename.' '.__("settings saved", 'sr_mila_theme').'.</strong></p></div>'; }
	if ( isset($_REQUEST['reset']) && $_REQUEST['reset'] != "") { echo '<div class="message_ok fade"><p><strong>'.$themename.' '.__("settings reset", 'sr_mila_theme').'.</strong></p></div>'; }
	if ( isset($_REQUEST['blogreset']) && $_REQUEST['blogreset'] != "") {
		echo '<div class="message_ok fade"><p><strong>'.$_REQUEST['blogreset'].' '.__("settings reset", 'sr_mila_theme').'.</strong></p></div>';
		if ($_REQUEST['blogreset'] == 'views') { sr_resetPostMeta('views'); }
		if ($_REQUEST['blogreset'] == 'likes') { sr_resetPostMeta('likes'); }
	}
	
	
	echo '<div id="sr_body" class="clear">';
    
	//  Add the sections
	echo '<ul id="section-list">';
	foreach ($sections as $section) {
	
		echo '<li class="'.$section['class'].'"><a href="'.$section['href'].'">'.$section['name'].'</a></li>';
	
	} 
	echo '</ul>';
	
	
	echo '<div class="section">';
	
	
	//  Add the options
	foreach ($options as $option) {
		
		$value = stripslashes(get_option( $option['id'])  );
		if ($value == '' && (isset($option['default']) && $option['default'] !== '')) { $value = $option['default']; }
		
		switch ( $option['type'] ) {
		
		//sectionstart
		case "sectionstart":
			echo '	<div id="'.$option['id'].'" class="section-content">';
			if ($option['desc'] && $option['desc'] !== '') { echo '<div class="section-desc"><i>'.$option['desc'].'</i></div>'; }
		break;
		
		
		//sectionend
		case "sectionend":
			echo '</div>';
		break;
		
		
		//groupstart
		case "groupstart":
			echo '<div id="'.$option['id'].'" class="optiongroup">';
			echo '<div class="optiongroup-title"><h4>'.$option['label'].'</h4></div>';
			echo '<div class="optiongroup-content">';
		break;
		
		
		//groupend
		case "groupend":
			echo '	</div>'; // END optiongroup-content
			echo '	</div>'; // END optiongroup
		break;
		
		
		// hidinggroupstart
		case "hidinggroupstart":
			echo '<div id="hiding'.$option['id'].'" class="hidinggroup '.$option['id'].'_'.$option['hiding'].'">';
		break;
		
		
		// hidinggroupend
		case "hidinggroupend":
			echo '	</div>'; // END hidinggroup
		break;
		
		
		//text
		case "text":
			echo '<div class="option clear">';
				echo '	<div class="option_name">
							<label for="'.$option['id'].'">'.$option['label'].'</label><br /><span class="option_desc"><i>'.$option['desc'].'</i></span>
						</div>';
				echo '	<div class="option_value">
							<input type="text" name="'.$option['id'].'" id="'.$option['id'].'" value="'.$value.'" size="30" />
						</div>';		
			echo '</div>';
		break;
		
		
		//textgroup
		case "textgroup":
			echo '<div class="option clear">';
				echo '	<div class="option_name">
							<label for="'.$option['id'].'">'.$option['label'].'</label><br /><span class="option_desc"><i>'.$option['desc'].'</i></span>
						</div>';
				echo '	<div class="option_value">';
				
				foreach ($option['option'] as $var) {
					$subvalue = stripslashes(get_option( $var['id'])  );
					echo '<textarea name="'.$var['id'].'" id="'.$var['id'].'" cols="20" rows="1"/>'.$subvalue.'</textarea><span class="sr_subdesc"><i>'.$var['desc'].'</i></span><br/>';  
				}

				echo '	</div>';		
			echo '</div>';
		break;
		
		
		//textarea
		case "textarea":
			echo '<div class="option clear">';
				echo '	<div class="option_name">
							<label for="'.$option['id'].'">'.$option['label'].'</label><br /><span class="option_desc"><i>'.$option['desc'].'</i></span>
						</div>';
				echo '	<div class="option_value">
							<textarea name="'.$option['id'].'" id="'.$option['id'].'" cols="40" rows="10">'.$value.'</textarea>
						</div>';		
			echo '</div>';
		break;
		
		
		//textareasmall
		case "textareasmall":
			echo '<div class="option clear">';
				echo '	<div class="option_name">
							<label for="'.$option['id'].'">'.$option['label'].'</label><br /><span class="option_desc"><i>'.$option['desc'].'</i></span>
						</div>';
				echo '	<div class="option_value">
							<textarea name="'.$option['id'].'" id="'.$option['id'].'" cols="40" rows="3">'.$value.'</textarea>
						</div>';		
			echo '</div>';
		break;
		
		
		//checkbox
		case 'checkbox':  
			echo '<div class="option clear">';
				echo '	<div class="option_name">
							<label for="'.$option['id'].'">'.$option['label'].'</label><br /><span class="option_desc"><i>'.$option['desc'].'</i></span>
						</div>';
				echo '	<div class="option_value">
							<input type="checkbox" style="display:none;" name="'.$option['id'].'" id="'.$option['id'].'" ',$value ? ' checked="checked"' : '','/>
							<a class="checkbox-status',$value ? '-active' : '','" href="" title="'.$option['id'].'">'.$option['id'].'</a>
						</div>';		
			echo '</div>';
		break;
		
		
		//radio
		case "radio":
			echo '<div class="option clear" id="sr_radio">';
				echo '	<div class="option_name">
							<label for="'.$option['id'].'">'.$option['label'].'</label><br /><span class="option_desc"><i>'.$option['desc'].'</i></span>
						</div>';
				echo '	<div class="option_value">';
				
				$i = 0;
				foreach ($option['option'] as $var) {
					if ($value == $var['value']) { $checked = 'checked="checked"'; } else { if ($value == '' && $i == 0) { $checked = 'checked="checked"'; } else { $checked = '';  } }
					echo '<div><input type="radio" name="'.$option['id'].'" id="'.$var['value'].'" value="'.$var['value'].'" '.$checked.' /> '.$var['name'].'</div>';
				$i++;	
				}

				echo '	</div>';		
			echo '</div>';
		break;
		
		
		// selectbox  
		case 'selectbox':  
			echo '<div class="option clear">';
				echo '	<div class="option_name">
							<label for="'.$option['id'].'">'.$option['label'].'</label><br /><span class="option_desc"><i>'.$option['desc'].'</i></span>
						</div>';
				echo '	<div class="option_value">';
				
				echo '<select name="'.$option['id'].'" id="'.$option['id'].'">';
				$i = 0;
				foreach ($option['option'] as $var) {
					if ($value == $var['value']) { $selected = 'selected="selected"'; } else { if ($value == '' && $i == 0) { $selected = 'selected="selected"'; } else { $selected = '';  } }
					echo '<option value="'.$var['value'].'" '.$selected.' /> '.$var['name'].'</div>';
				$i++;	
				}			  
				echo '</select>'; 
			echo '	</div>';		
			echo '</div>';
		break;
		
		
		// selectbox-hiding  
		case 'selectbox-hiding':  
			echo '<div class="option clear hiding'.$option['id'].'" id="hiding">';
				echo '	<div class="option_name">
							<label for="'.$option['id'].'">'.$option['label'].'</label><br /><span class="option_desc"><i>'.$option['desc'].'</i></span>
						</div>';
				echo '	<div class="option_value">';
				
				echo '<select name="'.$option['id'].'" id="'.$option['id'].'">';
				$i = 0;
				foreach ($option['option'] as $var) {
					if ($value == $var['value']) { $selected = 'selected="selected"'; } else { if ($value == '' && $i == 0) { $selected = 'selected="selected"'; } else { $selected = '';  } }
					echo '<option value="'.$var['value'].'" '.$selected.' /> '.$var['name'].'</div>';
				$i++;	
				}			  
				echo '</select>'; 
			echo '	</div>';		
			echo '</div>';
		break;
		
		
		// font-size  
		case 'font-size':  
			$fontsize = array('9px','10px','11px','12px','13px','14px','15px','16px','17px','18px','19px','20px','21px','22px','23px','24px','25px','26px','27px','28px','29px','30px' );
		
			echo '<div class="option clear">';
				echo '	<div class="option_name">
							<label for="'.$option['id'].'">'.$option['label'].'</label><br /><span class="option_desc"><i>'.$option['desc'].'</i></span>
						</div>';
				echo '	<div class="option_value">';
				
				echo '<select name="'.$option['id'].'" id="'.$option['id'].'">';
				$i = 0;
				foreach ($fontsize as $size) {
					if ($value == $size) { $selected = 'selected="selected"'; } else { $selected = '';  } 
					echo '<option value="'.$size.'" '.$selected.' /> '.$size.'</div>';
				$i++;	
				}			  
				echo '</select>'; 
			echo '	</div>';		
			echo '</div>';
		break;
		
		
		// font-lineheight  
		case 'font-lineheight':  
			$fontheight = array('14px','15px','16px','17px','18px','19px','20px','21px','22px','23px','24px','25px','26px','27px','28px','29px','30px','31px','32px','33px','34px','35px','36px','37px','38px','39px','40px','41px','42px','43px','44px','45px');
		
			echo '<div class="option clear">';
				echo '	<div class="option_name">
							<label for="'.$option['id'].'">'.$option['label'].'</label><br /><span class="option_desc"><i>'.$option['desc'].'</i></span>
						</div>';
				echo '	<div class="option_value">';
				
				echo '<select name="'.$option['id'].'" id="'.$option['id'].'">';
				$i = 0;
				foreach ($fontheight as $height) {
					if ($value == $height) { $selected = 'selected="selected"'; } else { $selected = '';  } 
					echo '<option value="'.$height.'" '.$selected.' /> '.$height.'</div>';
				$i++;	
				}			  
				echo '</select>'; 
			echo '	</div>';		
			echo '</div>';
		break;
		
		
		// fonts  
		case 'fonts': 
			
			$fonts = array('default','Amaranth','Andika','Arial','Armata','Arvo','Averia Serif Libre','Bree Serif','Cabin','Capriola','Comfortaa','Crete Round','Cuprum','Didact Gothic','Dosis','Droid Sans','Droid Serif','Enriqueta','Esteban','Francois One','Georgia','Gudea','Hammersmith One','Helvetica','Lato','Lucida Sans Unicode','Marmelad','Merriweather','Molengo','Nunito','Open Sans','Open Sans Condensed','PT Sans','PT Sans Caption','PT Sans Narrow','Puritan','Quando','Quicksand','Raleway','Salsa','Shanti','Share','Signika','Source Sans Pro','Tahoma','Telex','Times New Roman','Ubuntu','Ubuntu Condensed','Ubuntu Mono','Verdana','Volkhov','Yanone Kaffeesatz' );
		
			echo '<div class="option clear">';
				echo '	<div class="option_name">
							<label for="'.$option['id'].'">'.$option['label'].'</label><br /><span class="option_desc"><i>'.$option['desc'].'</i></span>
						</div>';
				echo '	<div class="option_value">';
				
				echo '<select name="'.$option['id'].'" id="'.$option['id'].'">';
				$i = 0;
				foreach ($fonts as $font) {
					if ($value == $font) { $selected = 'selected="selected"'; } else { $selected = '';  } 
					echo '<option value="'.$font.'" '.$selected.' /> '.$font.'</div>';
				$i++;	
				}			  
				echo '</select>'; 
			echo '	</div>';		
			echo '</div>';
		break;
		
		
		//radiocustom
		case "radiocustom":
			echo '<div class="option clear" id="sr_radiocustom">';
				echo '	<div class="option_name">
							<label for="'.$option['id'].'">'.$option['label'].'</label><br /><span class="option_desc"><i>'.$option['desc'].'</i></span>
						</div>';
				echo '	<div class="option_value">';
				
				$i = 0;
				foreach ($option['option'] as $var) {
					if ($value == $var['value']) { $checked = 'checked="checked"'; $active = "active"; } else { if ($value == '' && $i == 0) { $checked = 'checked="checked"'; $active = "active"; } else { $checked = ''; $active = ""; } }
					echo '<input type="radio" name="'.$option['id'].'" id="'.$var['value'].'" value="'.$var['value'].'" '.$checked.' />
					<a class="customradio '.$var['value'].' '.$active.'" href="'.$var['value'].'"><span>'.$var['name'].'</span></a>';
				$i++;	
				}

				echo '	</div>';		
			echo '</div>';
		break;
		
		
		// image  
		case 'image':  
			echo '<div class="option clear">';
				echo '	<div class="option_name">
							<label for="'.$option['id'].'">'.$option['label'].'</label><br /><span class="option_desc"><i>'.$option['desc'].'</i></span>
						</div>';
				echo '	<div class="option_value">
							<input class="upload_image" type="text" name="'.$option['id'].'" id="'.$option['id'].'" value="'.$value.'" size="30" />
							<input class="upload_image_button button" type="button" value="Add Image" /><br />
							<span class="preview_image"><img class="'.$option['id'].'"  src="'.$value.'" alt="" /></span>
						</div>';		
			echo '</div>';  
		break;
		
		
		// imagegroup  
		case 'imagegroup':  
			echo '<div class="option clear">';
				echo '	<div class="option_name">
							<label for="'.$option['id'].'">'.$option['label'].'</label><br /><span class="option_desc"><i>'.$option['desc'].'</i></span>
						</div>';
				echo '	<div class="option_value">
							<input class="add_image_button button" type="button" value="Add Slide" /><br />
							<textarea name="'.$option['id'].'" class="groupvalue" style="display:none;">'.$value.'</textarea><br />
							<ul id="imagegroup_preview" class="preview">';
						if ($value) {
							$value = substr($value, 3);
							$value = explode('~~~', $value);
					        foreach($value as $row) {
								$object = explode('|~|', $row);
								$id = $object[0];
								$caption = $object[1];
								$image = wp_get_attachment_image_src($id, 'full');
								echo '<li><a id="image-remove"  class="image-remove button" href="#" rel="'.$id.'">-</a><span class="image"><img src="'.$image[0].'"></span><textarea id="caption">'.$caption.'</textarea><textarea id="hidden-value" style="display:none;">~~~'.$id.'|~|'.$caption.'</textarea><a id="image-moveup"  class="button" href="#" rel="'.$id.'">&uarr;</a><a id="image-movedown"  class="button" href="#" rel="'.$id.'">&darr;</a></li>';
					        }  
					    } 		
				echo '			</ul>
						</div>';		
			echo '</div>';  
		break;
		
		
		// pagelist  
		case 'pagelist':  
			echo '<div class="option clear">';
				echo '	<div class="option_name">
							<label for="'.$option['id'].'">'.$option['label'].'</label><br /><span class="option_desc"><i>'.$option['desc'].'</i></span>
						</div>';
				echo '	<div class="option_value">
							<select name="'.$option['id'].'" id="'.$option['id'].'">';
							  $pages = get_pages(); 
							  foreach ( $pages as $page ) {
								if ($page->ID == $value) { $active = 'selected="selected"'; }  else { $active = ''; } 
								$option = '<option value="' . $page->ID . '" '.$active.'>';
								$option .= $page->post_title;
								$option .= '</option>';
								echo $option;
							  }
				echo '		</select>
						</div>';		
			echo '</div>';  
		break;
		
		
		// organize  
		case 'organize':  
			echo '<div class="option clear">';
				echo '	<div class="option_name">
							<label for="'.$option['id'].'">'.$option['label'].'</label><br /><span class="option_desc"><i>'.$option['desc'].'</i></span>
						</div>';
				echo '	<div class="option_value">';
					echo '<div class="organize-option">';
					
					if ($value) {
						echo '<ul id="sortable" class="organize-list">';
						$tempvalue = substr($value, 0, -3);
						$tempvalue = explode('|||', $tempvalue);
						foreach ($tempvalue as $var) {
							$varvalue = explode('|', $var);
							if ($varvalue[2] == 'active') { $active = 'active'; } else { $active = ''; }
							echo '	<li class="ui-state-default '.$active.'">'.$varvalue[0].'<a class="status" href="" title="'.$varvalue[0].'"></a><input type="checkbox" name="'.$varvalue[1].'"/></li>';
						}
						echo '</ul>';
						echo '<textarea name="'.$option['id'].'" class="organize-value" style="display:none;">'.$value.'</textarea><br />';
					} else {
						echo '<ul id="sortable" class="organize-list">';
						$valueoutput = '';
						$i = 0;
						foreach ($option['option'] as $var) {
							echo '	<li class="ui-state-default">'.$var['name'].'<a class="status" href="" title="'.$var['name'].'"></a><input type="checkbox" name="'.$var['value'].'"/></li>';
							$valueoutput .= $var['name'].'|'.$var['value'].'|nonactive|||';
						$i++;	
						}
						echo '</ul>';
						echo '<textarea name="'.$option['id'].'" class="organize-value" style="display:none;">'.$valueoutput.'</textarea><br />';
					}
					echo '</div>';
				echo ' 	</div>';		
			echo '</div>';  
		break;
		
		
		// reset
		case 'reset':  
			echo '<div class="option clear">';
				echo '	<div class="option_name">
							<label for="'.$option['id'].'">'.$option['label'].'</label><br /><span class="option_desc"><i>'.$option['desc'].'</i></span>
						</div>';
				echo '	<div class="option_value">';
				echo '	<a href="themes.php?page=option-panel.php&blogreset=views" class="button sr_button">'.__("Reset Views", 'sr_mila_theme').'</a>';
				echo '	<a href="themes.php?page=option-panel.php&blogreset=likes" class="button sr_button">'.__("Reset Likes", 'sr_mila_theme').'</a>';
				echo '	</div>';		
			echo '</div>';  
		break;
		
		
		// slider
		case 'slider':  
			echo '<div class="option clear">';
				echo '	<div class="option_name">
							<label for="'.$option['id'].'">'.$option['label'].'</label><br /><span class="option_desc"><i>'.$option['desc'].'</i></span>
						</div>';
				echo '	<div class="option_value">'; 
				echo '<select name="'.$option['id'].'" id="'.$option['id'].'">';
				$pages = get_posts( array( 'post_type' => 'slider' ) ); 
				foreach ( $pages as $page ) {
					if ($page->ID == $value) { $active = 'selected="selected"'; }  else { $active = ''; }
					echo '<option value="' . $page->ID . '" '.$active.'>'.$page->post_title.'</option>';
				}
				echo '</select>';
				echo '	</div>';		
			echo '</div>';	
		break;
		
		
		} // END switch ( $option['type'] ) {
	} // END foreach ($options_new as $option) {
	
	
	echo '</div>'; // END section-content
	echo '</div>'; // END section



echo '	
		<div class="sr_footer clear">
		<input name="save" type="submit" value="Save all changes" class="submit-option button"/> 
		<input type="hidden" name="action" value="save" />
		</form>
		<form method="post">
		<input name="reset" type="submit" value="Reset" class="reset-option button" />
		<input type="hidden" name="action" value="reset" />
		</form>
		</div>
		</div> ';
 

} // END function sr_theme_interface() {




/*-----------------------------------------------------------------------------------*/
/*	Register and Enqueue admin javascript
/*-----------------------------------------------------------------------------------*/

if( !function_exists( 'sr_admin_js' ) ) {
    function sr_admin_js($hook) {
		
		wp_enqueue_script('jquery-ui-core');
		wp_enqueue_script('jquery-ui-sortable');
		
		wp_register_script('admin-script', get_template_directory_uri() . '/theme-admin/option-panel/js/admin_script.js', 'jquery', '1.0', true);
		wp_enqueue_script('admin-script');
		
		wp_register_style('admin-style', get_template_directory_uri() . '/theme-admin/option-panel/css/admin.css');
		wp_enqueue_style('admin-style');
		
		wp_enqueue_script('media-upload');
		wp_enqueue_style('thickbox');
    }
    
    add_action('admin_enqueue_scripts','sr_admin_js',10,1);
}




/*-----------------------------------------------------------------------------------*/
/* Output Custom CSS from theme options
/*-----------------------------------------------------------------------------------*/

if ( !function_exists( 'sr_head_css' ) ) {
    function sr_head_css() {
        global $prefix;
        $output = '';
        $output = get_option($prefix.'_color');
    }

    add_action('wp_head', 'sr_head_css');
}
?>