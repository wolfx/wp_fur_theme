$(document).ready(function() {
						  

	$('.sc_list li:first-child a').addClass('active');
	$('.sc_option .option-content:first-child').show();
	
	
	$('.sc_list li a').click(function() {
	
		$('.sc_list li a').removeClass('active');
		$(this).addClass('active');
		
		var showoption = $(this).attr('href');
		$('.sc_option .option-content').hide();
		$('.sc_option #'+showoption).show();
		
		return (false);
	});
	
	
	
	
	/*
	* 	Custom radio
	*/
	$('.customradio').click(function() { 
		
		value = $(this).attr('href');
		parent = $(this).parent();
		
		$(parent).find(".customradio").removeClass('active');
		$(this).addClass('active');
		
		$(parent).find("input").removeAttr("checked");
		$(parent).find("#"+value).attr("checked", "checked");
		
		var form = $(parent).parents('form').attr('id');
		
		
		
		/* custom columns show/hide */
		if (form == 'form_columns') {
			$(parent).parents(".option-content").find('#sc_column_one').hide();
			$(parent).parents(".option-content").find('#sc_column_two').hide();
			$(parent).parents(".option-content").find('#sc_column_three').hide();
			$(parent).parents(".option-content").find('#sc_column_four').hide();
			
			if (value == 'layout_onehalf-onehalf' || value == 'layout_onethird-twothird' || value == 'layout_twothird-onethird') {
				$(parent).parents(".option-content").find('#sc_column_one').show();
				$(parent).parents(".option-content").find('#sc_column_two').show();
			} else if (value == 'layout_onethird-onethird-onethird' || value == 'layout_onehalf-onefourth-onefourth') {
				$(parent).parents(".option-content").find('#sc_column_one').show();
				$(parent).parents(".option-content").find('#sc_column_two').show();
				$(parent).parents(".option-content").find('#sc_column_three').show();
			} else if (value == 'layout_onefourth-onefourth-onefourth-onefourth') {
				$(parent).parents(".option-content").find('#sc_column_one').show();
				$(parent).parents(".option-content").find('#sc_column_two').show();
				$(parent).parents(".option-content").find('#sc_column_three').show();
				$(parent).parents(".option-content").find('#sc_column_four').show();
			}
		}
		
		
		
		/* team member show/hide */
		if (form == 'form_team') {
			$(parent).parents(".option-content").find('#sc_teamgroup_one').hide();
			$(parent).parents(".option-content").find('#sc_teamgroup_two').hide();
			$(parent).parents(".option-content").find('#sc_teamgroup_three').hide();
			$(parent).parents(".option-content").find('#sc_teamgroup_four').hide();
			
			if (value == 'layout_onehalf-onehalf') {
				$(parent).parents(".option-content").find('#sc_teamgroup_one').show();
				$(parent).parents(".option-content").find('#sc_teamgroup_two').show();
			} else if (value == 'layout_onethird-onethird-onethird') {
				$(parent).parents(".option-content").find('#sc_teamgroup_one').show();
				$(parent).parents(".option-content").find('#sc_teamgroup_two').show();
				$(parent).parents(".option-content").find('#sc_teamgroup_three').show();
			} else if (value == 'layout_onefourth-onefourth-onefourth-onefourth') {
				$(parent).parents(".option-content").find('#sc_teamgroup_one').show();
				$(parent).parents(".option-content").find('#sc_teamgroup_two').show();
				$(parent).parents(".option-content").find('#sc_teamgroup_three').show();
				$(parent).parents(".option-content").find('#sc_teamgroup_four').show();
			}
		}
		
		
		/* pricing tables show/hide */
		if (form == 'form_pricing') {
			$(parent).parents(".option-content").find('#sc_pricinggroup_one').hide();
			$(parent).parents(".option-content").find('#sc_pricinggroup_two').hide();
			$(parent).parents(".option-content").find('#sc_pricinggroup_three').hide();
			$(parent).parents(".option-content").find('#sc_pricinggroup_four').hide();
			
			if (value == 'layout_onehalf-onehalf') {
				$(parent).parents(".option-content").find('#sc_pricinggroup_one').show();
				$(parent).parents(".option-content").find('#sc_pricinggroup_two').show();
			} else if (value == 'layout_onethird-onethird-onethird') {
				$(parent).parents(".option-content").find('#sc_pricinggroup_one').show();
				$(parent).parents(".option-content").find('#sc_pricinggroup_two').show();
				$(parent).parents(".option-content").find('#sc_pricinggroup_three').show();
			} else if (value == 'layout_onefourth-onefourth-onefourth-onefourth') {
				$(parent).parents(".option-content").find('#sc_pricinggroup_one').show();
				$(parent).parents(".option-content").find('#sc_pricinggroup_two').show();
				$(parent).parents(".option-content").find('#sc_pricinggroup_three').show();
				$(parent).parents(".option-content").find('#sc_pricinggroup_four').show();
			}
		}
		
		
		return false;
		
	
	});
	
	
	
	
	/*
	* 	DUPLICATER
	*/
		
	$('.groupduplicater').click(function() {
		
		var group = $(this).attr('href');
		var parent = $(this).parent('form');
		var groupcontent = $(this).parent('form').find('.group:first').html();
		
		$(this).before('<div id="'+group+'" class="group">'+groupcontent+'</div>');
		
		return false;
	});
	
	
	
	
	/*
	* 	Submit
	*/
	$('.submit').click(function() {
		var check = true;
		
		// ---------------------- CONTROL COLUMNS
		if ($(this).attr('id') == 'insert_columns') {
			var layout = $(this).parent('form').find('input[name="sc_columnlayout"]:checked').val();
			if (layout) {   } else { alert('Please Choose a column layout'); check = false; } 
		}
		// ---------------------- CONTROL COLUMNS
		
		
		// ---------------------- CONTROL TEAM
		if ($(this).attr('id') == 'insert_team') {
			var layout = $(this).parent('form').find('input[name="sc_teamlayout"]:checked').val();
			if (layout) {   } else { alert('Please Choose a Team layout'); check = false; } 
		}
		// ---------------------- CONTROL TEAM
		
		
		// ---------------------- CONTROL PRICING
		if ($(this).attr('id') == 'insert_pricing') {
			var layout = $(this).parent('form').find('input[name="sc_pricinglayout"]:checked').val();
			if (layout) {   } else { alert('Please Choose a Pricing layout'); check = false; } 
		}
		// ---------------------- CONTROL TEAM
		
		
		// ---------------------- CONTROL CONTACT
		if ($(this).attr('id') == 'insert_contact') {
			var mail = $(this).parent('form').find('input#sc_contactsendto').val();
			if (mail) {   } else { alert('Please enter a recipient email'); check = false; } 
		}
		// ---------------------- CONTROL CONTACT
				
		
		if (check) {
			ButtonDialog.insert(ButtonDialog.local_ed, $(this).attr('id'));
		}
		
		return false;
	});
	

});



	
var ButtonDialog = {
	local_ed : 'ed',
	init : function(ed) {
		ButtonDialog.local_ed = ed;
		tinyMCEPopup.resizeToInnerSize();
	},
	insert : function insertColumns(ed, id) {
	 	
		// Try and remove existing style / blockquote
		tinyMCEPopup.execCommand('mceRemoveNode', false, null);
		 
		var outputbefore = ''; 
		var output = ''; 
		
		// ---------------------- COLUMNS
		if (id == 'insert_columns') {
			
			var layout = $('#'+id).parent('form').find('input[name="sc_columnlayout"]:checked').val();
			var text_one = $('#'+id).parent('form').find('textarea#sc_column_one').val();
			var text_two = $('#'+id).parent('form').find('textarea#sc_column_two').val();
			var text_three = $('#'+id).parent('form').find('textarea#sc_column_three').val();
			var text_four = $('#'+id).parent('form').find('textarea#sc_column_four').val();
						
			if (layout == 'layout_onehalf-onehalf') {
				output = '[one_half]'+text_one+'[/one_half]';	
				output += '[one_half_last]'+text_two+'[/one_half_last]';	
			} else if (layout == 'layout_twothird-onethird') {
				output = '[two_third]'+text_one+'[/two_third]';	
				output += '[one_third_last]'+text_two+'[/one_third_last]';	
			} else if (layout == 'layout_onethird-twothird') {
				output = '[one_third]'+text_one+'[/one_third]';	
				output += '[two_third_last]'+text_two+'[/two_third_last]';	
			} else if (layout == 'layout_onethird-onethird-onethird') {
				output = '[one_third]'+text_one+'[/one_third]';	
				output += '[one_third]'+text_two+'[/one_third]';	
				output += '[one_third_last]'+text_three+'[/one_third_last]';	
			} else if (layout == 'layout_onehalf-onefourth-onefourth') {
				output = '[one_half]'+text_one+'[/one_half]';	
				output += '[one_fourth]'+text_two+'[/one_fourth]';	
				output += '[one_fourth_last]'+text_three+'[/one_fourth_last]';	
			} else if (layout == 'layout_onefourth-onefourth-onefourth-onefourth') {
				output = '[one_fourth]'+text_one+'[/one_fourth]';	
				output += '[one_fourth]'+text_two+'[/one_fourth]';	
				output += '[one_fourth]'+text_three+'[/one_fourth]';	
				output += '[one_fourth_last]'+text_four+'[/one_fourth_last]';	
			}
			
		}
		// ---------------------- COLUMNS
		
		
		
		
		// ---------------------- SKILLS
		if (id == 'insert_skills') {
			
			$('#'+id).parent('form').find('.group').each(function(index) {					
					var skill_percent = jQuery(this).find('input#sc_skillpercent').val();
					var skill_name = jQuery(this).find('input#sc_skillname').val();
					output += '[skill amount="'+skill_percent+'" name="'+skill_name+'"]';
			});
			
		}
		// ---------------------- SKILLS
		
		
		
		
		// ---------------------- CONTACT
		if (id == 'insert_contact') {
			
			var fields = '';
			
			$('#'+id).parent('form').find('.group').each(function(index) {					
					
					var fieldtype = $(this).find('select#sc_contacttype').val();
					var fieldname = $(this).find('input#sc_contactname').val();
					var slugname = $(this).find('input#sc_contactslug').val();
					if (slugname == '') { slugname = fieldname.toLowerCase(); slugname = slugname.replace(' ',''); } 
					var required = $(this).find('select#sc_contactreq').val();
					
					output += '[field type='+fieldtype+' name="'+fieldname+'" slug='+slugname+' required='+required+']';
					if (fieldtype !== 'submitbutton') { fields += slugname+','; }
					
			});
			
			var email =  $('#'+id).parent('form').find('input#sc_contactsendto').val();
			var subject =  $('#'+id).parent('form').find('input#sc_contactsubject').val();
			var submitname = $('#'+id).parent('form').find('input#sc_contactsubmit').val();
			
			output = '[contactgroup fields='+fields+' email='+email+' subject="'+subject+'" submit="'+submitname+'"]'+output+'[/contactgroup]';
		}
		// ---------------------- CONTACT
		
		
		
		// ---------------------- BUTTONS
		if (id == 'insert_buttons') {
			
			var color = $('#'+id).parent('form').find('select#sc_buttoncolor').val();
			var url = $('#'+id).parent('form').find('input#sc_buttonurl').val();
			var text = $('#'+id).parent('form').find('input#sc_buttontext').val();
			
			output = '[button color='+color+' url="'+url+'"]'+text+'[/button]';
			
		}
		// ---------------------- BUTTONS
		
		
		
		// ---------------------- ALERT
		if (id == 'insert_alert') {
			
			var color = $('#'+id).parent('form').find('select#sc_alertcolor').val();
			var text = $('#'+id).parent('form').find('textarea#sc_alerttext').val();
			
			output = '[alert color='+color+']'+text+'[/alert]';
			
		}
		// ---------------------- ALERT
		
		
		
		// ---------------------- TOGGLE
		if (id == 'insert_toggle') {
			
			var title = $('#'+id).parent('form').find('input#sc_toggletitle').val();
			var text = $('#'+id).parent('form').find('textarea#sc_toggletext').val();
			output = '[toggle title="'+title+'"]'+text+'[/toggle]';
			
		}
		// ---------------------- TOGGLE
		
		
		// ---------------------- TABS
		if (id == 'insert_tab') {
			
			$('#'+id).parent('form').find('.group').each(function(index) {					
					var tab_name = jQuery(this).find('input#sc_tabname').val();
					var tab_text = jQuery(this).find('textarea#sc_tabtext').val();
					outputbefore += tab_name+',';
					output += '[tab id="'+(index+1)+'"]'+tab_text+'[/tab]';
			});
			
			output = '[tabs title="'+outputbefore+'"]'+output+'[/tabs]';
		}
		// ---------------------- TABS
		
		
		// ---------------------- ACCORDION
		if (id == 'insert_accordion') {
			
			$('#'+id).parent('form').find('.group').each(function(index) {					
					var accordion_name = jQuery(this).find('input#sc_accordiontitle').val();
					var accordion_text = jQuery(this).find('textarea#sc_accordiontext').val();
					output += '[accordion title="'+accordion_name+'"]'+accordion_text+'[/accordion]';
			});
			
			output = '[accordiongroup]'+output+'[/accordiongroup]';
		}
		// ---------------------- ACCORDION
		
		
		
		// ---------------------- GALLERY
		if (id == 'insert_gallery') {
			
			var gal_id = $('#'+id).parent('form').find('select#sc_galleryid').val();
			var crop = $('#'+id).parent('form').find('select#sc_gallerycrop').val();
			
			output = '[sr_customgallery id="'+gal_id+'" crop="'+crop+'"]';
			
		}
		// ---------------------- GALLERY
		
		
		
		// ---------------------- SEPERATOR
		if (id == 'insert_seperator') {
			
			var title = $('#'+id).parent('form').find('input#sc_seperatortitle').val();
			
			if (title !== "") {
				output = '[seperator title="'+title+'"]';
			} else {
				output = '[seperator]';
			}
			
		}
		// ---------------------- SEPERATOR
		
		
		
		// ---------------------- PAGEDESCRIPTION
		if (id == 'insert_pagedesc') {
			
			var text = $('#'+id).parent('form').find('textarea#sc_pagedesc').val();
			output = '[pagedescription]'+text+'[/pagedescription]';
			
		}
		// ---------------------- PAGEDESCRIPTION
		
		
		
		// ---------------------- TEAM
		if (id == 'insert_team') {
			
			var layout = $('#'+id).parent('form').find('input[name="sc_teamlayout"]:checked').val();
			
			var img_one = $('#'+id).parent('form').find('input#sc_teamimage_one').val();
			var name_one = $('#'+id).parent('form').find('input#sc_teamname_one').val();
			var title_one = $('#'+id).parent('form').find('input#sc_teamtitle_one').val();
			var text_one = $('#'+id).parent('form').find('textarea#sc_teamtext_one').val();
			
			var img_two = $('#'+id).parent('form').find('input#sc_teamimage_two').val();
			var name_two = $('#'+id).parent('form').find('input#sc_teamname_two').val();
			var title_two = $('#'+id).parent('form').find('input#sc_teamtitle_two').val();
			var text_two = $('#'+id).parent('form').find('textarea#sc_teamtext_two').val();
			
			var img_three = $('#'+id).parent('form').find('input#sc_teamimage_three').val();
			var name_three = $('#'+id).parent('form').find('input#sc_teamname_three').val();
			var title_three = $('#'+id).parent('form').find('input#sc_teamtitle_three').val();
			var text_three = $('#'+id).parent('form').find('textarea#sc_teamtext_three').val();
			
			var img_four = $('#'+id).parent('form').find('input#sc_teamimage_four').val();
			var name_four = $('#'+id).parent('form').find('input#sc_teamname_four').val();
			var title_four = $('#'+id).parent('form').find('input#sc_teamtitle_four').val();
			var text_four = $('#'+id).parent('form').find('textarea#sc_teamtext_four').val();
			
						
			if (layout == 'layout_onehalf-onehalf') {
				output = '[team_one_half name="'+name_one+'" title="'+title_one+'" img="'+img_one+'"]'+text_one+'[/team_one_half]';	
				output += '[team_one_half_last name="'+name_two+'" title="'+title_two+'" img="'+img_two+'"]'+text_two+'[/team_one_half_last]';	
			} else if (layout == 'layout_onethird-onethird-onethird') {
				output = '[team_one_third name="'+name_one+'" title="'+title_one+'" img="'+img_one+'"]'+text_one+'[/team_one_third]';	
				output += '[team_one_third name="'+name_two+'" title="'+title_two+'" img="'+img_two+'"]'+text_two+'[/team_one_third]';	
				output += '[team_one_third_last name="'+name_three+'" title="'+title_three+'" img="'+img_three+'"]'+text_three+'[/team_one_third_last]';	
			} else if (layout == 'layout_onefourth-onefourth-onefourth-onefourth') {
				output = '[team_one_fourth name="'+name_one+'" title="'+title_one+'" img="'+img_one+'"]'+text_one+'[/team_one_fourth]';	
				output += '[team_one_fourth name="'+name_two+'" title="'+title_two+'" img="'+img_two+'"]'+text_two+'[/team_one_fourth]';	
				output += '[team_one_fourth name="'+name_three+'" title="'+title_three+'" img="'+img_three+'"]'+text_three+'[/team_one_fourth]';	
				output += '[team_one_fourth_last name="'+name_four+'" title="'+title_four+'" img="'+img_four+'"]'+text_four+'[/team_one_fourth_last]';	
			}
			
			
		}
		// ---------------------- TEAM
		
		
		
		// ---------------------- PRICING
		if (id == 'insert_pricing') {
			
			var layout = $('#'+id).parent('form').find('input[name="sc_pricinglayout"]:checked').val();
			
			var type_one = $('#'+id).parent('form').find('select#sc_pricingtype_one').val();
			var title_one = $('#'+id).parent('form').find('input#sc_pricingtitle_one').val();
			var price_one = $('#'+id).parent('form').find('input#sc_pricingprice_one').val();
			var time_one = $('#'+id).parent('form').find('input#sc_pricingtime_one').val();
			var listing_one = $('#'+id).parent('form').find('textarea#sc_pricinglisting_one').val();
			var url_one = $('#'+id).parent('form').find('input#sc_pricingurl_one').val();
			
			var type_two = $('#'+id).parent('form').find('select#sc_pricingtype_two').val();
			var title_two = $('#'+id).parent('form').find('input#sc_pricingtitle_two').val();
			var price_two = $('#'+id).parent('form').find('input#sc_pricingprice_two').val();
			var time_two = $('#'+id).parent('form').find('input#sc_pricingtime_two').val();
			var listing_two = $('#'+id).parent('form').find('textarea#sc_pricinglisting_two').val();
			var url_two = $('#'+id).parent('form').find('input#sc_pricingurl_Two').val();
			
			var type_three = $('#'+id).parent('form').find('select#sc_pricingtype_three').val();
			var title_three = $('#'+id).parent('form').find('input#sc_pricingtitle_three').val();
			var price_three = $('#'+id).parent('form').find('input#sc_pricingprice_three').val();
			var time_three = $('#'+id).parent('form').find('input#sc_pricingtime_three').val();
			var listing_three = $('#'+id).parent('form').find('textarea#sc_pricinglisting_three').val();
			var url_three = $('#'+id).parent('form').find('input#sc_pricingurl_three').val();
			
			var type_four = $('#'+id).parent('form').find('select#sc_pricingtype_four').val();
			var title_four = $('#'+id).parent('form').find('input#sc_pricingtitle_four').val();
			var price_four = $('#'+id).parent('form').find('input#sc_pricingprice_four').val();
			var time_four = $('#'+id).parent('form').find('input#sc_pricingtime_four').val();
			var listing_four = $('#'+id).parent('form').find('textarea#sc_pricinglisting_four').val();
			var url_four = $('#'+id).parent('form').find('input#sc_pricingurl_four').val();
			
						
			if (layout == 'layout_onehalf-onehalf') {
				output = '[pricingwrapper columns="2"]';
				output += '[pricing title="'+title_one+'" type="'+type_one+'" price="'+price_one+'" time="'+time_one+'" url="'+url_one+'"]'+listing_one+'[/pricing]';	
				output += '[pricing_last title="'+title_two+'" type="'+type_two+'" price="'+price_two+'" time="'+time_two+'" url="'+url_two+'"]'+listing_two+'[/pricing_last]';	
				output += '[/pricingwrapper]';
			} else if (layout == 'layout_onethird-onethird-onethird') {
				output = '[pricingwrapper columns="3"]';
				output += '[pricing title="'+title_one+'" type="'+type_one+'" price="'+price_one+'" time="'+time_one+'" url="'+url_one+'"]'+listing_one+'[/pricing]';	
				output += '[pricing title="'+title_two+'" type="'+type_two+'" price="'+price_two+'" time="'+time_two+'" url="'+url_two+'"]'+listing_two+'[/pricing]';	
				output += '[pricing_last title="'+title_three+'" type="'+type_three+'" price="'+price_three+'" time="'+time_three+'" url="'+url_three+'"]'+listing_three+'[/pricing_last]';	
				output += '[/pricingwrapper]';	
			} else if (layout == 'layout_onefourth-onefourth-onefourth-onefourth') {
				output = '[pricingwrapper columns="4"]';
				output += '[pricing title="'+title_one+'" type="'+type_one+'" price="'+price_one+'" time="'+time_one+'" url="'+url_one+'"]'+listing_one+'[/pricing]';	
				output += '[pricing title="'+title_two+'" type="'+type_two+'" price="'+price_two+'" time="'+time_two+'" url="'+url_two+'"]'+listing_two+'[/pricing]';	
				output += '[pricing title="'+title_three+'" type="'+type_three+'" price="'+price_three+'" time="'+time_three+'" url="'+url_three+'"]'+listing_three+'[/pricing]';	
				output += '[pricing_last title="'+title_four+'" type="'+type_four+'" price="'+price_four+'" time="'+time_four+'" url="'+url_four+'"]'+listing_four+'[/pricing_last]';	
				output += '[/pricingwrapper]';	
			}
			
			
		}
		// ---------------------- PRICING
		
		
		
		tinyMCEPopup.execCommand('mceReplaceContent', false, output);
		// Return
		tinyMCEPopup.close();
	}
};
tinyMCEPopup.onInit.add(ButtonDialog.init, ButtonDialog);
