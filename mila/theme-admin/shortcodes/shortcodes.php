<?php

 
/*-----------------------------------------------------------------------------------*/
/*	Shortcodes for Columns
/*-----------------------------------------------------------------------------------*/
function sr_one_half( $atts, $content = null ) {
   return '<div class="column one_half">' . preg_replace('#^<\/p>|<p>$#', '', do_shortcode($content)) . '</div>';
}
add_shortcode('one_half', 'sr_one_half');

function sr_one_half_last( $atts, $content = null ) {
   return '<div class="column one_half last">' . preg_replace('#^<\/p>|<p>$#', '', do_shortcode($content)) . '</div><div class="clear"></div>';
}
add_shortcode('one_half_last', 'sr_one_half_last');

function sr_one_third( $atts, $content = null ) {
   return '<div class="column one_third">' . preg_replace('#^<\/p>|<p>$#', '', do_shortcode($content)) . '</div>';
}
add_shortcode('one_third', 'sr_one_third');

function sr_one_third_last( $atts, $content = null ) {
   return '<div class="column one_third last">' . preg_replace('#^<\/p>|<p>$#', '', do_shortcode($content)) . '</div><div class="clear"></div>';
}
add_shortcode('one_third_last', 'sr_one_third_last');

function sr_two_third( $atts, $content = null ) {
   return '<div class="column two_third">' . preg_replace('#^<\/p>|<p>$#', '', do_shortcode($content)) . '</div>';
}
add_shortcode('two_third', 'sr_two_third');

function sr_two_third_last( $atts, $content = null ) {
   return '<div class="column two_third last">' . preg_replace('#^<\/p>|<p>$#', '', do_shortcode($content)) . '</div><div class="clear"></div>';
}
add_shortcode('two_third_last', 'sr_two_third_last');

function sr_one_fourth( $atts, $content = null ) {
   return '<div class="column one_fourth">' . preg_replace('#^<\/p>|<p>$#', '', do_shortcode($content)) . '</div>';
}
add_shortcode('one_fourth', 'sr_one_fourth');

function sr_one_fourth_last( $atts, $content = null ) {
   return '<div class="column one_fourth last">' . preg_replace('#^<\/p>|<p>$#', '', do_shortcode($content)) . '</div><div class="clear"></div>';
}
add_shortcode('one_fourth_last', 'sr_one_fourth_last');




/*-----------------------------------------------------------------------------------*/
/*	Shortcodes for Team
/*-----------------------------------------------------------------------------------*/
function sr_team_one_half( $atts, $content = null ) {
	
	extract( shortcode_atts( array(
      'name' => 'MemberName',
	  'title' => 'MemberTitle',
	  'img' => ''
      ), $atts ) );
	
   $output = '<div class="column one_half team-member">';
   if ($img) { $output .= '<img src="'.$img.'" alt="Teammember '.$name.'"/>'; }
   $output .= '<div class="team-meta"><h5><strong>'.$name.'</strong></h5><span class="team-title"><i>'.$title.'</i></span></div>';
   $output .= '<div class="team-description">' . preg_replace('#^<\/p>|<p>$#', '', do_shortcode($content)) . '</div></div>';
   
   return $output;
}
add_shortcode('team_one_half', 'sr_team_one_half');

function sr_team_one_half_last( $atts, $content = null ) {
	
	extract( shortcode_atts( array(
      'name' => 'MemberName',
	  'title' => 'MemberTitle',
	  'img' => ''
      ), $atts ) );
	
   $output = '<div class="column one_half last team-member">';
   if ($img) { $output .= '<img src="'.$img.'" alt="Teammember '.$name.'"/>'; }
   $output .= '<div class="team-meta"><h5><strong>'.$name.'</strong></h5><span class="team-title"><i>'.$title.'</i></span></div>';
   $output .= '<div class="team-description">' . preg_replace('#^<\/p>|<p>$#', '', do_shortcode($content)) . '</div></div><div class="clear"></div>';
   
   return $output;
}
add_shortcode('team_one_half_last', 'sr_team_one_half_last');

function sr_team_one_third( $atts, $content = null ) {
	
	extract( shortcode_atts( array(
      'name' => 'MemberName',
	  'title' => 'MemberTitle',
	  'img' => ''
      ), $atts ) );
	
   $output = '<div class="column one_third team-member">';
   if ($img) { $output .= '<img src="'.$img.'" alt="Teammember '.$name.'"/>'; }
   $output .= '<div class="team-meta"><h5><strong>'.$name.'</strong></h5><span class="team-title"><i>'.$title.'</i></span></div>';
   $output .= '<div class="team-description">' . preg_replace('#^<\/p>|<p>$#', '', do_shortcode($content)) . '</div></div>';
   
   return $output;
}
add_shortcode('team_one_third', 'sr_team_one_third');

function sr_team_one_third_last( $atts, $content = null ) {
	
	extract( shortcode_atts( array(
      'name' => 'MemberName',
	  'title' => 'MemberTitle',
	  'img' => ''
      ), $atts ) );
	
   $output = '<div class="column one_third last team-member">';
   if ($img) { $output .= '<img src="'.$img.'" alt="Teammember '.$name.'"/>'; }
   $output .= '<div class="team-meta"><h5><strong>'.$name.'</strong></h5><span class="team-title"><i>'.$title.'</i></span></div>';
   $output .= '<div class="team-description">' . preg_replace('#^<\/p>|<p>$#', '', do_shortcode($content)) . '</div></div><div class="clear"></div>';
   
   return $output;
}
add_shortcode('team_one_third_last', 'sr_team_one_third_last');

function sr_team_one_fourth( $atts, $content = null ) {
	
	extract( shortcode_atts( array(
      'name' => 'MemberName',
	  'title' => 'MemberTitle',
	  'img' => ''
      ), $atts ) );
	
   $output = '<div class="column one_fourth team-member">';
   if ($img) { $output .= '<img src="'.$img.'" alt="Teammember '.$name.'"/>'; }
   $output .= '<div class="team-meta"><h5><strong>'.$name.'</strong></h5><span class="team-title"><i>'.$title.'</i></span></div>';
   $output .= '<div class="team-description">' . preg_replace('#^<\/p>|<p>$#', '', do_shortcode($content)) . '</div></div>';
   
   return $output;
}
add_shortcode('team_one_fourth', 'sr_team_one_fourth');

function sr_team_one_fourth_last( $atts, $content = null ) {
	
	extract( shortcode_atts( array(
      'name' => 'MemberName',
	  'title' => 'MemberTitle',
	  'img' => ''
      ), $atts ) );
	
   $output = '<div class="column one_fourth last team-member">';
   if ($img) { $output .= '<img src="'.$img.'" alt="Teammember '.$name.'"/>'; }
   $output .= '<div class="team-meta"><h5><strong>'.$name.'</strong></h5><span class="team-title"><i>'.$title.'</i></span></div>';
   $output .= '<div class="team-description">' . preg_replace('#^<\/p>|<p>$#', '', do_shortcode($content)) . '</div></div><div class="clear"></div>';
   
   return $output;
}
add_shortcode('team_one_fourth_last', 'sr_team_one_fourth_last');




/*-----------------------------------------------------------------------------------*/
/*	Shortcodes for Pricing
/*-----------------------------------------------------------------------------------*/
function sr_pricing( $atts, $content = null ) {
	
	extract( shortcode_atts( array(
      'type' => 'standard',
	  'title' => 'Title',
	  'price' => '19 $',
	  'time' => 'per month',
	  'url' => '#'
      ), $atts ) );
	
	$listing = preg_replace('#^<\/p>|<p>$#', '', do_shortcode($content));
	$listing = explode('||',$listing);
	
	$list = '';
	foreach ($listing as $l) {
		$list .= '<li>'.$l.'</li>';	
	}
	
   $output = '<div class="price-col '.$type.'">';
   $output .= '	<div class="price-header">
   					<h3><strong>'.$title.'</strong></h3>
					<h1><strong><i>'.$price.'</i></strong></h1>
					<span class="pricetime"><i>'.$time.'</i></span>
				</div>';
   $output .= '	<div class="price-body">
   					<ul>
					' .$list. '
					</ul>
				</div>';
   $output .= '	<div class="price-footer">
   					<a class="signupbutton" href="'.$url.'">'.__("Sign up", 'sr_mila_theme').'</a>
				</div>';				
   $output .= '</div>';
   
   return $output;
}
add_shortcode('pricing', 'sr_pricing');


function sr_pricing_last( $atts, $content = null ) {
	
	extract( shortcode_atts( array(
      'type' => 'standard',
	  'title' => 'Title',
	  'price' => '19 $',
	   'time' => 'per month',
	  'url' => '#'
      ), $atts ) );
	
	$listing = preg_replace('#^<\/p>|<p>$#', '', do_shortcode($content));
	$listing = explode('||',$listing);
	
	$list = '';
	foreach ($listing as $l) {
		$list .= '<li>'.$l.'</li>';	
	}
	
   $output = '<div class="price-col '.$type.' last-col">';
   $output .= '	<div class="price-header">
   					<h3><strong>'.$title.'</strong></h3>
					<h1><strong><i>'.$price.'</i></strong></h1>
					<span class="pricetime"><i>'.$time.'</i></span>
				</div>';
   $output .= '	<div class="price-body">
   					<ul>
					' .$list. '
					</ul>
				</div>';
   $output .= '	<div class="price-footer">
   					<a class="signupbutton" href="'.$url.'">'.__("Sign up", 'sr_mila_theme').'</a>
				</div>';				
   $output .= '</div>';
   
   return $output;
}
add_shortcode('pricing_last', 'sr_pricing_last');


function sr_pricingwrapper( $atts, $content = null ) {
	
	extract( shortcode_atts( array(
      'columns' => '4'
      ), $atts ) );
	
   
   $output = '<div class="pricing col'.$columns.' clearfix">' . preg_replace('#^<\/p>|<p>$#', '', do_shortcode($content)) . '</div>';
   
   return $output;
}
add_shortcode('pricingwrapper', 'sr_pricingwrapper');



/*-----------------------------------------------------------------------------------*/
/*	Shortcodes for Page Description
/*-----------------------------------------------------------------------------------*/
function sr_pagedescription( $atts, $content = null )
{		
	return '<h2 class="page-description"><i>' . preg_replace('#^<\/p>|<p>$#', '', do_shortcode($content)) . '</i></h2>';	
}
add_shortcode('pagedescription', 'sr_pagedescription');




/*-----------------------------------------------------------------------------------*/
/*	Shortcodes for Seperator Title
/*-----------------------------------------------------------------------------------*/
function sr_seperator( $atts, $content = null )
{	
	extract( shortcode_atts( array(
	  'title' => ''
      ), $atts ) );
	
	if ($title == '') {
	return '<div class="seperator"><span></span></div>';	
	} else {
	return '<div class="seperator"><span></span><h6 class="sectiontitle"><i>' . $title . '</i></h6></div>';	
	}
}
add_shortcode('seperator', 'sr_seperator');




/*-----------------------------------------------------------------------------------*/
/*	Shortcodes for Buttons
/*-----------------------------------------------------------------------------------*/
function sr_button( $atts, $content = null )
{
	extract( shortcode_atts( array(
      'color' => 'white',
	  'url' => ''
      ), $atts ) );
		
	return '<a href="'.$url.'" class="button '.$color.'">' . preg_replace('#^<\/p>|<p>$#', '', do_shortcode($content)) . '</a>';	
}
add_shortcode('button', 'sr_button');




/*-----------------------------------------------------------------------------------*/
/*	Shortcodes for Alerts
/*-----------------------------------------------------------------------------------*/
function sr_alert( $atts, $content = null )
{
	extract( shortcode_atts( array(
      'color' => 'blue'
      ), $atts ) );
		
	return '<p class="alert '.$color.'">' . preg_replace('#^<\/p>|<p>$#', '', do_shortcode($content)) . '</p>';	
}
add_shortcode('alert', 'sr_alert');




/*-----------------------------------------------------------------------------------*/
/*	Shortcodes for Toggles
/*-----------------------------------------------------------------------------------*/
function sr_toggle( $atts, $content = null )
{
	extract( shortcode_atts( array(
      'title' => 'Toggle'
      ), $atts ) );
		
	return '<div class="toggle"><div class="toggle_title"><a href="" class="color" title="toggle"><span class="toggle_icon">+</span><b>' . $title . '</b></a></div><div class="toggle_inner">' . preg_replace('#^<\/p>|<p>$#', '', do_shortcode($content)) . '</div></div>';	
}
add_shortcode('toggle', 'sr_toggle');




/*-----------------------------------------------------------------------------------*/
/*	Shortcodes for Skills
/*-----------------------------------------------------------------------------------*/
function sr_skill( $atts, $content = null )
{
	extract( shortcode_atts( array(
      'amount' => '55',
	  'name' => 'Skillname'
      ), $atts ) );
		
	return '<div class="skill">
				<span class="skill_name"><strong>'.$name.'</strong></span>
				<div class="skill_bar"><div class="skill_active" style="width: '.$amount.'%;"></div><span><i>'.$amount.'%</i></span></div>
			</div>';	
}
add_shortcode('skill', 'sr_skill');




/*-----------------------------------------------------------------------------------*/
/*	Shortcodes for Tabs
/*-----------------------------------------------------------------------------------*/
function sr_tabs( $atts, $content = null )
{
	extract( shortcode_atts( array(
      'title' => ''
      ), $atts ) );
	
	$return = '<div class="tabs"><ul class="tab_nav clearfix">';
	
	$title = substr($title, 0, -1);
	$title = explode(',', $title);
	$i = 1;
	foreach ($title as $t) {
		if ($i == 1) { $addclass = 'class="active"'; } else { $addclass = ''; }
		$return .= '<li><a href="tabid'.$i.'" '.$addclass.'><strong>'.$t.'</strong></a></li>';	
		$i++;
	}
	
	$return .= '</ul><div class="clear"></div><div class="tab_container">'.do_shortcode($content).'</div></div>';
	
	return $return;	
}
add_shortcode('tabs', 'sr_tabs');


function sr_tab( $atts, $content = null )
{	
	extract( shortcode_atts( array(
      'id' => ''
      ), $atts ) );
	
	if ($id == 1) { $addclass = 'active'; } else { $addclass = ''; }
	return '<div class="tab_content tabid'.$id.' '.$addclass.'">' . preg_replace('#^<\/p>|<p>$#', '', do_shortcode($content)) . '</div>';	
}
add_shortcode('tab', 'sr_tab');




/*-----------------------------------------------------------------------------------*/
/*	Shortcodes for Accordion
/*-----------------------------------------------------------------------------------*/
function sr_accordion( $atts, $content = null )
{
	extract( shortcode_atts( array(
      'title' => 'Toggle'
      ), $atts ) );
		
	return '<div class="toggle"><div class="toggle_title"><a href="" class="color" title="toggle"><span class="toggle_icon">+</span><b>' . $title . '</b></a></div><div class="toggle_inner">' . preg_replace('#^<\/p>|<p>$#', '', do_shortcode($content)) . '</div></div>';		
}
add_shortcode('accordion', 'sr_accordion');


function sr_accordiongroup( $atts, $content = null )
{	
	return '<div class="accordion">' . preg_replace('#^<\/p>|<p>$#', '', do_shortcode($content)) . '</div>';	
}
add_shortcode('accordiongroup', 'sr_accordiongroup');




/*-----------------------------------------------------------------------------------*/
/*	Shortcodes for Contact form
/*-----------------------------------------------------------------------------------*/
function sr_field( $atts, $content = null )
{
	extract( shortcode_atts( array(
      'type' => 'textfield',
	  'name' => 'Fieldname',
	  'slug' => 'slugname',
	  'required' => 'yes'
      ), $atts ) );
		
	
	if ($required == 'yes') { $label_req = 'class="req"'; $req = "*"; } else { $label_req = ''; $req = ""; }
	$input_name = strtolower($slug);
	
	
	$output = '';
	if ($type == 'textfield') {
		$output .= '<div class="form-row clearfix">
					<label for="'.$input_name.'" '.$label_req.'>'.$name.' '.$req.'</label>
					<div class="form-value"><input type="text" name="'.$input_name.'" class="'.$input_name.'" id="'.$input_name.'" value="" /></div>
					</div>';
	} else if ($type == 'textarea') {
		$output .= '<div class="form-row clearfix textbox">
					<label for="'.$input_name.'" '.$label_req.'>'.$name.' '.$req.'</label>
					<div class="form-value"><textarea name="'.$input_name.'" class="'.$input_name.'" id="'.$input_name.'" rows="15" cols="50"></textarea></div>
					</div>';
	} 
	
	
	return $output;
	
}
add_shortcode('field', 'sr_field');

function sr_contactgroup( $atts, $content = null ) {
	
	extract( shortcode_atts( array(
      'fields' => '',
      'email' => 'Testemail',
      'subject' => 'Subject',
      'submit' => 'Send'
      ), $atts ) );
	
	if ($fields == '') { 
		return '<p><span class="error_message">Please check your Contact form. Your shortcode [contactgroup] needs a "fields" attribute</span></p>';
	} else {
   		return '<form id="contact-form" class="checkform" action="" target="'.get_template_directory_uri().'/contact-form.php" method="post" >' . preg_replace('#^<\/p>|<p>$#', '', do_shortcode($content)) . '
		<div class="form-row form-submit"><input type="submit" name="submit_form" class="submit" value="'.$submit.'" /></div>
		<input type="hidden" name="subject" value="'.$subject.'" />
		<input type="hidden" name="fields" value="'.$fields.'" />
		<input type="hidden" name="sendto" value="'.$email.'" />
		</form><p id="form-note"><span class="error_icon">'.__("Error", 'sr_mila_theme').'</span><span class="error_message"><strong>'.__("Please check your entries", 'sr_mila_theme').'!</strong></span></p>';
	}
}
add_shortcode('contactgroup', 'sr_contactgroup');



/*-----------------------------------------------------------------------------------*/
/*	Shortcodes for Gallery
/*-----------------------------------------------------------------------------------*/
function sr_gallery( $atts, $content = null )
{
	extract( shortcode_atts( array(
      'id' => '',
      'crop' => 'yes'
      ), $atts ) );
	
	global $prefix;
	
	if ($id && $id !== '') {
		
		$gallery = get_post_meta($id, $prefix.'_gallery', true);
		$pics = explode('|||',$gallery);
		
		$gallery_items = '';
		foreach ($pics as $p) {
			$image = explode('~~', $p);
			$img_big = wp_get_attachment_image_src($image[1], 'fullwidth-thumb');
			if ($crop == 'no') { $img_thumb = wp_get_attachment_image_src($image[1], 'blog-thumb'); }
			else { $img_thumb = wp_get_attachment_image_src($image[1], 'portfolio-thumb'); }
			$gallery_items .= '
				<div class="masonry-item gallery-entry gallery-masonry">
                	<div class="imgoverlay">
						<a href="'.$img_big[0].'" class="openfancybox" rel="gallery'.$id.'" >
							<img src="'.$img_thumb[0].'" alt="'.get_the_title($image[1]).'"/>
						</a>
					</div>
				</div>';
		}
		
	}
	
	return $gallery_items;
	
}
add_shortcode('sr_customgallery', 'sr_gallery');




/*-----------------------------------------------------------------------------------*/
/*	Register Buttons
/*-----------------------------------------------------------------------------------*/

// Add our buttons
function init_buttons() {
	// If user has permission
	if ( ! current_user_can('edit_posts') && ! current_user_can('edit_pages') )
		return;
	 
	// Add only in Rich Editor mode
	if ( get_user_option('rich_editing') == 'true') {
		add_filter("mce_external_plugins", "add_buttons_plugin");
		add_filter('mce_buttons', 'register_buttons');
	}
}
add_action('init', 'init_buttons');

function register_buttons($buttons) {
	array_push($buttons, "|", "popup");
	return $buttons;
}

// add the button to the tinyMCE bar
function add_buttons_plugin($plugin_array) {
	$plugin_array['popup'] = get_template_directory_uri().'/theme-admin/shortcodes/popup.js';
	return $plugin_array;
}

?>