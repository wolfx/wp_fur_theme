<?php

$absolute_path = __FILE__;
$path_to_file = explode( 'wp-content', $absolute_path );
$path_to_wp = $path_to_file[0];

// Access WordPress
require_once( $path_to_wp . '/wp-load.php' );


$sc = 'sc';
/*-----------------------------------------------------------------------------------*/
/*	Sections & Options
/*-----------------------------------------------------------------------------------*/

$options = array(
	
	array( "name" => __("Columns", 'sr_mila_theme'),
		   "desc" => "",
		   "id" => "columns",
		   "type" => "sectionstart"
		  ),
			
			array( "name" => __("Column Layout", 'sr_mila_theme'),
				   "desc" => __("Choose your layout for the columns you want include.", 'sr_mila_theme'),
				   "id" => $sc."_columnlayout",
				   "type" => "radiocustom",
				   "option" => array( 
						array(	"name" =>"Layout 2 x one_half", 
							  	"value" => "layout_onehalf-onehalf"),
						array( 	"name"=>"Layout one_third + two_third", 
							  	"value"=> "layout_onethird-twothird"),
						array( 	"name"=>"Layout two_third + one_third", 
							  	"value"=> "layout_twothird-onethird"),
						array(	"name" =>"Layout 3 x one_third", 
							  	"value"=> "layout_onethird-onethird-onethird"),
						array(	"name" =>"Layout one_half + 2 x one_fourth", 
							  	"value"=> "layout_onehalf-onefourth-onefourth"),
						array(	"name" =>"Layout 4 x one_fourth", 
							  	"value"=> "layout_onefourth-onefourth-onefourth-onefourth")
						)
				  ),
	
			array( "name" => __("Column 1", 'sr_mila_theme'),
				   "desc" => "",
				   "id" => $sc."_column_one",
				   "type" => "textarea"
				  ),
			
			array( "name" => __("Column 2", 'sr_mila_theme'),
				   "desc" => "",
				   "id" => $sc."_column_two",
				   "type" => "textarea"
				  ),
			
			array( "name" => __("Column 3", 'sr_mila_theme'),
				   "desc" => "",
				   "id" => $sc."_column_three",
				   "type" => "textarea"
				  ),
			
			array( "name" => __("Column 4", 'sr_mila_theme'),
				   "desc" => "",
				   "id" => $sc."_column_four",
				   "type" => "textarea"
				  ),
			
	
	array( "name" => __("Columns", 'sr_mila_theme'),
		   	"id" => "columns",
		    "type" => "sectionend"),
	
	
	
	array( "name" => __("Skills", 'sr_mila_theme'),
		   "desc" => "",
		   "id" => "skills",
		   "type" => "sectionstart"
		  ),
			
			array( "name" => "Group",
				   "id" => $sc."_skillgroup",
				   "type" => "groupstart"
				  ),
			
				array( "name" => __("Percent Amount", 'sr_mila_theme'),
				   	   "desc" => "",
					   "id" => $sc."_skillpercent",
					   "type" => "text"
					  ),
				
				array( "name" => __("Name", 'sr_mila_theme'),
					   "desc" => "",
					   "id" => $sc."_skillname",
					   "type" => "text"
					  ),
			
			array( "name" => "Groupend",
				   "id" => $sc."_skillgroup",
				   "type" => "groupend"
				  ),
			
			array( "name" => __("Add Skill", 'sr_mila_theme'),
				   "id" => $sc."_skillduplicater",
				   "group" => $sc."_skillgroup",
				   "type" => "grouduplicater"
				  ),

	array ( "name" => __("Skills", 'sr_mila_theme'),
		   	"id" => "skills",
		    "type" => "sectionend"),
	
	
	/*array( "name" => __("Pricing Tables", 'sr_mila_theme'),
		   "desc" => "",
		   "id" => "pricing",
		   "type" => "sectionstart"
		  ),
	
			array( "name" => __("Pricing Layout", 'sr_mila_theme'),
				   "desc" => __("Choose a layout", 'sr_mila_theme'),
				   "id" => $sc."_pricinglayout",
				   "type" => "radiocustom",
				   "option" => array( 
						array(	"name" =>"Layout 2 x one_half", 
							  	"value" => "layout_onehalf-onehalf"),
						array(	"name" =>"Layout 3 x one_third", 
							  	"value"=> "layout_onethird-onethird-onethird"),
						array(	"name" =>"Layout 4 x one_fourth", 
							  	"value"=> "layout_onefourth-onefourth-onefourth-onefourth")
						)
				  ),
			
			array( "name" => "Group",
				   "id" => $sc."_pricinggroup_one",
				   "type" => "groupstart"
				  ),
			
				array( "name" => __("Type", 'sr_mila_theme'),
					   "desc" => "",
					   "id" => $sc."_pricingtype_one",
					   "type" => "selectbox",
					   "option" => array( 
						array(	"name" => __("Standard", 'sr_mila_theme'), 
							  	"value" => "standard"),
						array(	"name" => __("Recommended", 'sr_mila_theme'), 
							  	"value"=> "recommended"),
						array(	"name" => __("Best", 'sr_mila_theme'), 
							  	"value"=> "best")
						)
					  ),
				
				array( "name" => __("Title", 'sr_mila_theme'),
					   "desc" => "",
					   "id" => $sc."_pricingtitle_one",
					   "type" => "text"
					  ),
				
				array( "name" => __("Price", 'sr_mila_theme'),
					   "desc" => __("Enter the price with currency", 'sr_mila_theme'),
					   "id" => $sc."_pricingprice_one",
					   "type" => "text"
					  ),
				
				array( "name" => __("Time indication", 'sr_mila_theme'),
					   "desc" => "",
					   "id" => $sc."_pricingtime_one",
					   "type" => "text"
					  ),
				
				array( "name" => __("Listing", 'sr_mila_theme'),
					   "desc" => __("Enter the different listing values seperated by a double '||' sign.", 'sr_mila_theme')."<br /><br /><b>Example</b><br>100MB Disk Space||200MB Monthly Traffic||2 Subdomains<br>",
					   "id" => $sc."_pricinglisting_one",
					   "type" => "textarea"
					  ),
				
				array( "name" => __("Link URL", 'sr_mila_theme'),
					   "desc" => __("Enter a url for the sign up button.", 'sr_mila_theme'),
					   "id" => $sc."_pricingurl_one",
					   "type" => "text"
					  ),
			
			array( "name" => "Groupend",
				   "id" => $sc."_pricinggroup_one",
				   "type" => "groupend"
				  ),
			
			array( "name" => "Group",
				   "id" => $sc."_pricinggroup_two",
				   "type" => "groupstart"
				  ),
			
				array( "name" => __("Type", 'sr_mila_theme'),
					   "desc" => "",
					   "id" => $sc."_pricingtype_two",
					   "type" => "selectbox",
					   "option" => array( 
						array(	"name" => __("Standard", 'sr_mila_theme'), 
							  	"value" => "standard"),
						array(	"name" => __("Recommended", 'sr_mila_theme'), 
							  	"value"=> "recommended"),
						array(	"name" => __("Best", 'sr_mila_theme'), 
							  	"value"=> "best")
						)
					  ),
				
				array( "name" => __("Title", 'sr_mila_theme'),
					   "desc" => "",
					   "id" => $sc."_pricingtitle_two",
					   "type" => "text"
					  ),
				
				array( "name" => __("Price", 'sr_mila_theme'),
					   "desc" => __("Enter the price with currency", 'sr_mila_theme'),
					   "id" => $sc."_pricingprice_two",
					   "type" => "text"
					  ),
				
				array( "name" => __("Time indication", 'sr_mila_theme'),
					   "desc" => "",
					   "id" => $sc."_pricingtime_two",
					   "type" => "text"
					  ),
				
				array( "name" => __("Listing", 'sr_mila_theme'),
					   "desc" => __("Enter the different listing values seperated by a double '||' sign.", 'sr_mila_theme')."<br /><br /><b>Example</b><br>100MB Disk Space||200MB Monthly Traffic||2 Subdomains<br>",
					   "id" => $sc."_pricinglisting_two",
					   "type" => "textarea"
					  ),
				
				array( "name" => __("Link URL", 'sr_mila_theme'),
					   "desc" => __("Enter a url for the sign up button.", 'sr_mila_theme'),
					   "id" => $sc."_pricingurl_two",
					   "type" => "text"
					  ),
			
			array( "name" => "Groupend",
				   "id" => $sc."_pricinggroup_two",
				   "type" => "groupend"
				  ),
			
			array( "name" => "Group",
				   "id" => $sc."_pricinggroup_three",
				   "type" => "groupstart"
				  ),
			
				array( "name" => __("Type", 'sr_mila_theme'),
					   "desc" => "",
					   "id" => $sc."_pricingtype_three",
					   "type" => "selectbox",
					   "option" => array( 
						array(	"name" => __("Standard", 'sr_mila_theme'), 
							  	"value" => "standard"),
						array(	"name" => __("Recommended", 'sr_mila_theme'), 
							  	"value"=> "recommended"),
						array(	"name" => __("Best", 'sr_mila_theme'), 
							  	"value"=> "best")
						)
					  ),
				
				array( "name" => __("Title", 'sr_mila_theme'),
					   "desc" => "",
					   "id" => $sc."_pricingtitle_three",
					   "type" => "text"
					  ),
				
				array( "name" => __("Price", 'sr_mila_theme'),
					   "desc" => __("Enter the price with currency", 'sr_mila_theme'),
					   "id" => $sc."_pricingprice_three",
					   "type" => "text"
					  ),
				
				array( "name" => __("Time indication", 'sr_mila_theme'),
					   "desc" => "",
					   "id" => $sc."_pricingtime_three",
					   "type" => "text"
					  ),
				
				array( "name" => __("Listing", 'sr_mila_theme'),
					   "desc" => __("Enter the different listing values seperated by a double '||' sign.", 'sr_mila_theme')."<br /><br /><b>Example</b><br>100MB Disk Space||200MB Monthly Traffic||2 Subdomains<br>",
					   "id" => $sc."_pricinglisting_three",
					   "type" => "textarea"
					  ),
				
				array( "name" => __("Link URL", 'sr_mila_theme'),
					   "desc" => __("Enter a url for the sign up button.", 'sr_mila_theme'),
					   "id" => $sc."_pricingurl_three",
					   "type" => "text"
					  ),
			
			array( "name" => "Groupend",
				   "id" => $sc."_pricinggroup_three",
				   "type" => "groupend"
				  ),
			
			array( "name" => "Group",
				   "id" => $sc."_pricinggroup_four",
				   "type" => "groupstart"
				  ),
			
				array( "name" => __("Type", 'sr_mila_theme'),
					   "desc" => "",
					   "id" => $sc."_pricingtype_four",
					   "type" => "selectbox",
					   "option" => array( 
						array(	"name" => __("Standard", 'sr_mila_theme'), 
							  	"value" => "standard"),
						array(	"name" => __("Recommended", 'sr_mila_theme'), 
							  	"value"=> "recommended"),
						array(	"name" => __("Best", 'sr_mila_theme'), 
							  	"value"=> "best")
						)
					  ),
				
				array( "name" => __("Title", 'sr_mila_theme'),
					   "desc" => "",
					   "id" => $sc."_pricingtitle_four",
					   "type" => "text"
					  ),
				
				array( "name" => __("Price", 'sr_mila_theme'),
					   "desc" => __("Enter the price with currency", 'sr_mila_theme'),
					   "id" => $sc."_pricingprice_four",
					   "type" => "text"
					  ),
				
				array( "name" => __("Time indication", 'sr_mila_theme'),
					   "desc" => "",
					   "id" => $sc."_pricingtime_four",
					   "type" => "text"
					  ),
				
				array( "name" => __("Listing", 'sr_mila_theme'),
					   "desc" => __("Enter the different listing values seperated by a double '||' sign.", 'sr_mila_theme')."<br /><br /><b>Example</b><br>100MB Disk Space||200MB Monthly Traffic||2 Subdomains<br>",
					   "id" => $sc."_pricinglisting_four",
					   "type" => "textarea"
					  ),
				
				array( "name" => __("Link URL", 'sr_mila_theme'),
					   "desc" => __("Enter a url for the sign up button.", 'sr_mila_theme'),
					   "id" => $sc."_pricingurl_four",
					   "type" => "text"
					  ),
			
			array( "name" => "Groupend",
				   "id" => $sc."_pricinggroup_four",
				   "type" => "groupend"
				  ),

	array( "name" => __("Pricing Tables", 'sr_mila_theme'),
		   	"id" => "pricing",
		    "type" => "sectionend"),*/
	
	
	
	array( "name" => __("Contact Form", 'sr_mila_theme'),
		   "desc" => "",
		   "id" => "contact",
		   "type" => "sectionstart"
		  ),
	
			array( "name" => __("Recipient Email", 'sr_mila_theme'),
				   "desc" => "",
				   "id" => $sc."_contactsendto",
				   "type" => "text"
				  ),
			
			array( "name" => __("Subject", 'sr_mila_theme'),
				   "desc" => "",
				   "id" => $sc."_contactsubject",
				   "type" => "text"
				  ),
			
			array( "name" => __("Submit Button", 'sr_mila_theme'),
				   "desc" => "",
				   "id" => $sc."_contactsubmit",
				   "type" => "text"
				  ),
			
			array( "name" => "Group",
				   "id" => $sc."_contactgroup",
				   "type" => "groupstart"
				  ),
			
				array( "name" => __("Fieltype", 'sr_mila_theme'),
					   "desc" => "",
					   "id" => $sc."_contacttype",
					   "type" => "selectbox",
					   "option" => array( 
						array(	"name" => __("Textfield", 'sr_mila_theme'), 
							  	"value" => "textfield"),
						array(	"name" => __("Textarea", 'sr_mila_theme'), 
							  	"value"=> "textarea")
						)
					  ),
				
				array( "name" => __("Fieldname / Label", 'sr_mila_theme'),
					   "desc" => "",
					   "id" => $sc."_contactname",
					   "type" => "text"
					  ),
				
				array( "name" => __("Slugname", 'sr_mila_theme'),
					   "desc" => "",
					   "id" => $sc."_contactslug",
					   "type" => "text"
					  ),
				
				array( "name" => __("Required field?", 'sr_mila_theme'),
					   "desc" => "",
					   "id" => $sc."_contactreq",
					   "type" => "selectbox",
					   "option" => array( 
						array(	"name" =>"Yes", 
							  	"value" => "yes"),
						array( 	"name"=>"No", 
							  	"value"=> "no")
						)
					  ),
			
			array( "name" => "Groupend",
				   "id" => $sc."_contactgroup",
				   "type" => "groupend"
				  ),
			
			array( "name" => __("Add FIeld", 'sr_mila_theme'),
				   "id" => $sc."_contactduplicater",
				   "group" => $sc."_contactgroup",
				   "type" => "grouduplicater"
				  ),

	array( "name" => __("Contact Form", 'sr_mila_theme'),
		   	"id" => "contact",
		    "type" => "sectionend"),
	
	
	array( "name" => __("Buttons", 'sr_mila_theme'),
		   "desc" => "",
		   "id" => "buttons",
		   "type" => "sectionstart"
		  ),
			
			array( "name" => __("Button Color", 'sr_mila_theme'),
				   "desc" => "",
				   "id" => $sc."_buttoncolor",
				   "type" => "selectbox",
				   "option" => array( 
						array(	"name" => __("White", 'sr_mila_theme'), 
							  	"value" => "white"),
						array(	"name" => __("Blue", 'sr_mila_theme'), 
							  	"value"=> "blue"),
						array(	"name" => __("Yellow", 'sr_mila_theme'), 
							  	"value"=> "yellow"),
						array(	"name" => __("Green", 'sr_mila_theme'), 
							  	"value"=> "green"),
						array(	"name" => __("Grey", 'sr_mila_theme'), 
							  	"value"=> "grey"),
						array(	"name" => __("Pink", 'sr_mila_theme'), 
							  	"value"=> "pink"),
						array(	"name" => __("Purple", 'sr_mila_theme'), 
							  	"value"=> "purple"),
						array(	"name" => __("Orange", 'sr_mila_theme'), 
							  	"value"=> "orange")
						)
				  ),
	
			array( "name" => __("Button URL", 'sr_mila_theme'),
				   "desc" => "",
				   "id" => $sc."_buttonurl",
				   "type" => "text"
				  ),
			
			array( "name" => __("Button Text", 'sr_mila_theme'),
				   "desc" => "",
				   "id" => $sc."_buttontext",
				   "type" => "text"
				  ),
			
	
	array( "name" => __("Buttons", 'sr_mila_theme'),
		   	"id" => "buttons",
		    "type" => "sectionend"),
	
	
	
	array( "name" => __("Alerts", 'sr_mila_theme'),
		   "desc" => "",
		   "id" => "alert",
		   "type" => "sectionstart"
		  ),
			
			array( "name" => __("Alert Type/Color", 'sr_mila_theme'),
				   "desc" => "",
				   "id" => $sc."_alertcolor",
				   "type" => "selectbox",
				   "option" => array( 
						array(	"name" =>"Info / Blue", 
							  	"value" => "blue"),
						array(	"name" =>"Note / Yellow", 
							  	"value" => "yellow"),
						array(	"name" =>"Confirm / Green", 
							  	"value" => "green"),
						array(	"name" =>"Error / Red", 
							  	"value" => "red")
						)
				  ),
	
			array( "name" => __("Alert Text", 'sr_mila_theme'),
				   "desc" => "",
				   "id" => $sc."_alerttext",
				   "type" => "textarea"
				  ),
						
	
	array( "name" => __("Alerts", 'sr_mila_theme'),
		   "id" => "alert",
		    "type" => "sectionend"),
	
	
	
	array( "name" => __("Toggle", 'sr_mila_theme'),
		   "desc" => "",
		   "id" => "toggle",
		   "type" => "sectionstart"
		  ),
			
	
			array( "name" => __("Toggle Title", 'sr_mila_theme'),
				   "desc" => "",
				   "id" => $sc."_toggletitle",
				   "type" => "text"
				  ),
			
			array( "name" => __("Toggle Text", 'sr_mila_theme'),
				   "desc" => "",
				   "id" => $sc."_toggletext",
				   "type" => "textarea"
				  ),
						
	
	array( "name" => __("Toggle", 'sr_mila_theme'),
		   "id" => "toggle",
		    "type" => "sectionend"),
	
	
	
	array( "name" => __("Tabs", 'sr_mila_theme'),
		   "desc" => "",
		   "id" => "tab",
		   "type" => "sectionstart"
		  ),
			
			array( "name" => "Group",
				   "id" => $sc."_tabgroup",
				   "type" => "groupstart"
				  ),
			
				array( "name" => __("Tab Name", 'sr_mila_theme'),
				   	   "desc" => "",
					   "id" => $sc."_tabname",
					   "type" => "text"
					  ),
				
				array( "name" => __("Tab Text", 'sr_mila_theme'),
					   "desc" => "",
					   "id" => $sc."_tabtext",
					   "type" => "textarea"
					  ),
			
			array( "name" => "Groupend",
				   "id" => $sc."_tabgroup",
				   "type" => "groupend"
				  ),
			
			array( "name" => __("Add Tab", 'sr_mila_theme'),
				   "id" => $sc."_tabduplicater",
				   "group" => $sc."_tabgroup",
				   "type" => "grouduplicater"
				  ),

	array ( "name" => __("Tabs", 'sr_mila_theme'),
		   	"id" => "tab",
		    "type" => "sectionend"),
	
	
	
	array( "name" => __("Accordion", 'sr_mila_theme'),
		   "desc" => "",
		   "id" => "accordion",
		   "type" => "sectionstart"
		  ),
			
			array( "name" => "Group",
				   "id" => $sc."_accordiongroup",
				   "type" => "groupstart"
				  ),
			
				array( "name" => __("Title", 'sr_mila_theme'),
				   	   "desc" => "",
					   "id" => $sc."_accordiontitle",
					   "type" => "text"
					  ),
				
				array( "name" => __("Text", 'sr_mila_theme'),
					   "desc" => "",
					   "id" => $sc."_accordiontext",
					   "type" => "textarea"
					  ),
			
			array( "name" => "Groupend",
				   "id" => $sc."_accordiongroup",
				   "type" => "groupend"
				  ),
			
			array( "name" => __("Add Tab", 'sr_mila_theme'),
				   "id" => $sc."_accordionduplicater",
				   "group" => $sc."_accordiongroup",
				   "type" => "grouduplicater"
				  ),

	array ( "name" => __("Accordion", 'sr_mila_theme'),
		   	"id" => "accordion",
		    "type" => "sectionend"),
	
	
	array( "name" => __("Gallery", 'sr_mila_theme'),
		   "desc" => "",
		   "id" => "gallery",
		   "type" => "sectionstart"
		  ),
			
			array( "name" => __("Choose Gallery", 'sr_mila_theme'),
				   "desc" => __("Please make sure to have created/added a Gallery", 'sr_mila_theme'),
				   "id" => $sc."_galleryid",
				   "type" => "gallery"
				  ),
	
			array( "name" => __("Crop Images to same Size", 'sr_mila_theme'),
				   "desc" => "",
				   "id" => $sc."_gallerycrop",
				   "type" => "selectbox",
				   "option" => array( 
						array(	"name" =>"No", 
							  	"value" => "no"),
						array(	"name" =>"Yes", 
							  	"value" => "yes")
						)
				  ),
						
	
	array( "name" => __("Gallery", 'sr_mila_theme'),
		   "id" => "gallery",
		    "type" => "sectionend"),
	
	
	
	/*array( "name" => __("Seperator", 'sr_mila_theme'),
		   "desc" => "",
		   "id" => "seperator",
		   "type" => "sectionstart"
		  ),
						
			array( "name" => __("Seperator Title (optional)", 'sr_mila_theme'),
				   "desc" => __("Enter a title if you want to add a seperator followed by a title.", 'sr_mila_theme'),
				   "id" => $sc."_seperatortitle",
				   "type" => "text"
				  ),
						
	
	array( "name" => __("Seperator", 'sr_mila_theme'),
		   "id" => "seperator",
		    "type" => "sectionend"),*/
	
	
	
	/*array( "name" => __("Team Member", 'sr_mila_theme'),
		   "desc" => "",
		   "id" => "team",
		   "type" => "sectionstart"
		  ),
			
			array( "name" => __("Team Layout", 'sr_mila_theme'),
				   "desc" => __("Choose a layout", 'sr_mila_theme'),
				   "id" => $sc."_teamlayout",
				   "type" => "radiocustom",
				   "option" => array( 
						array(	"name" =>"Layout 2 x one_half", 
							  	"value" => "layout_onehalf-onehalf"),
						array(	"name" =>"Layout 3 x one_third", 
							  	"value"=> "layout_onethird-onethird-onethird"),
						array(	"name" =>"Layout 4 x one_fourth", 
							  	"value"=> "layout_onefourth-onefourth-onefourth-onefourth")
						)
				  ),
	
			array( "name" => "Group",
				   "id" => $sc."_teamgroup_one",
				   "type" => "groupstart"
				  ),
			
				array( "name" => __("Image URL", 'sr_mila_theme'),
					   "desc" => "",
					   "id" => $sc."_teamimage_one",
					   "type" => "text"
					  ),
				
				array( "name" => __("Name", 'sr_mila_theme'),
					   "desc" => "",
					   "id" => $sc."_teamname_one",
					   "type" => "text"
					  ),
				
				array( "name" => __("Title", 'sr_mila_theme'),
					   "desc" => "",
					   "id" => $sc."_teamtitle_one",
					   "type" => "text"
					  ),
				
				array( "name" => __("Text", 'sr_mila_theme'),
					   "desc" => "",
					   "id" => $sc."_teamtext_one",
					   "type" => "textarea"
					  ),
				
			array( "name" => "Groupend",
				   "id" => $sc."_teamgroup_one",
				   "type" => "groupend"
				  ),
			
			array( "name" => "Group",
				   "id" => $sc."_teamgroup_two",
				   "type" => "groupstart"
				  ),
			
				array( "name" => __("Image URL", 'sr_mila_theme'),
					   "desc" => "",
					   "id" => $sc."_teamimage_two",
					   "type" => "text"
					  ),
				
				array( "name" => __("Name", 'sr_mila_theme'),
					   "desc" => "",
					   "id" => $sc."_teamname_two",
					   "type" => "text"
					  ),
				
				array( "name" => __("Title", 'sr_mila_theme'),
					   "desc" => "",
					   "id" => $sc."_teamtitle_two",
					   "type" => "text"
					  ),
				
				array( "name" => __("Text", 'sr_mila_theme'),
					   "desc" => "",
					   "id" => $sc."_teamtext_two",
					   "type" => "textarea"
					  ),
				
			array( "name" => "Groupend",
				   "id" => $sc."_teamgroup_two",
				   "type" => "groupend"
				  ),
			
			array( "name" => "Group",
				   "id" => $sc."_teamgroup_three",
				   "type" => "groupstart"
				  ),
			
				array( "name" => __("Image URL", 'sr_mila_theme'),
					   "desc" => "",
					   "id" => $sc."_teamimage_three",
					   "type" => "text"
					  ),
			
				array( "name" => __("Name", 'sr_mila_theme'),
					   "desc" => "",
					   "id" => $sc."_teamname_three",
					   "type" => "text"
					  ),
				
				array( "name" => __("Title", 'sr_mila_theme'),
					   "desc" => "",
					   "id" => $sc."_teamtitle_three",
					   "type" => "text"
					  ),
				
				array( "name" => __("Text", 'sr_mila_theme'),
					   "desc" => "",
					   "id" => $sc."_teamtext_three",
					   "type" => "textarea"
					  ),
				
			array( "name" => "Groupend",
				   "id" => $sc."_teamgroup_three",
				   "type" => "groupend"
				  ),
			
			array( "name" => "Group",
				   "id" => $sc."_teamgroup_four",
				   "type" => "groupstart"
				  ),
			
				array( "name" => __("Image URL", 'sr_mila_theme'),
					   "desc" => "",
					   "id" => $sc."_teamimage_four",
					   "type" => "text"
					  ),
			
				array( "name" => __("Name", 'sr_mila_theme'),
					   "desc" => "",
					   "id" => $sc."_teamname_four",
					   "type" => "text"
					  ),
				
				array( "name" => __("Title", 'sr_mila_theme'),
					   "desc" => "",
					   "id" => $sc."_teamtitle_four",
					   "type" => "text"
					  ),
				
				array( "name" => __("Text", 'sr_mila_theme'),
					   "desc" => "",
					   "id" => $sc."_teamtext_four",
					   "type" => "textarea"
					  ),
				
			array( "name" => "Groupend",
				   "id" => $sc."_teamgroup_four",
				   "type" => "groupend"
				  ),
			
	array( "name" => __("Team Member", 'sr_mila_theme'),
		   "id" => "team",
		    "type" => "sectionend"),
	
	
	
	array( "name" => __("Pagedescription", 'sr_mila_theme'),
		   "desc" => "",
		   "id" => "pagedesc",
		   "type" => "sectionstart"
		  ),
			
			array( "name" => __("Text", 'sr_mila_theme'),
				   "desc" => __("Enter your page description", 'sr_mila_theme'),
				   "id" => $sc."_pagedesc",
				   "type" => "textarea"
				  ),
	
	array( "name" => __("Pagedescription", 'sr_mila_theme'),
		   "id" => "pagedesc",
		    "type" => "sectionend"),*/
	
);

?>




<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script language="javascript" type="text/javascript" src="js/tinymce_popup.js"></script>
<script language="javascript" type="text/javascript" src="js/shortcodes-tinymce.js"></script>
<!-- <style type="text/css" src="css/dialog.css"></style> -->
<link rel="stylesheet" href="css/shortcodes-tinymce.css" />

<title>Shortcodes</title>

</head>
<body>

<div id="shortcodes">

	<div class="sc_option_panel">
    	<ul class="sc_list">
		<?php 
        
        foreach ($options as $opt) {
            if ($opt['type'] == 'sectionstart') {
                echo '<li class="'.$opt['id'].'"><a href="'.$opt['id'].'"><b>'.$opt['name'].'</b></a></li>';	 
            }
        }
        
        ?>
		</ul>
	</div> <!-- END .sc_option_panel -->
    
    
    
    <div class="sc_option">
    
    	<?php 
        
        foreach ($options as $option) {
            switch ( $option['type'] ) {
		
			//sectionstart
			case "sectionstart":
				echo '	<div id="'.$option['id'].'" class="option-content">
						<div class="option-title"><b>'.$option['name'].'</b><br /><span><i>'.$option['desc'].'</i></span></div>
						<form id="form_'.$option['id'].'" action="" method="get" accept-charset="utf-8">
						';
			break;
			
			//sectionend
			case "sectionend":
				echo '	<a href="" id="insert_'.$option['id'].'" class="submit">'.__("Insert", 'sr_mila_theme').'</a> 
						</form>
						</div>';
			break;
			
			//groupstart
			case "groupstart":
				echo '	<div id="'.$option['id'].'" class="group">';
			break;
			
			//groupend
			case "groupend":
				echo '	</div>';
			break;
			
			//grouduplicater
			case "grouduplicater";
				echo '	<a href="'.$option['group'].'" id="'.$option['id'].'" class="groupduplicater">&raquo; '.$option['name'].'</a><br>';
			break;
			
			//text
			case "text":
				echo '<div id="'.$option['id'].'" class="option clear">';
					echo '	<div class="option_name">
								<label for="'.$option['id'].'">'.$option['name'].'</label><br /><span class="sr_description">'.$option['desc'].'</span>
							</div>';
					echo '	<div class="option_value">
								<input type="text" name="'.$option['id'].'" id="'.$option['id'].'" value="" size="30" />
							</div>';
				echo '</div>';
			break;
			
			// selectbox  
			case 'selectbox':  
				echo '<div id="'.$option['id'].'" class="option clear">';
					echo '	<div class="option_name">
								<label for="'.$option['id'].'">'.$option['name'].'</label><br /><span class="sr_description">'.$option['desc'].'</span>
							</div>';
					echo '	<div class="option_value">
								<select name="'.$option['id'].'" id="'.$option['id'].'">';
								$i = 0;
								foreach ($option['option'] as $var) {
									echo '<option value="'.$var['value'].'" /> '.$var['name'].'</div>';
								$i++;	
								}			  
					echo '		</select> 
							</div>';
				echo '</div>';
			break;
			
			//textarea
			case "textarea":
				echo '<div id="'.$option['id'].'" class="option clear">';
					echo '	<div class="option_name">
								<label for="'.$option['id'].'">'.$option['name'].'</label><br /><span class="sr_description">'.$option['desc'].'</span>
							</div>';
					echo '	<div class="option_value">
								<textarea name="'.$option['id'].'" id="'.$option['id'].'"></textarea>
							</div>';
				echo '</div>';
			break;
			
			//radiocustom
			case "radiocustom":
				echo '<div id="'.$option['id'].'" class="option clear radiocustom">';
					echo '	<div class="option_name">
								<label for="'.$option['id'].'">'.$option['name'].'</label><br /><span class="sr_description">'.$option['desc'].'</span>
							</div>';
					echo '	<div class="option_value">';
					
					$i = 0;
					foreach ($option['option'] as $var) {
						echo '<input type="radio" name="'.$option['id'].'" id="'.$var['value'].'" value="'.$var['value'].'" />
						<a class="customradio '.$var['value'].'" href="'.$var['value'].'"><span>'.$var['name'].'</span></a>';
					$i++;	
					}
	
					echo '	</div>';		
				echo '</div>';
			break;
			
			//gallery
			case "gallery":
				echo '<div id="'.$option['id'].'" class="option clear">';
					echo '	<div class="option_name">
								<label for="'.$option['id'].'">'.$option['name'].'</label><br /><span class="sr_description">'.$option['desc'].'</span>
							</div>';
					echo '	<div class="option_value">
								<select name="'.$option['id'].'" id="'.$option['id'].'">';
								  $gal = get_posts( array('post_type' => 'gallery') );
								  foreach ( $gal as $g ) {
									if ($g->ID == $value) { $active = 'selected="selected"'; }  else { $active = ''; } 
									$option = '<option value="' . $g->ID . '" '.$active.'>';
									$option .= $g->post_title;
									$option .= '</option>';
									echo $option;
								  }
					echo '		</select> 
							</div>';
				echo '</div>';
			break;
			
			}
			
        }
        
        ?>
    
    </div> <!-- END .sc_option_panel -->

</div>
    
</body>
</html>