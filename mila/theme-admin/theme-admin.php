<?php


/*-----------------------------------------------------------------------------------

	Theme Admin

-----------------------------------------------------------------------------------*/



/*-----------------------------------------------------------------------------------*/
/*	Includes
/*-----------------------------------------------------------------------------------*/

// Adding Option Panel
include("option-panel/option-panel.php");



// Adding post/work meta boxes
include("functions/theme-postmeta.php");

// Adding Custom Post type
include("functions/theme-custom-post-types.php");

// Theme general frontend features
include("functions/theme-general-features.php");



// Adding Shortcodes
include("shortcodes/shortcodes.php");



// Add the Latest Tweets Custom Widget
include("widgets/widget-tweets.php");

// Add the Latest Tweets Custom Widget
include("widgets/widget-flickr.php");

// Add the Latest Tweets Custom Widget
include("widgets/widget-dribbble.php");

// Add the Social Links Widget
include("widgets/widget-sociallinks.php");





/*-----------------------------------------------------------------------------------*/
/*	Register Widget Areas
/*-----------------------------------------------------------------------------------*/
if( !function_exists( 'sr_widgets_init' ) ) {
	function sr_widgets_init() {
						
		register_sidebar( array(
			'name' => __( 'Bottom Left', 'sr_mila_theme' ),
			'id' => 'bottom-left',
			'before_widget' => '<div class="left_float clearfix">',
			'after_widget' => "</div>",
			'before_title' => '<h3 class="sectiontitle widgettitle">',
			'after_title' => '</h3>'
		) );
			
		
	}
}
add_action( 'widgets_init', 'sr_widgets_init' );




/*-----------------------------------------------------------------------------------*/
/*	Custom Wordpress Login Logo
/*-----------------------------------------------------------------------------------*/
if( !function_exists( 'sr_custom_login_logo' ) ) {
	function sr_custom_login_logo() {
	   global $prefix;
	   if (get_option($prefix.'_loginlogo')) {
		echo '<style type="text/css">
			h1 a { 
				background-image: url('.get_option($prefix.'_loginlogo').') !important;
				background-position: center center !important;
			}
		</style>';
		}
	} 
}
add_action('login_head', 'sr_custom_login_logo');





/*-----------------------------------------------------------------------------------*/
/*	Next/Prev links customization
/*-----------------------------------------------------------------------------------*/
add_filter('next_posts_link_attributes', 'add_prev_class');
add_filter('previous_posts_link_attributes', 'add_next_class');

function add_next_class() {
    return 'class="next"';
}

function add_prev_class() {
    return 'class="prev"';
}




/*-----------------------------------------------------------------------------------*/
/*	Comment Function
/*-----------------------------------------------------------------------------------*/
if( !function_exists( 'sr_comment' ) ) {
    function sr_comment($comment, $args, $depth) {

        $GLOBALS['comment'] = $comment; ?>
        <li <?php comment_class(); ?> id="li-comment-<?php comment_ID() ?>">
        
        <div id="comment-<?php comment_ID() ?>">
            <div class="user"><?php echo get_avatar( $comment, $size = '50'); ?></div>
            <div class="comment_content">
                <h5><strong><?php comment_author(); ?></strong></h5>
                <div class="comment_text">
                    <?php comment_text() ?>
                </div>
                <div class="comment_date"><?php comment_date(); ?> <?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?></div>
            </div>
        </div>

    <?php
    }
}



/*-----------------------------------------------------------------------------------*/
/* Add Favicon
/*-----------------------------------------------------------------------------------*/
if ( !function_exists( 'sr_favicon' ) ) {
    function sr_favicon() {
    	global $prefix;
    	if (get_option($prefix . '_favicon') != '') {
    	echo '<link rel="shortcut icon" href="'. get_option($prefix . '_favicon') .'"/>'."\n";
    	}
    	else { ?>
    	<link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri() ?>/images/favicon.ico" />
    	<?php }
    }
    add_action('wp_head', 'sr_favicon');
}



/*-----------------------------------------------------------------------------------*/
/* Show analytics code
/*-----------------------------------------------------------------------------------*/
if ( !function_exists( 'sr_analytics' ) ) {
    function sr_analytics(){
    	global $prefix;
    	$output = get_option($prefix . '_analytics');
    	if ( $output <> "" ) 
    		echo stripslashes($output) . "\n";
    }
    add_action('wp_footer','sr_analytics');
}



/*-----------------------------------------------------------------------------------*/
/*	Passwort protection
/*-----------------------------------------------------------------------------------*/
if ( !function_exists( 'content' ) ) {
	function sr_password_form() {
		global $post;
		$label = 'pwbox-'.( empty( $post->ID ) ? rand() : $post->ID );
		$o = '<form class="protected-post-form checkform" action="' . get_option( 'siteurl' ) . '/wp-login.php?action=postpass" method="post">
		<div class="form-row clearfix">
			<label for="comment_form" class="req">'.__( "To view this protected post, enter the password below:", "sr_mila_theme" ).'</label>
			<div class="form-value"><input name="post_password" id="' . $label . '" type="password" size="20" /><div class="form-value"></div>
		<div class="form-row clearfix"><input type="submit" name="Submit" value="' . __( "Submit", "sr_mila_theme" ) . '" /></div>
		</form>
		';
		echo $o;
	}
}
add_filter( 'the_password_form', 'sr_password_form' );




/*-----------------------------------------------------------------------------------*/
/*	Remove "Protected" from Title
/*-----------------------------------------------------------------------------------*/
function the_title_trim($title) {
	$title = esc_attr($title);
	$findthese = array(
		'#Protected:#',
		'#Private:#'
	);
	$replacewith = array(
		'', // What to replace "Protected:" with
		'' // What to replace "Private:" with
	);
	$title = preg_replace($findthese, $replacewith, $title);
	return $title;
}
add_filter('the_title', 'the_title_trim');





/*-----------------------------------------------------------------------------------*/
/*	Custom function to limit the content
/*-----------------------------------------------------------------------------------*/
if ( !function_exists( 'content' ) ) {
	function content($contenttype,$limit,$readmore) {
		global $prefix;
		if ($contenttype == 'content') { $content = get_the_content(); }
		if ($contenttype == 'excerpt') { $content = get_the_excerpt(); }
		$content = preg_replace('/<img[^>]+./','', $content);
		$content = preg_replace( '/<blockquote>.*<\/blockquote>/', '', $content );
		
		if ($readmore) { $redmorelink = '<a href="'.get_permalink().'" class="color readmore">'.$readmore.'</a>'; } else { $redmorelink = ''; }
		
		$content = explode(' ', $content, $limit);
		if (count($content)>=$limit) {
			array_pop($content);
			$content = implode(" ",$content).'... ';
		} else {
			$content = implode(" ",$content);
		}	
		$content = preg_replace('/\[.+\]/','', $content);
		$content = apply_filters('the_content', $content); 
		$content = str_replace(']]>', ']]&gt;', $content);
		
		return $content.$redmorelink;
	}
}



/*-----------------------------------------------------------------------------------*/
/* Add meta datas for social share
/*-----------------------------------------------------------------------------------*/
if ( !function_exists( 'sr_get_social_metas' ) ) {
    function sr_get_social_metas() {
        global $prefix;
        
		$customcss = '';
        
        // get post id
        global $wp_query;
        if( is_single() ) {
            $postid = $wp_query->post->ID;
			
			$og_title = get_the_title( $postid );
			$og_desc = get_post($postid);
			$og_desc = content('excerpt', 20, '');
			$og_desc = strip_tags($og_desc);
			$og_url = get_permalink( $postid );
			$og_img = wp_get_attachment_image_src( get_post_thumbnail_id( $postid ), 'medium' );;
			$og_img = $og_img[0];
			
			echo '
			<meta property="og:site_name" content="'.get_bloginfo('name').'" />
			<meta property="og:title" content="'.$og_title.'" />
			<meta property="og:description" content="'.$og_desc.'" />
			<meta property="og:url" content="'.$og_url.'" />
			<meta property="og:image" content="'.$og_img.'" />';
			
        }
	
    }

}




/*-----------------------------------------------------------------------------------*/
/* Get Current title
/*-----------------------------------------------------------------------------------*/
if ( !function_exists( 'sr_getTitle' ) ) {
	function sr_getTitle() {
		
		$sr_titles = array();
		
		if (is_tag()) {
			$sr_titles['tax'] = single_tag_title('', false);
			$sr_titles['title'] = __("Tag", 'sr_mila_theme');
		} else if (is_category()) {
			$sr_titles['tax'] = single_cat_title('', false);
			$sr_titles['title'] = __("Category", 'sr_mila_theme');
		} else if (is_search()) {
			$sr_titles['tax'] = get_search_query();
			$sr_titles['title'] = __("Search", 'sr_mila_theme');
		} else if (is_tax('portfolio_category')) {
			$the_tax = get_term_by( 'slug', get_query_var( 'portfolio_category' ) , 'portfolio_category');
			$sr_titles['tax'] = $the_tax->name;
			$sr_titles['title'] = __("Portfolio", 'sr_mila_theme');
		} else if (is_archive()) {
			$sr_titles['tax'] = single_month_title(' ', false);
			$sr_titles['title'] = __("Archive", 'sr_mila_theme');
		} else if (is_home()) {
			if (get_option('page_for_posts') > 0) {
				$blog = get_post(get_option('page_for_posts'));
				$title = $blog->post_title;
			} else {
				$title = __("Home", 'sr_mila_theme');
			}
			$sr_titles['tax'] = false;
			$sr_titles['title'] = $title;
		} else {
			$sr_titles['tax'] = false;
			$sr_titles['title'] = get_the_title();
		}
		
		return $sr_titles;
	}
}



/*-----------------------------------------------------------------------------------*/
/* Custom post type parent fix
/*-----------------------------------------------------------------------------------*/
if ( !function_exists( 'remove_parent' ) ) {
	function remove_parent($var)
	{
		// check for current page values, return false if they exist.
		if ($var == 'current_page_parent' || $var == 'current-menu-item' || $var == 'current-page-ancestor') { return false; }
		return true;
	}
}

if ( !function_exists( 'sr_remove_class_from_menu' ) ) {
	function sr_remove_class_from_menu($classes, $item)
	{
		if (is_singular('portfolio') || is_singular('gallery') || is_tax('gallery_category') && $item->title == 'Blog' )
		{
			// we're viewing a custom post type, so remove the 'current-page' from all menu items.
			$classes = array_filter($classes, "remove_parent");
		}
		return $classes;
	}
}
add_filter('nav_menu_css_class', 'sr_remove_class_from_menu', 10, 2);




/*-----------------------------------------------------------------------------------*/
/*	Get related posts by taxonomy
/*-----------------------------------------------------------------------------------*/
if ( !function_exists( 'sr_related_posts' ) ) {
    function sr_related_posts($post_id, $taxonomy, $args=array()) {
		$query = new WP_Query();
		
        $terms = wp_get_object_terms($post_id, $taxonomy);
		$terms_array = array();
		foreach ($terms as $t) { $terms_array[] = $t->slug;}
        if (count($terms)) {
        
		$post = get_post($post_id);
        $args = wp_parse_args($args,array(
            'post_type' => $post->post_type, // The assumes the post types match
            'post__not_in' => array($post_id),
            'taxonomy' => $taxonomy,
			'tax_query'=> array(
				array(
				  'taxonomy' => $taxonomy,
				  'field' => 'slug',
				  'terms' => $terms_array
				)
			  )
        ));
		
        $query = new WP_Query($args);
        }
        return $query;
    }
}




/*-----------------------------------------------------------------------------------*/
/*	Customize Comment form
/*-----------------------------------------------------------------------------------*/
if ( !function_exists( 'sr_comment_fields' ) ) {
	function sr_comment_fields($fields) {
		foreach($fields as $field){
			 // Add the class to your field's input
		}
		
		$fields =  array(
			'author' => '<div class="form-row clearfix">
						 <label for="author" class="req">'.__('Name', 'sr_mila_theme').' *</label>
						 <div class="form-value"><input type="text" name="author" class="author" id="author" value=""/></div>
						 </div>',
			'email'  => '<div class="form-row clearfix">
						 <label for="email" class="req">'.__('Email', 'sr_mila_theme').' *</label>
						 <div class="form-value"><input type="text" name="email" class="email" id="email" value=""/></div>
						 </div>',
			'url'    => ''
		);
		
		return $fields;
	}
}
add_filter('comment_form_default_fields','sr_comment_fields');


if ( !function_exists( 'sr_comment_form_after' ) ) {
	function sr_comment_form_after(){
		echo '<p id="form-note">
					<span class="error_icon">'.__('Error', 'sr_mila_theme').'</span>
					<span class="error_message"><strong>'.__('Please check your entries!', 'sr_mila_theme').'</strong></span>
				</p>';
	}
}
add_action( 'comment_form_after', 'sr_comment_form_after' );


$comments_defaults = array( 
	'comment_field' => '<div class="form-row clearfix textbox">
						<label for="comment_form" class="req">'.__('Comment', 'sr_mila_theme').' *</label>
						<div class="form-value"><textarea name="comment" class="comment_form" id="comment_form" rows="15" cols="50"></textarea></div>
						</div>',
	'comment_notes_before'  => '',
	'comment_notes_after'  => '',
	'title_reply'          => '',
	'title_reply_to'       => '',
	'cancel_reply_link'    => __( 'Click here to cancel reply', 'sr_mila_theme' ),
	'label_submit'         => __( 'Post Comment', 'sr_mila_theme' )
);
	



/*-----------------------------------------------------------------------------------*/
/*	Ajax Comment
/*-----------------------------------------------------------------------------------*/
add_action('comment_post', 'ajax_comments',20, 2);
function ajax_comments($comment_ID, $comment_status){
	if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
		
		switch($comment_status){
			case '0':
				wp_notify_moderator($comment_ID);
			break;
			
			case '1': //Approved comment
				echo '<span class="confirm_icon">'.__('Confirm', 'sr_mila_theme').'</span>
					  <span class="confirm_message"><strong>'.__( 'Your Comment has ben sent. Thank you.', 'sr_mila_theme' ).'</strong></span>';
				$commentdata= &get_comment($comment_ID, ARRAY_A);
				$post= &get_post($commentdata['comment_post_ID']); 
				wp_notify_postauthor($comment_ID, $commentdata['comment_type']);
				break;
			
			default:
				echo "error";
		}
		exit;
		
	}
}


?>