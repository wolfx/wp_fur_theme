<?php

/*-----------------------------------------------------------------------------------

	Custom Post/Portfolio Meta boxes

-----------------------------------------------------------------------------------*/


/*-----------------------------------------------------------------------------------*/
/*	Add metaboxes
/*-----------------------------------------------------------------------------------*/

function add_meta_medias() {  
    add_meta_box(  
        'meta_medias', // $id  
        __('Medias', 'sr_mila_theme'), // $title  
        'show_meta_medias', // $callback  
        'post', // $page  
        'normal', // $context  
        'high'); // $priority	
	
	add_meta_box(  
        'meta_medias', // $id  
        __('Medias', 'sr_mila_theme'), // $title  
        'show_meta_medias', // $callback  
        'portfolio', // $page  
        'normal', // $context  
        'high'); // $priority	
}  
add_action('add_meta_boxes', 'add_meta_medias');

function add_meta_gallery() {  
    add_meta_box(  
        'meta_gallery', // $id  
        __('Gallery', 'sr_mila_theme'), // $title  
        'show_meta_gallery', // $callback  
        'gallery', // $page  
        'normal', // $context  
        'high'); // $priority	
}  
add_action('add_meta_boxes', 'add_meta_gallery');

function add_meta_audio() {  
    add_meta_box(  
        'meta_audio', // $id  
        __('Audio', 'sr_mila_theme'), // $title  
        'show_meta_audio', // $callback  
        'post', // $page  
        'normal', // $context  
        'high'); // $priority
	
}  
add_action('add_meta_boxes', 'add_meta_audio');

function add_meta_video() {  
    add_meta_box(  
        'meta_video', // $id  
        __('Video', 'sr_mila_theme'), // $title  
        'show_meta_video', // $callback  
        'post', // $page  
        'normal', // $context  
        'high'); // $priority
	
}  
add_action('add_meta_boxes', 'add_meta_video');

function add_meta_quote() {  
    add_meta_box(  
        'meta_quote', // $id  
        __('Quote', 'sr_mila_theme'), // $title  
        'show_meta_quote', // $callback  
        'post', // $page  
        'normal', // $context  
        'high'); // $priority
	
}  
add_action('add_meta_boxes', 'add_meta_quote');

function add_meta_link() {  
    add_meta_box(  
        'meta_link', // $id  
        __('Link', 'sr_mila_theme'), // $title  
        'show_meta_link', // $callback  
        'post', // $page  
        'normal', // $context  
        'high'); // $priority
	
}  
add_action('add_meta_boxes', 'add_meta_link');

function add_meta_portfoliolink() {  
    add_meta_box(  
        'meta_portfoliolink', // $id  
        __('Link to Website', 'sr_mila_theme'), // $title  
        'show_meta_portfoliolink', // $callback  
        'portfolio', // $page  
        'normal', // $context  
        'high'); // $priority */
}  
add_action('add_meta_boxes', 'add_meta_portfoliolink');

function add_meta_sidefeatures() {  
    add_meta_box(  
        'meta_sidefeatures', // $id  
        __('Side Page Area', 'sr_mila_theme'), // $title  
        'show_meta_sidefeatures', // $callback  
        'page', // $page  
        'normal', // $context  
        'high'); // $priority */
}  
add_action('add_meta_boxes', 'add_meta_sidefeatures');




/*-----------------------------------------------------------------------------------*/
/*	Define fields
/*-----------------------------------------------------------------------------------*/

$prefix = '_sr';  


$meta_medias = array(  
	array(  
	    'label' => __("Medias / Gallery", 'sr_mila_theme'),  
	    'desc'  => __("Add images or embedded videos, then drag and drop to arrange them.", 'sr_mila_theme'),  
	    'id'    => $prefix.'_medias',  
	    'type'  => 'medias'  
	)
 );

$meta_gallery = array(  
	array(  
	    'label' => __("Gallery", 'sr_mila_theme'),  
	    'desc'  => __("Add the gallery images, then drag and drop to arrange them.", 'sr_mila_theme'),  
	    'id'    => $prefix.'_gallery',  
	    'type'  => 'gallery'  
	)
 );


$meta_audio = array(  
	array(  
		"label" => __("MP3 File URL", 'sr_mila_theme'),
	   	"desc" => __("The url to the mp3 file", 'sr_mila_theme'),
	    'id'    => $prefix.'_audio_mp3',  
	    'type'  => 'text'  
	),
	array(  
		"label" => __("OGA/OGG File URL", 'sr_mila_theme'),
	   	"desc" => __("The url to the oga/ogg file", 'sr_mila_theme'),
	    'id'    => $prefix.'_audio_oga',  
	    'type'  => 'text'  
	)
 );


$meta_video = array(  
	array(  
		"label" => __("Embedded Video", 'sr_mila_theme'),
	   	"desc" => __("Include the embedded iframe.", 'sr_mila_theme'),
	    'id'    => $prefix.'_video',  
	    'type'  => 'textarea'  
	),
	array(  
		"label" => __("", 'sr_mila_theme'),
	   	"desc" => __("Or self hosted video.  Please leave these fields empty if you don't want to show an self hosted video.", 'sr_mila_theme'),
	    'type'  => 'info'  
	),
	array(  
		"label" => __("M4V File URL", 'sr_mila_theme'),
	   	"desc" => __("The url to the M4V file", 'sr_mila_theme'),
	    'id'    => $prefix.'_video_m4v',  
	    'type'  => 'text'  
	),
	array(  
		"label" => __("OGA/OGG File URL", 'sr_mila_theme'),
	   	"desc" => __("The url to the oga/ogg file", 'sr_mila_theme'),
	    'id'    => $prefix.'_video_oga',  
	    'type'  => 'text'  
	),
	array(  
		"label" => __("WEBMV File URL", 'sr_mila_theme'),
	   	"desc" => __("The url to the webmv file", 'sr_mila_theme'),
	    'id'    => $prefix.'_video_webmv',  
	    'type'  => 'text'  
	)
 );


$meta_quote = array(  
	array(  
		"label" => __("Quote", 'sr_mila_theme'),
	   	"desc" => __("Enter the quote.", 'sr_mila_theme'),
	    'id'    => $prefix.'_quote',  
	    'type'  => 'textarea'  
	)
 );


$meta_link = array(  
	array(  
		"label" => __("Link", 'sr_mila_theme'),
	   	"desc" => __("Link url", 'sr_mila_theme'),
	    'id'    => $prefix.'_link',  
	    'type'  => 'text'  
	)
 );

$meta_portfoliolink = array(  
	array(  
		"label" => __("Link", 'sr_mila_theme'),
	   	"desc" => __("Link to the a file download or live project. Please use 'http://'", 'sr_mila_theme'),
	    'id'    => $prefix.'_sidebar_link',  
	    'type'  => 'text'  
	),
	array(  
		"label" => __("Link Name", 'sr_mila_theme'),
	   	"desc" => __("Name for Link", 'sr_mila_theme'),
	    'id'    => $prefix.'_sidebar_linkname',  
	    'type'  => 'text'  
	)
 );

$meta_sidefeatures = array(  
	array(  
		 "label" => __("Add Side page area", 'sr_mila_theme'),
	   	 "desc" => __('Add the features to the side page area.  If this is empty the content will display on fullwidth.', 'sr_mila_theme'),
         'id'    => $prefix.'_sidefeatures',  
         'type'  => 'sortable',
		 'options' => array( 
							array( "element" => "googlemap", "name" => __('Google Map', 'sr_mila_theme'), "fields" => array(
								array(	"name" 	=> __('Title', 'sr_mila_theme'),
										"id"	=> 'title',
										"type"	=> 'text' ),
								array(	"name" 	=> __('Latitude & Longitude', 'sr_mila_theme'),
										"id"	=> 'latlong',
										"type"	=> 'text' ),
								array(	"name" 	=> __('Map Info', 'sr_mila_theme'),
										"id"	=> 'mapinfo',
										"type"	=> 'textarea' )
							)),
							array( "element" => "background", "name" => __('Background Image', 'sr_mila_theme'), "fields" => array(
								array(	"name" 	=> __('Title', 'sr_mila_theme'),
										"id"	=> 'title',
										"type"	=> 'text' ),
								array(	"name" 	=> __('Image', 'sr_mila_theme'),
										"id"	=> 'image',
										"type"	=> 'singleimage' )
							)),
							array( "element" => "custom", "name" => __('Custom', 'sr_mila_theme'), "fields" => array(
								array(	"name" 	=> __('Title', 'sr_mila_theme'),
										"id"	=> 'title',
										"type"	=> 'text' ),
								array(	"name" 	=> __('Code', 'sr_mila_theme'),
										"id"	=> 'code',
										"type"	=> 'textarea' )
							))
						),
		 					
							
     )
 );




/*-----------------------------------------------------------------------------------*/
/*	Callback Show fields
/*-----------------------------------------------------------------------------------*/

function show_meta_medias() {  
 	global $meta_medias, $post;  
 	// Use nonce for verification  
 	echo '<input type="hidden" name="meta_medias_nonce" value="'.wp_create_nonce(basename(__FILE__)).'" />';  
   	show_fields($meta_medias);  
}


function show_meta_gallery() {  
 	global $meta_gallery, $post;  
 	// Use nonce for verification  
 	echo '<input type="hidden" name="meta_gallery_nonce" value="'.wp_create_nonce(basename(__FILE__)).'" />';  
   	show_fields($meta_gallery);  
}


function show_meta_audio() {  
 	global $meta_audio, $post;  
 	// Use nonce for verification  
 	echo '<input type="hidden" name="meta_audio_nonce" value="'.wp_create_nonce(basename(__FILE__)).'" />';  
	show_fields($meta_audio);  
}


function show_meta_video() {  
 	global $meta_video, $post;  
 	// Use nonce for verification  
 	echo '<input type="hidden" name="meta_video_nonce" value="'.wp_create_nonce(basename(__FILE__)).'" />';  
   	show_fields($meta_video);  
}


function show_meta_quote() {  
 	global $meta_quote, $post;  
 	// Use nonce for verification  
 	echo '<input type="hidden" name="meta_quote_nonce" value="'.wp_create_nonce(basename(__FILE__)).'" />';  
    show_fields($meta_quote);  
}


function show_meta_link() {  
 	global $meta_link, $post;  
 	// Use nonce for verification  
 	echo '<input type="hidden" name="meta_link_nonce" value="'.wp_create_nonce(basename(__FILE__)).'" />';  
    show_fields($meta_link);  
}


function show_meta_portfoliolink() {  
 	global $meta_portfoliolink, $post;  
 	// Use nonce for verification  
 	echo '<input type="hidden" name="meta_portfoliolink_nonce" value="'.wp_create_nonce(basename(__FILE__)).'" />';  
    show_fields($meta_portfoliolink);  
}


function show_meta_sidefeatures() {  
 	global $meta_sidefeatures, $post;
	$prefix = 'sr';
 	// Use nonce for verification  
 	echo '<input type="hidden" name="meta_topfeature_nonce" value="'.wp_create_nonce(basename(__FILE__)).'" />';  
   	show_fields($meta_sidefeatures);  
}



/*-----------------------------------------------------------------------------------*/
/*	Show fields
/*-----------------------------------------------------------------------------------*/
function show_fields($a) {
 	global $post; 
	
	// Begin the field table and loop  
    echo '<table class="form-table">';  
    foreach ($a as $field) {  
		
		if ($field['type'] == 'info') {
			echo '<tr><th></th><td><br>------------------------<br><b>'.$field['desc'].'</b><br>------------------------</td></tr>';
		} else {
			
    	// get value of this field if it exists for this post  
    	$meta = get_post_meta($post->ID, $field['id'], true);  
    	// begin a table row with  
    	echo '<tr> 
    			<th><label for="'.$field['id'].'"><b>'.$field['label'].'</b></label><br /><span class="sr_description">'.$field['desc'].'</span></th> 
    			<td>';  
    			switch($field['type']) {  
                    					
					// text
    				case 'text':  
						echo '<input type="text" name="'.$field['id'].'" id="'.$field['id'].'" value="'.$meta.'" size="30" />';  
					break;
					
					// textarea
					case 'textarea':  
					    echo '<textarea name="'.$field['id'].'" id="'.$field['id'].'" cols="60" rows="4">'.$meta.'</textarea>';  
					break;
					
					// select
					case 'select':  
					    echo '<select name="'.$field['id'].'" id="'.$field['id'].'">';
						$i = 0;
						foreach ($field['option'] as $var) {
							if ($meta == $var['value']) { $selected = 'selected="selected"'; } else { if ($meta == '' && $i == 0) { $selected = 'selected="selected"'; } else { $selected = '';  } }
							echo '<option value="'.$var['value'].'" '.$selected.' /> '.$var['name'].'</div>';
						$i++;	
						}			  
						echo '</select>';   
					break;	
					
					// gallery  
					case 'gallery':  
						echo '<div id="sortable-medias">';
						echo '	<input class="add_image button" type="button" value="'.__("Add Image", 'sr_mila_theme').'" /> 
								<textarea name="'.$field['id'].'" style="display: none;" class="media-value">'.$meta.'</textarea>';
						echo '<ul id="sortable" class="media-elements">';		
					    if ($meta) {
							$meta = explode('|||', $meta);
					        foreach($meta as $row) {
								$object = explode('~~', $row);
								$type = $object[0];
								$val = $object[1];
								if ($type == 'image') {
									$image = wp_get_attachment_image_src($val, 'thumbnail'); $image = $image[0];
									echo '<li class="ui-state-default" title="image"><img src="'.$image.'" class="'.$val.'"><div class="delete"><span></span></div></li>';
								} else if ($type == 'video') {
									echo '<li class="ui-state-default" title="video"><div class="move">move</div><textarea name="videovalue">'.$val.'</textarea><div class="delete"><span></span></div></li>';
								}
					        }  
					    }  
					    echo '</ul>';
						echo '</div>';				
					break;
					
					// medias  
					case 'medias':
						echo '<div id="sortable-medias">';
						echo '	<input class="add_image button" type="button" value="'.__("Add Image", 'sr_mila_theme').'" /> 
								<input class="add_video button" type="button" value="'.__("Add Video", 'sr_mila_theme').'" />
								<textarea name="'.$field['id'].'" style="display: none;" class="media-value">'.$meta.'</textarea>';
						echo '<ul id="sortable" class="media-elements">';		
					    if ($meta) {
							$meta = explode('|||', $meta);
					        foreach($meta as $row) {
								$object = explode('~~', $row);
								$type = $object[0];
								$val = $object[1];
								if ($type == 'image') {
									$image = wp_get_attachment_image_src($val, 'thumbnail'); $image = $image[0];
									echo '<li class="ui-state-default" title="image"><img src="'.$image.'" class="'.$val.'"><div class="delete"><span></span></div></li>';
								} else if ($type == 'video') {
									echo '<li class="ui-state-default" title="video"><div class="move">move</div><textarea name="videovalue">'.$val.'</textarea><div class="delete"><span></span></div></li>';
								}
					        }  
					    }  
					    echo '</ul>';
						echo '</div>';
					break;
					
					// sortable
    				case 'sortable':
						echo '<div id="sortable-elements">';
						
						// Create the selectbox + Count + Hiddenfield
						$hiddenfields = '';
						$elements = '';
						$count = 0;
						foreach ($field['options'] as $option) {
							$hiddenfields .= '<div class="'.$option['element'].'">
												<li class="ui-state-default" title="'.$option['element'].'">
												<div class="edit"><span></span></div><div class="move">'.$option['name'].' ()</div><div class="delete"><span></span></div>
												<div class="editcontent">';
							foreach ($option['fields'] as $f) {
								switch($f['type']) { 
									case 'text':
										$hiddenfields .= '<div class="row">
											<div class="rowtitle"><label for="'.$f['id'].'">'.$f['name'].'</label></div>
											<div class="rowvalue"><input type="text" name="'.$f['id'].'" class="'.$f['id'].'" value=""></div>
										</div>';
									break;
									case 'textarea':
										$hiddenfields .= '<div class="row">
											<div class="rowtitle"><label for="'.$f['id'].'">'.$f['name'].'</label></div>
											<div class="rowvalue"><textarea name="'.$f['id'].'" class="'.$f['id'].'"></textarea></div>
										</div>';
									break;
									case 'singleimage':
										$hiddenfields .= '<div class="row">
											<div class="rowtitle"><label for="'.$f['id'].'">'.$f['name'].'</label></div>
											<div class="rowvalue"><input type="text" name="'.$f['id'].'" class="'.$f['id'].'" value="">
												<input type="button" name="add-singleimage" class="add_singleimage" value="'.__('Add Image', 'sr_mila_theme').'"></div>
										</div>';
									break;
									case 'select':
										$hiddenfields .= '<div class="row">
											<div class="rowtitle"><label for="'.$f['id'].'">'.$f['name'].'</label></div>';
										$hiddenfields .= '<div class="rowvalue"><select name="'.$f['id'].'" class="'.$f['id'].'">';	
										$y = 0;
										foreach ($f['options'] as $var) {
											$hiddenfields .= '<option value="'.$var['value'].'" /> '.$var['name'].'</div>';
										$y++;	
										}			  
										$hiddenfields .= '</select></div></div>';   
									break;
									case 'sliderselect':  
										$hiddenfields .= '<div class="row">
											<div class="rowtitle"><label for="'.$f['id'].'">'.$f['name'].'</label></div>';
										$hiddenfields .= '<div class="rowvalue"><select name="'.$f['id'].'" class="'.$f['id'].'">
												<option value="no">'.__("Select Slider", 'sr_mila_theme').' ...</option>';
										  $pages = get_posts( array( 'post_type' => 'slider' ) ); 
										  foreach ( $pages as $page ) {
											$hiddenfields .= '<option value="' . $page->ID . '">'.$page->post_title.'</option>';
										  }
										$hiddenfields .= '</select></div></div>';   
									break;
								} # END switch $f
							}					
							$hiddenfields .= '</div>
												</li>
											</div>';
							
							$elements .= '<option value="'.$option['element'].'">'.$option['name'].'</option>';
							$count++;
							}
						if ($count > 1) { $elements = '<select name="element-sortable" class="element-sortable">'.$elements.'</select>'; }
						else { $elements = '<select name="element-sortable" class="element-sortable" style="display:none;">'.$elements.'</select>'; }
						// END	
						
						// display the saved values
						$values = explode('|||', $meta);
						echo '<ul id="sortable" class="visual-elements visual-slides">';
						foreach ($values as $v) {
							$item = explode('|', $v);
							
							foreach ($field['options'] as $option) {
								if ($option['element'] == $item[0]) {
									$value = explode('~~', $item[1]);
									
									echo '<li class="ui-state-default" title="'.$option['element'].'"><div class="edit"><span></span></div><div class="move">'.$option['name'].' ('.$value[0].')</div><div class="delete"><span></span></div>';
									echo '<div class="editcontent">';
									
									$i = 0;
									foreach ($option['fields'] as $f) {
										switch($f['type']) { 
											case 'text':
												echo '<div class="row">
													<div class="rowtitle"><label for="'.$f['id'].'">'.$f['name'].'</label></div>
													<div class="rowvalue"><input type="text" name="'.$f['id'].'" class="'.$f['id'].'" value="'.$value[$i].'"></div>
												</div>';
											break;
											case 'textarea':
												echo '<div class="row">
													<div class="rowtitle"><label for="'.$f['id'].'">'.$f['name'].'</label></div>
													<div class="rowvalue"><textarea name="'.$f['id'].'" class="'.$f['id'].'">'.$value[$i].'</textarea></div>
												</div>';
											break;
											case 'singleimage':
												echo '<div class="row">
													<div class="rowtitle"><label for="'.$f['id'].'">'.$f['name'].'</label></div>
													<div class="rowvalue"><input type="text" name="'.$f['id'].'" class="'.$f['id'].'" value="'.$value[$i].'">
														<input type="button" name="add-singleimage" class="add_singleimage" value="'.__('Add Image', 'sr_mila_theme').'"></div>
												</div>';
											break;
											case 'select':
												echo '<div class="row">
													<div class="rowtitle"><label for="'.$f['id'].'">'.$f['name'].'</label></div>';
												echo '<div class="rowvalue"><select name="'.$f['id'].'" class="'.$f['id'].'">';	
												$y = 0;
												foreach ($f['options'] as $var) {
													if ($value[$i] == $var['value']) { $selected = 'selected="selected"'; 
													} else { if ($value[$i] == '' && $y == 0) { $selected = 'selected="selected"'; } else { $selected = '';  } }
													echo '<option value="'.$var['value'].'" '.$selected.' /> '.$var['name'].'</div>';
												$y++;	
												}			  
												echo '</select></div></div>';   
											break;
											case 'sliderselect':  
												echo '<div class="row">
													<div class="rowtitle"><label for="'.$f['id'].'">'.$f['name'].'</label></div>';
												echo '<div class="rowvalue"><select name="'.$f['id'].'" class="'.$f['id'].'">
														<option value="no">'.__("Select Slider", 'sr_mila_theme').' ...</option>';
												  $pages = get_posts( array( 'post_type' => 'slider' ) ); 
												  foreach ( $pages as $page ) {
													if ($page->ID == $value[$i]) { $active = 'selected="selected"'; }  else { $active = ''; } 
													$option = '<option value="' . $page->ID . '" '.$active.'>';
													$option .= $page->post_title;
													$option .= '</option>';
													echo $option;
												  }
												echo '</select></div></div>';   
											break;
										} # END switch $f
										$i++;	
									}
									echo '</div></li>';
								}	
							}
						}
						echo '</ul>';
						// END
						
						echo '	<input type="button" name="add-element" class="add-element add-slide" value="'.__('Add Item', 'sr_mila_theme').'"/>
								'.$elements.'
								<textarea name="'.$field['id'].'" id="'.$field['id'].'" class="value-elements value-slides">'.$meta.'</textarea>';
						
						// HIDDEN ELEMENTS FOR JS
						echo '<div id="hiddenelements" style="display: none;">'.$hiddenfields.'</div>';
						
						echo '</div>';
					break;
					
					
					 
                 } //end switch  
    	 echo '</td></tr>';  
		} // END if info
	} // end foreach  
	echo '</table>'; // end table
}



/*-----------------------------------------------------------------------------------*/
/*	Save Datas
/*-----------------------------------------------------------------------------------*/

function save_meta_medias($post_id) {  
    global $meta_medias;  
  
    // verify nonce  
    if (!isset($_POST['meta_medias_nonce']) || !wp_verify_nonce($_POST['meta_medias_nonce'], basename(__FILE__))) 
        return $post_id; 
		
    // check autosave  
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)  
        return $post_id;
		
    // check permissions  
    if ('page' == $_POST['post_type']) {  
        if (!current_user_can('edit_page', $post_id))  
            return $post_id;  
        } elseif (!current_user_can('edit_post', $post_id)) {  
            return $post_id;  
    }  
  
    // loop through fields and save the data  
    foreach ($meta_medias as $field) {  
        $old = get_post_meta($post_id, $field['id'], true);  
        $new = $_POST[$field['id']];  
        if ($new && $new != $old) {  
            update_post_meta($post_id, $field['id'], $new);  
        } elseif ('' == $new && $old) {  
            delete_post_meta($post_id, $field['id'], $old);  
        }  
    } // end foreach  
}  
add_action('save_post', 'save_meta_medias');


function save_meta_gallery($post_id) {  
    global $meta_gallery;  
  
    // verify nonce  
    if (!isset($_POST['meta_gallery_nonce']) || !wp_verify_nonce($_POST['meta_gallery_nonce'], basename(__FILE__))) 
        return $post_id; 
		
    // check autosave  
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)  
        return $post_id;
		
    // check permissions  
    if ('page' == $_POST['post_type']) {  
        if (!current_user_can('edit_page', $post_id))  
            return $post_id;  
        } elseif (!current_user_can('edit_post', $post_id)) {  
            return $post_id;  
    }  
  
    // loop through fields and save the data  
    foreach ($meta_gallery as $field) {  
        $old = get_post_meta($post_id, $field['id'], true);  
        $new = $_POST[$field['id']];  
        if ($new && $new != $old) {  
            update_post_meta($post_id, $field['id'], $new);  
        } elseif ('' == $new && $old) {  
            delete_post_meta($post_id, $field['id'], $old);  
        }  
    } // end foreach  
}  
add_action('save_post', 'save_meta_gallery');



function save_meta_audio($post_id) {  
    global $meta_audio;  
  
    // verify nonce  
    if (!isset($_POST['meta_audio_nonce']) || !wp_verify_nonce($_POST['meta_audio_nonce'], basename(__FILE__))) 
        return $post_id; 
		
    // check autosave  
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)  
        return $post_id;
		
    // check permissions  
    if ('page' == $_POST['post_type']) {  
        if (!current_user_can('edit_page', $post_id))  
            return $post_id;  
        } elseif (!current_user_can('edit_post', $post_id)) {  
            return $post_id;  
    }  
  
    // loop through fields and save the data  
    foreach ($meta_audio as $field) {  
        $old = get_post_meta($post_id, $field['id'], true);  
        $new = $_POST[$field['id']];  
        if ($new && $new != $old) {  
            update_post_meta($post_id, $field['id'], $new);  
        } elseif ('' == $new && $old) {  
            delete_post_meta($post_id, $field['id'], $old);  
        }  
    } // end foreach  
}  
add_action('save_post', 'save_meta_audio');



function save_meta_video($post_id) {  
    global $meta_video;  
  
    // verify nonce  
    if (!isset($_POST['meta_video_nonce']) || !wp_verify_nonce($_POST['meta_video_nonce'], basename(__FILE__))) 
        return $post_id; 
		
    // check autosave  
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)  
        return $post_id;
		
    // check permissions  
    if ('page' == $_POST['post_type']) {  
        if (!current_user_can('edit_page', $post_id))  
            return $post_id;  
        } elseif (!current_user_can('edit_post', $post_id)) {  
            return $post_id;  
    }  
  
    // loop through fields and save the data  
    foreach ($meta_video as $field) {
		if ($field['type'] !== 'info') {
        $old = get_post_meta($post_id, $field['id'], true);  
        $new = $_POST[$field['id']];  
        if ($new && $new != $old) {  
            update_post_meta($post_id, $field['id'], $new);  
        } elseif ('' == $new && $old) {  
            delete_post_meta($post_id, $field['id'], $old);  
        }  
		}
    } // end foreach  
}  
add_action('save_post', 'save_meta_video');



function save_meta_quote($post_id) {  
    global $meta_quote;  
  
    // verify nonce  
    if (!isset($_POST['meta_quote_nonce']) || !wp_verify_nonce($_POST['meta_quote_nonce'], basename(__FILE__))) 
        return $post_id; 
		
    // check autosave  
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)  
        return $post_id;
		
    // check permissions  
    if ('page' == $_POST['post_type']) {  
        if (!current_user_can('edit_page', $post_id))  
            return $post_id;  
        } elseif (!current_user_can('edit_post', $post_id)) {  
            return $post_id;  
    }  
  
    // loop through fields and save the data  
    foreach ($meta_quote as $field) {  
        $old = get_post_meta($post_id, $field['id'], true);  
        $new = $_POST[$field['id']];  
        if ($new && $new != $old) {  
            update_post_meta($post_id, $field['id'], $new);  
        } elseif ('' == $new && $old) {  
            delete_post_meta($post_id, $field['id'], $old);  
        }  
    } // end foreach  
}  
add_action('save_post', 'save_meta_quote');



function save_meta_link($post_id) {  
    global $meta_link;  
  
    // verify nonce  
    if (!isset($_POST['meta_link_nonce']) || !wp_verify_nonce($_POST['meta_link_nonce'], basename(__FILE__))) 
        return $post_id; 
		
    // check autosave  
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)  
        return $post_id;
		
    // check permissions  
    if ('page' == $_POST['post_type']) {  
        if (!current_user_can('edit_page', $post_id))  
            return $post_id;  
        } elseif (!current_user_can('edit_post', $post_id)) {  
            return $post_id;  
    }  
  
    // loop through fields and save the data  
    foreach ($meta_link as $field) {  
        $old = get_post_meta($post_id, $field['id'], true);  
        $new = $_POST[$field['id']];  
        if ($new && $new != $old) {  
            update_post_meta($post_id, $field['id'], $new);  
        } elseif ('' == $new && $old) {  
            delete_post_meta($post_id, $field['id'], $old);  
        }  
    } // end foreach  
}  
add_action('save_post', 'save_meta_link');



function save_meta_portfoliolink($post_id) {  
    global $meta_portfoliolink;  
  
    // verify nonce  
    if (!isset($_POST['meta_portfoliolink_nonce']) || !wp_verify_nonce($_POST['meta_portfoliolink_nonce'], basename(__FILE__))) 
        return $post_id; 
		
    // check autosave  
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)  
        return $post_id;
		
    // check permissions  
    if ('page' == $_POST['post_type']) {  
        if (!current_user_can('edit_page', $post_id))  
            return $post_id;  
        } elseif (!current_user_can('edit_post', $post_id)) {  
            return $post_id;  
    }  
  
    // loop through fields and save the data  
    foreach ($meta_portfoliolink as $field) {  
        $old = get_post_meta($post_id, $field['id'], true);  
        $new = $_POST[$field['id']];  
        if ($new && $new != $old) {  
            update_post_meta($post_id, $field['id'], $new);  
        } elseif ('' == $new && $old) {  
            delete_post_meta($post_id, $field['id'], $old);  
        }  
    } // end foreach  
}  
add_action('save_post', 'save_meta_portfoliolink');


function save_meta_sidefeatures($post_id) {  
    global $meta_sidefeatures;  
  
    // verify nonce  
    if (!isset($_POST['meta_topfeature_nonce']) || !wp_verify_nonce($_POST['meta_topfeature_nonce'], basename(__FILE__))) 
        return $post_id; 
		
    // check autosave  
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)  
        return $post_id;
		
    // check permissions  
    if ('page' == $_POST['post_type']) {  
        if (!current_user_can('edit_page', $post_id))  
            return $post_id;  
        } elseif (!current_user_can('edit_post', $post_id)) {  
            return $post_id;  
    }  
  
    // loop through fields and save the data  
    foreach ($meta_sidefeatures as $field) {  
        $old = get_post_meta($post_id, $field['id'], true);  
		if (isset($_POST[$field['id']])) {
			$new = $_POST[$field['id']];  
			if ($new && $new != $old) {  
				update_post_meta($post_id, $field['id'], $new);  
			} elseif ('' == $new && $old) {  
				delete_post_meta($post_id, $field['id'], $old);  
			} 
		}
    } // end foreach  
}  
add_action('save_post', 'save_meta_sidefeatures');


/*-----------------------------------------------------------------------------------*/
/*	Register and load function javascript
/*-----------------------------------------------------------------------------------*/

if( !function_exists( 'sr_admin_meta_js' ) ) {
    function sr_admin_meta_js($hook) {
		global $pagenow;

		wp_register_script('functions-script', get_template_directory_uri() . '/theme-admin/functions/js/functions_script.js', 'jquery');
		wp_enqueue_script('functions-script');
		
		if ( (isset($_GET['post']) && $_GET['action'] == 'edit' && get_post_type( $_GET['post'] )  == 'post' ) || ($pagenow == 'post-new.php' && !isset($_GET['post_type'])) ) {
			wp_register_script('customfields-script', get_template_directory_uri() . '/theme-admin/functions/js/customfields_script.js', 'jquery');
			wp_enqueue_script('customfields-script');
		}
		
		wp_register_style('functions-style', get_template_directory_uri() . '/theme-admin/functions/css/functions.css');
		wp_enqueue_style('functions-style');
    }
    
    add_action('admin_enqueue_scripts','sr_admin_meta_js',10,1);
}


?>