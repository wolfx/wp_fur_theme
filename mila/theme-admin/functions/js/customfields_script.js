jQuery(function(jQuery) {  
  	
	jQuery("#formatdiv input").click(function () {
          var format = jQuery(this).val();
		  
		  	jQuery('#meta_gallery').hide();
		  	jQuery('#meta_medias').hide();
			jQuery('#meta_video').hide();
			jQuery('#meta_audio').hide();
			jQuery('#meta_quote').hide();
			jQuery('#meta_link').hide();
		  
		  if (format == 'gallery') { jQuery('#meta_medias').show(); }
		  if (format == 'video') { jQuery('#meta_video').show(); }
		  if (format == 'audio') { jQuery('#meta_audio').show(); }
		  if (format == 'quote') { jQuery('#meta_quote').show(); }
		  if (format == 'link') { jQuery('#meta_link').show(); }
		  
        })
  
}); 

jQuery(document).ready(function(jQuery) {  
  	
	jQuery('#meta_gallery').hide();
	jQuery('#meta_medias').hide();
	jQuery('#meta_video').hide();
	jQuery('#meta_audio').hide();
	jQuery('#meta_quote').hide();
	jQuery('#meta_link').hide();
	
	jQuery("#formatdiv input").each(function () { 
		var format = jQuery(this).val();
		var checked = jQuery(this).attr('checked');
		
		if (checked == 'checked') {
			 jQuery('#meta_'+format).show();
			 if (format == 'gallery') {
				 jQuery('#meta_medias').show();
				 }
		}
	});
  
});