<?php

global $prefix;

$quote = get_post_meta($post->ID, $prefix.'_quote', true);

?>
<div class="quote">
	<div class="quote-text"><i><?php echo $quote; ?></i></div>
    <div class="quote-author">- <?php the_title(); ?></div>
</div>