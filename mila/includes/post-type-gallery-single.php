<?php

global $prefix;

$gallery = get_post_meta($post->ID, $prefix.'_medias', true);
$postdisplay = get_option($prefix.'_blogpostsdisplay');
$postid = $post->ID;


if( empty($gallery) ) {
?>	
	
<?php if(has_post_thumbnail()) { ?>
	<div class="entry-thumb">
		<?php the_post_thumbnail('fullwidth-thumb'); ?>
	</div>
<?php } ?>
    
<?php	
} else {	


$medias = explode('|||', $gallery);

$output_medias = '';
foreach ($medias as $media) {
	$object = explode('~~', $media);
	$type = $object[0];
	$val = $object[1];
	
	$output_medias .= "<li>"; 
	if ($type == 'image') { 
		$image = wp_get_attachment_image_src($val, 'fullwidth-thumb'); $image = $image[0];
		$thisimage = '<img src="'.$image.'" alt="'.get_the_title($image[1]).'"/>';
		if(get_option($prefix.'_blogpostsdisablefancybox') !== "on") {
			$output_medias .= '<div class="imgoverlay"><a href="'.$image.'" class="openfancybox" rel="gallery'.get_the_ID().'" title="'.get_the_title($image[1]).'">'.$thisimage.'</a></div>';
		} else {
			$output_medias .= $thisimage;
		}
	} else {
		$output_medias .= '<div class="embeddedvideo">'.$val.'</div>';
	}
	$output_medias .= "</li>";
}

?>
   	<?php if(get_option($prefix.'_blogpostgallerydisplay') !== "list" ) {  ?> 
    <div class="entry-thumb">
        <div id="slider-single<?php echo $postid; ?>" class="slider blogslider">        
            <div class="flexslider">
                <ul class="slides">
                    <?php echo $output_medias; ?>
                </ul>
            </div>
        </div>
    </div>
    <?php } else { ?>
    <div class="entry-thumb">
        <ul class="gallery-list">
            <?php echo $output_medias; ?>
        </ul>
    </div>
    <?php } ?>
        
<?php } ?>