<?php
//get global prefix
global $prefix;
global $query;


if ($query) { $query = $query; } else { $query = $wp_query; }

while ($query->have_posts()) : $query->the_post(); 

$format = get_post_format(); 
if( false === $format ) { $format = 'standard'; }

	// GET CATEGORIES
	$categories = wp_get_object_terms($post->ID, 'category'); 
	$current_cats = '';
	$masonry_cats = '';
	foreach($categories as $cat){
		$category = $cat->name; 
		$current_cats .= $category.', ';
		$category = strtolower($category);
		$replace = array(" ", "'", '"', "&amp;",  "amp;", "&");
		$category = str_replace($replace, "", $category);
		$masonry_cats .= $category.' ';
	}
	$current_cats = substr($current_cats, 0, -2);
	
?>  

				<div class="masonry-item entry blog-masonry blog-entry <?php echo $masonry_cats; ?>">
                	
                    <?php if(get_option($prefix.'_blogentriesstyle') !== "portfolio" ) {  ?> 
                    <div class="masonry-inner">
                    <div <?php post_class(); ?>>
                    	
					<?php 
                    // ----- IF FORMAT IS standard,gallery,image,video,audio
                    if( $format == 'standard' || $format == 'gallery' || $format == 'image' || $format == 'video' || $format == 'audio' ) { ?>
                        
                        <?php if ( (get_option($prefix.'_blogentriesdisplay') == 'featuredimage' || $format == 'standard' || $format == 'image')  && has_post_thumbnail()) { ?>
                        <div class="entry-thumb">
                            <div class="imgoverlay">
                                <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" class="loadcontent" data-id="<?php echo get_the_ID(); ?>" data-slug="<?php echo $post->post_name; ?>" data-type="post"><?php the_post_thumbnail('blog-thumb'); ?></a>
                            </div>
                        </div>
                        <?php } else {
							get_template_part( 'includes/post-type', $format );		
						} ?>
                                                
                        <div class="entry-title">
                            <h5><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" class="loadcontent" data-id="<?php echo get_the_ID(); ?>" data-slug="<?php echo $post->post_name; ?>" data-type="post"><?php the_title(); ?></a></h5>
                        </div>
                        
                        <?php if ( !get_option($prefix.'_blogpostsdisabledate')) { ?>
                        <div class="meta_date"><?php the_time(get_option('date_format')); ?>
							<?php 
								// CATEGORY
								if ( !get_option($prefix.'_blogpostsdisablecategory')) {
									$categories = get_the_category();
									$separator = ',';
									$output = '';

									if($categories){
										foreach($categories as $category) {
											$output .= 	'<a href="'.get_category_link($category->term_id ).'" title="' . esc_attr( sprintf( __( "View all posts in %s", 'sr_mila_theme' ), $category->name ) ) . '">
														'.$category->cat_name.'</a>'.$separator;
										}
										echo ' <i>in</i> '.trim($output, $separator);
									}
								}
							?>
                        </div>
                        <?php } ?>
                        
                        <?php if ( ! post_password_required() ) { ?>
                        <div class="entry-text">
                            <?php 
                                $excerptlength 	= get_option($prefix.'_blogentriesexcerpt');
								$readmore	 	= get_option($prefix.'_blogentriesreadmore');
                                if (!$excerptlength) { $excerptlength = 20; }
                                if (!$readmore) { $readmoretext = __("Read more &rarr;", 'sr_mila_theme'); } else { $readmoretext = false; }
                                echo content('excerpt', $excerptlength, $readmoretext); ?>
                        </div>
                        <?php } ?>
                        
                        <ul class="entry-meta clearfix">
                                    
                            <?php if ( !get_option($prefix.'_blogpostsdisableviews')) { ?>
                            <li class="meta_views "><a href="<?php the_permalink(); ?>" title="Views" class="loadcontent <?php if (isset($_COOKIE["viewscookie".get_the_ID()])) { echo 'viewsactive'; } ?>" data-id="<?php echo get_the_ID(); ?>" data-slug="<?php echo $post->post_name; ?>" data-type="post">
                            <span class="icon"></span><?php echo sr_getPostMeta(get_the_ID(), 'views'); ?></a></li>
                            <?php } ?>
                            <?php if ( !get_option($prefix.'_blogpostsdisablelikes')) { ?>
                            <li class="meta_likes"><a href="<?php the_permalink(); ?>" title="I like it" <?php if (isset($_COOKIE["likescookie".get_the_ID()])) { echo 'class="likesactive"'; } ?> data-id="<?php echo get_the_ID(); ?>">
                            <span class="icon"></span><span class="amount"><?php echo sr_getPostMeta(get_the_ID(), 'likes'); ?></span></a></li>
                            <?php } ?>
                            <?php if ( get_option($prefix.'_commentsblog') == 'enabled' && $post->comment_status == 'open') { ?>
                            <li class="meta_comments"><a href="<?php the_permalink(); ?>#comments" title="Comments" class="loadcontent" data-id="<?php echo get_the_ID(); ?>" data-slug="<?php echo $post->post_name; ?>" data-type="post"><span class="icon"></span><?php echo comments_number('0', '1', '%'); ?></a></li>
                            <?php } ?>
                        
                        </ul>
                        
					<?php } // END IF FORMAT
                    else {
                        get_template_part( 'includes/post-type', $format );	
                    }
                    ?>
                        
                    </div>
                	</div>     <!-- .masonry_inner -->
                    
                    <?php // IF PORTFOLIO STYLE 
					} else { ?>
                    <div <?php post_class(); ?>>
                    	<?php if( $format == 'standard' || $format == 'gallery' || $format == 'image' || $format == 'video' || $format == 'audio' ) { ?>
                            <div class="imgoverlay">
                                <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" class="loadcontent" data-id="<?php echo get_the_ID(); ?>" data-slug="<?php echo $post->post_name; ?>" data-type="post"><?php the_post_thumbnail('blog-thumb');?>
                                <div class="overlaymeta">
                                    <h6><?php the_title(); ?></h6>
                                    <?php 
									if ( !get_option($prefix.'_blogpostsdisablecategory')) {
										$categories = get_the_category(); 
										$separator = ',';
										$catoutput = '';
										
										if($categories){
											$catoutput .= '<span class="meta-seperator"></span><div class="meta-category">';
											foreach($categories as $category) {
												$catoutput .= 	$category->cat_name.$separator;
											}
											$catoutput .= '</div>';
											echo trim($catoutput, $separator);
										}
									}
									?>
                                </div>
                                </a>
                            </div>
                            <?php } else {
                                get_template_part( 'includes/post-type', $format );		
                            } ?>
                    </div>
                    <?php } ?>
                        
                    	
                    
                </div> <!-- .entry -->
                
                
<?php endwhile; ?>