<?php

global $prefix;

$link = get_post_meta($post->ID, $prefix.'_link', true);

?>
<div class="link">
	<div class="link-target"><a href="<?php echo $link; ?>" ><?php echo the_content(); ?></a></div>
    <div class="link-name">- <?php echo the_title(); ?></div>
</div>