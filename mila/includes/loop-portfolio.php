<?php
//get global prefix
global $prefix;
global $query;


if ($query) { $query = $query; } else { $query = $wp_query; }

while ($query->have_posts()) : $query->the_post(); 
	
	// GET CATEGORIES
	$categories = wp_get_object_terms($post->ID, 'portfolio_category'); 
	$current_cats = '';
	$masonry_cats = '';
	foreach($categories as $cat){
		$category = $cat->name; 
		$current_cats .= $category.', ';
		$category = strtolower($category);
		$replace = array(" ", "'", '"', "&amp;",  "amp;", "&");
		$category = str_replace($replace, "", $category);
		$masonry_cats .= $category.' ';
	}
	$current_cats = substr($current_cats, 0, -2);

?>  

				<div class="masonry-item portfolio-entry portfolio-masonry <?php echo $masonry_cats; ?>">
                	<div <?php post_class(); ?>>
                        <div class="imgoverlay">
                            <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" class="loadcontent" data-id="<?php echo get_the_ID(); ?>" data-slug="<?php echo $post->post_name; ?>" data-type="portfolio">
                            <?php 
                                if (!get_option($prefix.'_portfoliocrop')) {
                                    the_post_thumbnail('portfolio-thumb');	
                                } else {
                                    the_post_thumbnail('blog-thumb');	
                                }
                             ?>
                            <div class="overlaymeta">
                                <h6><?php the_title(); ?></h6>
                                <span class="meta-seperator"></span>
                                <div class="meta-category"><?php echo $current_cats; ?></div>
                            </div>
                            </a>
                        </div>
                    </div>
                </div>
                
<?php endwhile; 
?>