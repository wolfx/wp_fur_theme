<?php 
$prefix = "_sr";

$standard_fonts = array('default', 'Arial','Lucida Sans Unicode','Times New Roman','Verdana', 'Helvetica', 'Georgia','Tahoma');

$bodyfont = get_option($prefix.'_bodyfont');
$headingfont = get_option($prefix.'_headingfont');
$navigationfont = get_option($prefix.'_navigationfont');

$fonts = array( $bodyfont,
				$headingfont,
				$navigationfont
				);

$active_fonts = array();

foreach($fonts as $font) {
if(!in_array($font, $standard_fonts) && !in_array($font, $active_fonts) && $font ) {
	$active_fonts[] = $font;
?>
<link href='http://fonts.googleapis.com/css?family=<?php echo str_replace(' ', '+', $font); ?>' rel='stylesheet' type='text/css'>
<?php 
} }

// Default font
if ($bodyfont == 'default' || $headingfont == 'default' || $navigationfont == 'default' || $bodyfont == '' || $headingfont == '' || $navigationfont == '') { ?>
<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
<?php }
?>