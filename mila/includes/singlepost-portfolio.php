<?php
//get global prefix
global $prefix;
global $query;

if ($query) { $query = $query; } else { $query = $wp_query; }

if (have_posts()) : while (have_posts()) : the_post();


####### GET METAS 
$slides = get_post_meta(get_the_ID(), $prefix.'_medias', true);
$link = get_post_meta(get_the_ID(), $prefix.'_sidebar_link', true);
$linkname = get_post_meta(get_the_ID(), $prefix.'_sidebar_linkname', true);
if (!$linkname) { $linkname = 'Visit Live Project'; }

?>  

				<div id="portfolio-single">
                    <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					
                    <?php if ( ! post_password_required() ) { ?>
					
					<?php
                        $slides = explode('|||',$slides);
                        
                        if (get_post_meta(get_the_ID(), $prefix.'_medias', true)) {
                            
                            $slider = '';
                            foreach ($slides as $s) {
                                $object = explode('~~', $s);
                                $type = $object[0];
                                $val = $object[1];
                                
                                $slider .= "<li>"; 
                                if ($type == 'image') { 
                                    $image = wp_get_attachment_image_src($val, 'fullwidth-thumb'); $image = $image[0];
                                    $thisimage = '<img src="'.$image.'" alt="'.get_the_title($image[1]).'"/>';
                                    if(get_option($prefix.'_portfoliodisablefancybox') !== "on") {
                                        $slider .= '<div class="imgoverlay"><a href="'.$image.'" class="openfancybox" rel="gallery'.get_the_ID().'" title="'.get_the_title($image[1]).'">'.$thisimage.'</a></div>';
                                    } else {
                                        $slider .= $thisimage;
                                    }
                                } else {
                                    $slider .= '<div class="embeddedvideo">'.$val.'</div>';
                                }
                                $slider .= "</li>";
                            }
                    ?>
                	
                    <?php if(get_option($prefix.'_portfoliodisplay') !== "list" ) {  ?> 
                    <div class="entry-media portfolio-media">
                        <div id="slider-<?php echo get_the_ID(); ?>" class="slider sliderportfolio">        
                            <div class="flexslider">
                                <ul class="slides">
                                    <?php echo $slider; ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <?php } else { ?>
                    <div class="entry-media portfolio-media">
                        <ul class="gallery-list">
                            <?php echo $slider; ?>
                        </ul>
                    </div>
                    <?php } ?>
                
                <?php } // END if (get_post_meta(get_the_ID(), $prefix.'_medias', true)) {  ?>
                    
                    <div class="entry clearfix">
                        <article class="portfolio-article">
                            
                                <div class="entry-content portfolio-content">
                                    <?php if(!get_option($prefix.'_portfoliodisablepagination')) { sr_singlepagination('portfolio','<div id="single-pagination" class="clearfix">','</div>'); } ?>
                                    <div class="entry-title">
                                        <h1><?php the_title(); ?></h1>
                                    </div>
                                    <div class="entry-text">
                                        <?php the_content(); ?>
                                    </div>
                                    <?php if ($link) { ?>
                                    <p><a class="more-link" href="<?php echo $link; ?>" title="<?php the_title(); ?>" target="_blank"><?php echo $linkname; ?></a></p>
                                    <?php } ?>
                                	
                                    <?php if(!get_option($prefix.'_portfoliodisableshare')) { ?>
										<?php sr_sharebutton(); ?>
                                    <?php  } ?>
                                    
                                </div> <!-- END entry-content -->
                        		
                                <?php 
									if (get_option($prefix.'_commentsportfolio') == 'enabled') {
										comments_template( '', true ); 
									}
								?>
                                
                        </article>
                    </div>
                    
                    <?php } else { // ELSE PASSWORD PROTECTION ?>
                    <div class="entry">
                    	<article class="portfolio-article">
                                <div class="entry-content portfolio-content">
                                	<div class="entry-title">
                                        <h1><?php the_title(); ?></h1>
                                    </div>
                    				
                                    <div class="entry-passwordprotected">
                                        <h4><?php _e("Password Protected", 'sr_mila_theme'); ?></h4>
                                    </div>
                                    <?php sr_password_form(); ?>
                    			</div> <!-- END entry-content -->
                        </article>
                    </div>		
					<?php }	 // END IF PASSWORD PROTECTION?>
                    
                    </div>
                </div>
                
<?php endwhile; endif; ?>