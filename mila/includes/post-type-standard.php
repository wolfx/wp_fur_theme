<?php if(has_post_thumbnail()) {  ?>
	
    <div class="entry-thumb">
			<?php the_post_thumbnail('fullwidth-thumb'); ?>
	</div>
    
<?php  } ?>
  