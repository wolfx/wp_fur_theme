<?php
//get global prefix
global $prefix;
global $query;

if ($query) { $query = $query; } else { $query = $wp_query; }

if (have_posts()) : while (have_posts()) : the_post();
		
?>  

				<div id="post-single">
                    <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                        
                    <?php if ( ! post_password_required() ) { ?>
                        
                        <?php 
							$format = get_post_format(); 
							if( false === $format ) { $format = 'standard'; }
						?>
                        
                        <?php 
						// ----- IF FORMAT IS standard,gallery,image,video,audio
						if( $format == 'standard' || $format == 'gallery' || $format == 'image' || $format == 'video' || $format == 'audio' ) { ?>
                    	
                        <div class="entry-media blog-media">
                    	<?php
							if($format == 'audio' || $format == 'video' || $format == 'gallery') {
								get_template_part( 'includes/post-type', $format.'-single' );
							} else {
								get_template_part( 'includes/post-type', $format );
							}
						?>
                        </div>
                                        
                        <div class="entry clearfix entry<?php echo $format; ?>">
                            <article class="post-article">
                                    
                                    <div class="entry-content post-content">                                        
                                        
                                        <?php if(!get_option($prefix.'_blogpostsdisablepagination')) { sr_singlepagination('blog','<div id="single-pagination" class="clearfix">','</div>'); } ?>
                                        
                                        <div class="entry-title">
                                            <h1><?php the_title(); ?></h1>
                                        </div>
                                        
                                        <?php if ( !get_option($prefix.'_blogpostsdisabledate')) { ?>
                                        <div class="meta_date"><?php the_time(get_option('date_format')); ?>
                                        	<?php 
												// CATEGORY
												if ( !get_option($prefix.'_blogpostsdisablecategory')) {
													$categories = get_the_category();
													$separator = ',';
													$output = '';
				
													if($categories){
														foreach($categories as $category) {
															$output .= 	'<a href="'.get_category_link($category->term_id ).'" title="' . esc_attr( sprintf( __( "View all posts in %s", 'sr_mila_theme' ), $category->name ) ) . '">
																		'.$category->cat_name.'</a>'.$separator;
														}
														echo ' <i>in</i> '.trim($output, $separator);
													}
												}
											?>
                                        </div>
                                        <?php } ?>
                                        
                                        
                                        <div class="entry-text">
                                            <?php the_content(); ?>
                                        </div>
                                        
                                        <ul class="entry-meta clearfix">
                                    
											<?php if ( !get_option($prefix.'_blogpostsdisableviews')) { ?>
                                            <li class="meta_views "><a href="<?php the_permalink(); ?>" title="Views" <?php if (isset($_COOKIE["viewscookie".get_the_ID()])) { echo 'class="viewsactive"'; } ?>>
                                            <span class="icon"></span><?php echo sr_getPostMeta(get_the_ID(), 'views'); ?></a></li>
                                            <?php } ?>
                                            <?php if ( !get_option($prefix.'_blogpostsdisablelikes')) { ?>
                                            <li class="meta_likes"><a href="<?php the_permalink(); ?>" title="I like it" <?php if (isset($_COOKIE["likescookie".get_the_ID()])) { echo 'class="likesactive"'; } ?> data-id="<?php echo get_the_ID(); ?>">
                                            <span class="icon"></span><span class="amount"><?php echo sr_getPostMeta(get_the_ID(), 'likes'); ?></span></a></li>
                                            <?php } ?>
                                            <?php if ( get_option($prefix.'_commentsblog') == 'enabled' && $post->comment_status == 'open') { ?>
                                            <li class="meta_comments"><a href="<?php the_permalink(); ?>#comments" title="Comments"><span class="icon"></span><?php echo comments_number('0', '1', '%'); ?></a></li>
                                            <?php } ?>
                                        
                                        </ul>
                                        
                                        <?php if(!get_option($prefix.'_blogpostsdisableshare')) { ?>
                                            <?php sr_sharebutton(); ?>
                                        <?php  } ?>
                        
                                    </div> <!-- END entry-content -->
                                    
                                    <?php 
                                        if (get_option($prefix.'_commentsblog') !== 'disabled') {
                                            comments_template( '', true ); 
                                        }
                                    ?>
                                        
                            </article>
                            
                        </div>
                        
                        <?php } // END IF FORMAT
						else {
							echo '<div class="entry clearfix entry-'.$format.'"><article><div class="entry-content post-content">';
							get_template_part( 'includes/post-type', $format );
							if(!get_option($prefix.'_blogpostsdisablepagination')) { sr_singlepagination('blog','<div id="single-pagination" class="clearfix">','</div>'); } 
							echo '</div>';
							if (get_option($prefix.'_commentsblog') !== 'disabled') {
								comments_template( '', true ); 
							}
							echo '</article></div>';							
						}
						?>
                    
                    <?php } else { // ELSE PASSWORD PROTECTION ?>
                        <div class="entry">
                            <article class="post-article">
                                    <div class="entry-content post-content">
                                        <div class="entry-title">
                                            <h1><?php the_title(); ?></h1>
                                        </div>
                                        
                                        <div class="entry-passwordprotected">
                                            <h4><?php _e("Password Protected", 'sr_mila_theme'); ?></h4>
                                        </div>
                                        <?php sr_password_form(); ?>
                                    </div> <!-- END entry-content -->
                            </article>
                        </div>		
                    <?php }	 // END IF PASSWORD PROTECTION ?>
                    
                    </div>
                </div>
                
<?php endwhile; endif; ?>