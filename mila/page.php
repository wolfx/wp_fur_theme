<?php
/*
* Main Page
*/

//get global prefix
global $prefix;

//get template header
get_header();

if (have_posts()) : while (have_posts()) : the_post(); 

$sidefeature = get_post_meta(get_the_id(), $prefix."_sidefeatures", true);

?>
		
        <?php 
			if ($sidefeature) {
				sr_sideFeature($sidefeature);
			}
		?>
        
        <div id="content" class="maincontent <?php if ($sidefeature) { echo 'halfcontent'; } else { echo 'fullcontent'; } ?>">
            <div class="content-inner">
                
                <div class="entry clearfix">
                    <article class="post-article">
                    
					<?php the_content(); ?>
                    <?php wp_link_pages('before=<p>&after=</p>&next_or_number=number&pagelink=page %'); ?>
                    
                    <?php 
                    if (get_option($prefix.'_commentspage') == 'enabled') {
                        comments_template( '', true ); 
                    }
                    ?>
                    
                    </article>
                </div>
        	</div>
		</div>
                
       

<?php endwhile; ?>
<?php endif; ?>

<?php get_footer(); ?>