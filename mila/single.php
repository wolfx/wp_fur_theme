<?php

//get global prefix
global $prefix;

// Count the views
if(!isset($_COOKIE["viewscookie".get_the_ID()]) && !isset($_POST['countlikes'])) { 
	sr_setPostMeta(get_the_ID(), 'views');
	setcookie("viewscookie".get_the_ID(), 'yes', time()+3600, '/');
}

//get template header
get_header();

?>
		
        <div id="content" class="maincontent">
         	<div class="content-inner">         	
        
             <?php   get_template_part( 'includes/singlepost', 'blog'); ?>
                
			</div>
         </div>
		

		<div id="blog" class="mainside">
            <div id="blog-grid" class="masonry clearfix">
            
                <?php
                /***********************
				QUERY BLOG ITEMS
				***********************/
				
				$query = new WP_Query(array(
						'post_type' => array('post')
					) );
                
                get_template_part( 'includes/loop', 'blog');
                
				wp_reset_postdata();
                ?> 
            </div>
            
            <?php $max_num_page = $query->max_num_pages; loadmore('post', $max_num_page); ?>
            
         </div>
         
        <div class="mainside-bg"></div>

<?php get_footer(); ?>