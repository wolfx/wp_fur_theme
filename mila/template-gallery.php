<?php
/*
* Template Name: Gallery
*/

//get global prefix
global $prefix;

//get template header
get_header();

if (have_posts()) : while (have_posts()) : the_post(); ?>

		<div id="gallery" class="mainside">
            <div id="gallery-grid" class="masonry clearfix">
            
			<?php the_content(); ?>
            
            </div>
        </div>
        
        <div class="mainside-bg"></div>

<?php endwhile; ?>
<?php endif; ?>

<?php get_footer(); ?>