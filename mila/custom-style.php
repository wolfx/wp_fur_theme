<?php 
$prefix = "_sr";
header("Content-type: text/css");
$absolute_path = __FILE__;
$path_to_file = explode( 'wp-content', $absolute_path );
$path_to_wp = $path_to_file[0];
require_once( $path_to_wp . '/wp-load.php' );



/***********************
GET CUSTOM COLOR
***********************/
$main_color = '#00a276';
if (get_option($prefix.'_customcolor')){
    $main_color = get_option($prefix.'_customcolor'); 
} 

$header_color = '#00a276';
if (get_option($prefix.'_headercolor')){
    $header_color = get_option($prefix.'_headercolor'); 
} 




/***********************
GET CUSTOM CSS
***********************/
if (get_option($prefix.'_customcss')){
    $customcss = get_option($prefix.'_customcss'); 
}  else {
	$customcss = '';
}



/***********************
GET CUSTOM FONTS AND SET DEFAULTS
***********************/
$bodyfont = get_option($prefix.'_bodyfont'); 
	if (!$bodyfont || $bodyfont == 'default') { $bodyfont = "'Helvetica Neue', Helvetica, Arial"; }
$bodyfontsize = get_option($prefix.'_bodyfontsize'); 
	if (!$bodyfontsize) { $bodyfontsize = "13px"; }
$bodyfontlineheight = get_option($prefix.'_bodyfontlineheight'); 
	if (!$bodyfontlineheight) { $bodyfontlineheight = "21px"; }
	
$headingfont = get_option($prefix.'_headingfont');
	if (!$headingfont || $headingfont == 'default') { $headingfont = "'Helvetica Neue', Helvetica, Arial"; }
$headingfontweight = get_option($prefix.'_headingfontweight'); 
	if (!$headingfontweight) { $headingfontweight = "bold"; }	

$navigationfont = get_option($prefix.'_navigationfont');
	if (!$navigationfont || $navigationfont == 'default') { $navigationfont = "'Helvetica Neue', Helvetica, Arial"; }
$navigationfontsize = get_option($prefix.'_navigationfontsize'); 
	if (!$navigationfontsize) { $navigationfontsize = "14px"; }	
$navigationfontweight = get_option($prefix.'_navigationfontweight'); 
	if (!$navigationfontweight) { $navigationfontweight = "bold"; }		
	
	


/***********************
GET CUSTOM SPACINGS
***********************/
$spacing = array();
$spacing['header'] = get_option($prefix.'_spacingheader'); 		if(!$spacing['header']) { $spacing['header'] = '30'; }
$spacing['menu'] = get_option($prefix.'_spacingmenu');			if(!$spacing['menu']) { $spacing['menu'] = '0'; }
$spacing['footer'] = get_option($prefix.'_spacingfooter');		if(!$spacing['footer']) { $spacing['footer'] = '15'; }


?>

/* CUSTOM FONTS */
body { font-family: <?php echo $bodyfont; ?>; font-size: <?php echo $bodyfontsize; ?>; line-height: <?php echo $bodyfontlineheight; ?>; }
h1, h2, h3, h4, h5, h6 { font-family: <?php echo $headingfont; ?>; font-weight: <?php echo $headingfontweight; ?>; }
nav#main-nav ul li > a  { font-family: <?php echo $navigationfont; ?>; font-size: <?php echo $navigationfontsize; ?>; font-weight: <?php echo $navigationfontweight; ?>; }


/* CUSTOM COLOR */
::selection { background: <?php echo $main_color; ?>; }
::-moz-selection { background: <?php echo $main_color; ?>; }
::-webkit-selection { background: <?php echo $main_color; ?>; }
input[type=submit]:hover { background: <?php echo $main_color; ?>; }
a { color: <?php echo $main_color; ?>; }
a.more-link:hover {	background: <?php echo $main_color; ?>; }
h1 a:hover, h2 a:hover, h3 a:hover, h4 a:hover, h5 a:hover, h6 a:hover { color: <?php echo $main_color; ?>; }
header { background: <?php echo $header_color; ?>; }
nav#main-nav ul .sub-menu li a:hover { color: <?php echo $main_color; ?>; }
#dropdown-menu { color: <?php echo $main_color; ?>; }
#responsive-nav li:hover > a, #responsive-nav li.current-menu-item > a, #responsive-nav li.current-menu-ancestor > a, #responsive-nav li.current_page_parent > a { color: <?php echo $main_color; ?>; }
#responsive-nav li ul li:hover > a, #responsive-nav li ul li.current-menu-item > a, #responsive-nav li ul li.current-menu-ancestor > a {  color: <?php echo $main_color; ?>; }
#totop { background-color: <?php echo $main_color; ?>; }
.meta_date a:hover { color: <?php echo $main_color; ?>; }
.quote { background: <?php echo $main_color; ?>; }
.filter ul li a:hover, .filter ul li a.active {	color: <?php echo $main_color; ?>; }
#single-pagination ul li a:hover { background-color: <?php echo $main_color; ?>; }
#load-more a { background: <?php echo $main_color; ?>; }
.widget ul li a { color: <?php echo $main_color; ?>; }
.tag-list a:hover { color: <?php echo $main_color; ?>; }
a.standard { color: <?php echo $main_color; ?>; }
.skill .skill_bar .skill_active { background: <?php echo $main_color; ?>; }
.toggle .toggle_title .toggle_icon { background-color: <?php echo $main_color; ?>; }
.imgoverlay a .overlay { background-color: <?php echo $main_color; ?>; }
.loading span { background-color: <?php echo $main_color; ?>; }
.notfound h1 { color: <?php echo $main_color; ?>; }
.flex-direction-nav li { background-color: <?php echo $main_color; ?>; }
.fancybox-close { background-color: <?php echo $main_color; ?>; }
.fancybox-prev span, .fancybox-next span { background-color: <?php echo $main_color; ?>; }
#fancybox-overlay { background: <?php echo $main_color; ?>; }
.fancybox-outer { background: <?php echo $main_color; ?>; }
div.jp-volume-bar-value { background: <?php echo $main_color; ?>; }
div.jp-play-bar { background: <?php echo $main_color; ?>; }


<?php if (get_option($prefix.'_menustyle') == 'dark'){ ?>
/* DARK MENU STYLE */
nav#main-nav ul li > a  { color: #000000; opacity: 0.60; filter: alpha(opacity=60); -ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=60)"; }
nav#main-nav ul li:hover a { opacity: 0.90; filter: alpha(opacity=90); -ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=90)"; }
nav#main-nav ul li.current-menu-item a, nav#main-nav ul li.current-menu-ancestor a, nav#main-nav ul li.current_page_parent a { opacity: 0.90; filter: alpha(opacity=90); -ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=90)"; }
.filter a.openfilter { color: #000000; opacity: 0.60; filter: alpha(opacity=60); -ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=60)"; }
.filter-seperator { background: #000000; opacity: 0.2; filter: alpha(opacity=20); -ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=20)"; }
<?php } ?>


<?php if (get_option($prefix.'_maincolorstyle') == 'dark'){ ?>
/* DARK MAIN STYLE */
a.more-link:hover { color: #333333; }
#single-pagination ul li a:hover { background-image: url(files/images/next-prev-dark.png); }
#load-more a { color: #333333; }
#load-more a:hover { color: #ffffff; }
#totop { background-image: url(files/images/next-prev-dark.png); }
#totop:hover {  background-image: url(files/images/next-prev.png); }
.imgoverlay a .overlaymeta { color: #333333; }
.imgoverlay a .overlaymeta h6 { color: #333333; }
.imgoverlay a .overlaymeta .meta-seperator { background: #000000; opacity: 0.2; filter: alpha(opacity=20); -ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=20)"; }
.flex-direction-nav li a { background-image: url(files/images/next-prev-dark.png) !important; }
.fancybox-close, .fancybox-prev span, .fancybox-next span { background-image: url(files/images/next-prev-dark.png); }
<?php } ?>


<?php if (get_option($prefix.'_contentwidth') !== '' && get_option($prefix.'_contentwidth') !== '50'){ 
	if ( get_option($prefix.'_contentwidth') == '65' ) { $contentwidth = 65; $asidewidth = 35; }
	if ( get_option($prefix.'_contentwidth') == '75' ) { $contentwidth = 75; $asidewidth = 25; }
?>
/* CONTENT WIDTH */
.single .mainside, .single .mainside-bg, .page-template-default .mainside, .page-template-default .mainside-bg { width: <?php echo $asidewidth; ?>%; }
.single .maincontent, .halfcontent { width: <?php echo $contentwidth; ?>%; }
.fullcontent { width: 100%; }
#map { width: <?php echo $asidewidth; ?>%; }
#fullscreen-bg {  width: <?php echo $asidewidth; ?>%; }
<?php } ?>


/* SPACINGS */
.header-inner { padding-top: <?php echo $spacing['header']; ?>px; padding-bottom: <?php echo $spacing['header']; ?>px; }
#footer-inner { padding-top: <?php echo $spacing['footer']; ?>px; padding-bottom: <?php echo $spacing['footer']; ?>px; }
nav#main-nav { margin-top: <?php echo $spacing['menu']; ?>px; } 
.filter { margin-top: <?php echo $spacing['menu']; ?>px; } 
.filter-seperator { margin-top: <?php echo $spacing['menu']; ?>px; } 

<?php if (get_option($prefix.'_preloader')) { ?>
/* PRELOADER */
#loading { background: transparent url(files/images/loader-overlay.png) center center; display: none; }
<?php } ?>


<?php if (get_option($prefix.'_sidebar') == 'sidebar_right') { ?>
/* SIDEBAR POSITION */
.maincontent { float:right; }
.mainside { float:left; }
.mainside-bg { left: 0px; right: inherit; }
#map { left: 0px; right: inherit; }
#fullscreen-bg { left: 0px; right: inherit; }
<?php } ?>


<?php if (get_option($prefix.'_footerfixed')) { ?>
/* FOOTER POSITION */
footer { position: inherit; bottom: inherit; left: inherit; }
<?php } ?>


/* CUSTOM CSS (Theme Options) */
<?php echo $customcss; ?>