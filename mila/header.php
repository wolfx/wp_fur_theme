<?php 

global $prefix;

?>
<!doctype html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->
<head>
<meta charset="utf-8">

<!-- scaling not possible (for smartphones, ipad, etc.) -->
<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />

<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />


<?php  
$titles = sr_getTitle();
if ($titles['tax']) {
	$title =  $titles['tax'].' | '.$titles['title'];
} else {
	$title =  $titles['title'];
}
?>
<title><?php echo $title; ?> | <?php bloginfo('name'); ?> | <?php bloginfo('description'); ?></title>

<?php sr_get_social_metas(); ?>

<?php get_template_part( 'includes/custom-fonts' ); ?>

<?php wp_head(); ?>

</head>


<body <?php body_class(); ?>>

<div id="page-content">

<div id="loading"><span>loading</span></div>

<header>        
        <div class="header-inner wrapper clearfix">
            <span style="font-size:30px;color:#fff;">兽人星球</span>
	        <a target="_blank" style="float:right;font-size:20px;color:#fff;margin-left:20px;" href="http://srxq.net.103-224-22-17.mdhk-pp-wb1.webhostbox.net/bbs">进入论坛</a>
            <a target="_blank" style="float:right;font-size:20px;color:#fff;margin-left:20px;" href="http://srxq.net.103-224-22-17.mdhk-pp-wb1.webhostbox.net/zazhi/online.html">兽星杂志</a>
            <a id="friendLinks" style="float:right;font-size:20px;color:#fff;" href="javascript:void(0)">友情链接</a>
        </div>
        <div style="clear:both"></div>
        
        
        <div id="friendLinks-template" style="display:none;">
            <div class="links-type">图站</div>
            <a class="links-item" target="_blank" href="http://www.furaffinity.net/" alt="欧美Furry绘师站">FA</a>
            <a class="links-item" target="_blank" href="http://www.pixiv.net/" alt="日本画师站点">PIXIV</a>
            <a class="links-item" target="_blank" href="https://inkbunny.net/" alt="英文兽站">inkbunny</a>
            <a class="links-item" target="_blank" href="http://www.sofurry.com/" alt="英文兽站">SOFURRY</a>
            <div class="links-type">论坛</div>
            <a class="links-item" target="_blank" href="http://chunghwa-furry.deviantart.com/" alt="中华兽群">中华兽群</a>
            <a class="links-item" target="_blank" href="http://yinglong.org/forum/" alt="中文龙类主题站">鳞目界域</a>
            <a class="links-item" target="_blank" href="http://www.boku-star.net/bbs/portal.php" alt="正太少年控聚集地">少年唐</a>
            <a class="links-item" target="_blank" href="http://www.hu67.com/" alt="幻想生物 中文兽站">虎67</a>
            <a class="links-item" target="_blank" href="http://www.cnfurry.com/forum.php" alt="中文兽站">兽向中文网</a>
            <a class="links-item" target="_blank" href="http://wolfbbs.net/forum.php" alt="中文兽站之一">狼之乐园</a>
            <a class="links-item" target="_blank" href="http://www.dnaxcat.net/" alt="萌系兽站">九藏喵窝</a>
            <a class="links-item" target="_blank" href="http://www.furagon.com/" alt="中文龙类主题站">龙兽帝国</a>
            <a class="links-item" target="_blank" href="http://www.fursarcar.com/" alt="中文兽站">兽人世家</a>
            <a class="links-item" target="_blank" href="http://www.wilddream.net/" alt="中文兽站">WildDream</a>
            <div class="links-type">其它</div>
            <a class="links-item" target="_blank" href="https://www.tumblr.com/">汤不热</a>
            <a class="links-item" target="_blank" href="http://www.twitter.com " >推特</a>
            <a class="links-item" target="_blank" href="http://www.plurk.com/top/">噗浪</a>
            <a class="links-item" target="_blank" href="http://weibo.com/">新浪</a>
            <a class="links-item" target="_blank" href="http://blog.yam.com/msg/sto751025#yamBox_REG_END">BL兽兽区</a>
            
            
        </div>
</header> <!-- END header -->

<section id="main">
	
	<div id="main-inner" class="clearfix">