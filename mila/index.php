<?php
//get global prefix
global $prefix;

//get template header
get_header();

?>

		<div id="content" class="maincontent">
            <div class="content-inner">
                <!-- LOADING AREA -->
            </div>
        </div>
        
        
		<div id="blog" class="mainside left_float">
            <div id="blog-grid" class="masonry clearfix">
                <?php
                get_template_part( 'includes/loop', 'blog');
                ?>
                             
            </div>
            
        	<?php $max_num_page = $query->max_num_pages; loadmore('post', $max_num_page); ?>
            
            <!-- CACHING ALL ITEMS TO ENABLE THE LOAD FUNCTION -->
            <div id="caching">
				<?php
				$caching = get_posts( array( 'posts_per_page'=> '-1', 'post_type' => array('post') ));
                foreach( $caching as $post ) { setup_postdata($post);
                ?>
                <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" class="loadcontent" data-id="<?php echo get_the_ID(); ?>" data-slug="<?php echo $post->post_name; ?>" data-type="post">
				<?php the_title(); ?></a>
                <?php }	?>
            </div>
           	<!-- END CACHING -->
            
        </div>
        
        <div class="mainside-bg"></div>

<?php get_footer(); ?>