<?php

if (!empty($_SERVER['SCRIPT_FILENAME']) && 'comments.php' == basename($_SERVER['SCRIPT_FILENAME']))
	{ die (__('Please do not load this page directly!', 'sr_mila_theme')); }

if ( post_password_required() ) { ?>
	<p class="password_required"><?php _e('The comments is password protected. Enter your password.', 'sr_mila_theme'); ?></p>
<?php
	return;
}
?>

<?php 
global $prefix;
?>

			<?php if ( comments_open() )  { ?>

                <?php if ( have_comments() ) { 
						$comments_amount = get_comment_count($post->ID);
						if ($comments_amount['approved'] > 0) {
				?>
                <div class="seperator"><span class="seperator-big"></span></div>
                <div id="comments" class="clearfix">
                	
                    <h3 class="sectiontitle"><?php echo comments_number('0', '1', '%'); ?> <?php _e('Comments', 'sr_mila_theme'); ?></h3>
                
                	<?php if ( get_option($prefix.'_blogicons') !== 'on') {?>
            		<div id="commenticon" class="left_float"><span class="icon"></span></div>
                    <?php } ?>
                    
                    <ul class="comment-list right_float <?php if ( get_option($prefix.'_blogicons') == 'on') {?>full-comment-list<?php } ?>">
                    <?php wp_list_comments('type=comment&callback=sr_comment'); ?>
                    </ul>
                    <?php paginate_comments_links(); ?> 
                </div> <!-- END #comments -->    
                	<?php } ?>
                <?php } ?>
                
                
                <div class="seperator"><span class="seperator-big"></span></div>
                <div id="leavecomment" class="clearfix">
                
                    <h3 class="sectiontitle"><?php _e('Leave Comment', 'sr_mila_theme'); ?></h3>
                
					<?php 
                    global $comments_defaults;
                    comment_form($comments_defaults);    
                    ?> 
                       
                </div> <!-- END #leave_comment -->
                
<?php } ?> 