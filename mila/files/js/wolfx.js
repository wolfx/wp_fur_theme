jQuery(function(){
    var content = jQuery("#friendLinks-template").html();

    jQuery("#friendLinks").click(function(e) {
        //页面层-自定义
        layer.open({
            type: 1,
            title: false,
            closeBtn: 0,
            shadeClose: true,
            skin: 'wolfx-layer',
            content: content,
            area: '400px'
        });
    });
    
});