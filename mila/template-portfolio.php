<?php
/*
Template Name: Portfolio
*/

//get global prefix
global $prefix;

//get template header
get_header();

?>
		
        <div id="content" class="maincontent">
         	<div class="content-inner">
         		<!-- LOADING AREA -->
        	</div>
        </div>
        
        
        <div id="portfolio" class="mainside">
            <div id="portfolio-grid" class="masonry clearfix">
                <?php 
					
					$sr_portfoliocount = get_option($prefix.'_portfoliocount');
					
					$query = new WP_Query(array(
						'posts_per_page'=> $sr_portfoliocount,
						'paged' => ( get_query_var('paged') ? get_query_var('paged') : 1 ),
						'portfolio_category' => get_query_var('portfolio_category'),
						'm' => get_query_var('m'),		   
						'w' => get_query_var('w'),
						'post_type' => array('portfolio')
					) );
					
					get_template_part( 'includes/loop', 'portfolio'); 
					
				?> 
            </div>
            
            <?php $max_num_page = $query->max_num_pages; loadmore('portfolio', $max_num_page); ?>
            
            
            <!-- CACHING ALL ITEMS TO ENABLE THE LOAD FUNCTION -->
            <div id="caching">
				<?php
				$caching = get_posts( array( 'posts_per_page'=> '-1', 'post_type' => array('portfolio') ));
                foreach( $caching as $post ) { setup_postdata($post);
                ?>
                <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" class="loadcontent" data-id="<?php echo get_the_ID(); ?>" data-slug="<?php echo $post->post_name; ?>" data-type="portfolio"><?php the_title(); ?></a>
                <?php }	?>
            </div>
           	<!-- END CACHING -->
            
        </div>
		
        <div class="mainside-bg"></div>
        
<?php get_footer(); ?>