*** Mila Changelog ***

November 29 2012 - Version 1.2
	* OPTION: dark text for colored background
	* OPTION: Portfolio thumbnails width (small, medium, big)
	* OPTION: Filter Menu for Blog
	* FIX: filter issue ipad/iphone
	* FIX: target blank for portfolio link
	* FIX: load more button translatable

November 23 2012 - Version 1.1
	* OPTION: Gallery thumbnails width (small, medium, big)
	* OPTION: 2/3 Content - 3/4 Content
	* OPTION: List style for galleries
	* OPTION: different color for header
	* OPTION: blog style like portfolio
	* FIX: ipad issue
	* FIX: fix custom filter margin

November 15 2012 - Version 1.0
	* First Release

