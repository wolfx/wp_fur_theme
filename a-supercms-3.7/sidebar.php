<div id="sidebar" style="display:none;">
<?php dynamic_sidebar('Sidebar'); ?>

<?php if(get_option('T_hot') == "On" || get_option('T_hot') == "") {?>
<h3 class="sub">每月热评</h3>
<div class="s-list">
<ul><?php diy_most_comments('5', '30'); ?></ul>
</div>
<?php }?>

<?php if(get_option('T_recentcomments') == "On" || get_option('T_recentcomments') == "") {?>
<h3 class="sub">最近评论</h3>
<ul class="spy">
<?php
global $wpdb;
$sql = "SELECT DISTINCT ID, post_title, post_password, comment_ID, comment_post_ID, comment_author, comment_date_gmt, comment_approved, comment_type,comment_author_url,comment_author_email, SUBSTRING(comment_content,1,50) AS com_excerpt FROM $wpdb->comments LEFT OUTER JOIN $wpdb->posts ON ($wpdb->comments.comment_post_ID = $wpdb->posts.ID) WHERE comment_approved = '1' AND comment_type = '' AND post_password = '' ORDER BY comment_date_gmt DESC LIMIT 5";
$comments = $wpdb->get_results($sql);
$output = $pre_HTML;
foreach ($comments as $comment) {
$output .= "<li><div class='img'>".get_avatar(get_comment_author_email(),50)."</div><div class=\"author\"><a href=\"". get_comment_link( $comment->comment_ID ) ."\" title=\"". $comment->post_title ."\"><strong>".strip_tags($comment->comment_author). "</strong> : " .stripslashes(convert_smilies($comment->com_excerpt)). "...</a></div></li>";
}
$output .= $post_HTML;
echo $output;?>
</ul>
<?php }?>

<?php if(get_option('T_recommend') == "On" || get_option('T_recommend') == "") {?>
<h3 class="sub">推荐阅读</h3>
<ul class="spy">
<?php query_posts( array('posts_per_page' => 5,'orderby' => 'rand','caller_get_posts' => 1) ); while ( have_posts() ) : the_post();?>
<li><?php if ( has_post_thumbnail() ) { ?><div class="img"><?php the_post_thumbnail('thumbnail', array( 'alt' => trim(strip_tags( $post->post_title )), 'title'	=> trim(strip_tags( $post->post_title )), )); ?></div><?php } ?>
<h2><a href="<?php the_permalink() ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h2>
<div class="fcats"><?php the_category(', '); ?></div> 
<div class="fcats">POSTED ON <?php the_time('Y.m.d'); ?></div> 
</li>
<?php endwhile; wp_reset_query();?>
</ul>
<?php }?>

<?php dynamic_sidebar('Sidebar2'); ?>

</div>