<?php
/*
Template Name: 标签页
*/
?>
<?php get_header(); ?>
<div id="primary">
<h1 class="sub"><?php the_title(); ?></h1>
<div class="content">
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
<div <?php if(function_exists('post_class')) : ?><?php post_class(); ?><?php else : ?>class="post post-<?php the_ID(); ?>"<?php endif; ?>>
<?php the_content('<br />[ More .......................................................... ]'); ?>
<p>共计<?php echo $count_tags = wp_count_terms('post_tag'); ?>个标签<p/>
<ul class="tagclouds">
<?php $tags_list = get_tags('orderby=count&order=DESC');
if ($tags_list) { 
foreach($tags_list as $tag) {
echo '<li><a class="tags tags-'. $tag->term_id .'" href="'.get_tag_link($tag).'">'. $tag->name .'</a>('. $tag->count .')<p class="tag-posts">'; 
$posts = get_posts( "tag_id=". $tag->term_id ."&numberposts=1" );
if( $posts ){
foreach( $posts as $post ) {
setup_postdata( $post );
echo '<a href="'.get_permalink().'">'.get_the_title().'</a></p><em>'.get_the_time('Y-m-d').'</em>';
}
}
echo '</li>';
} 
} 
?>
</ul>
</div>
<?php endwhile; ?>
<?php endif; ?>
</div>
</div>

<?php get_sidebar(); ?>
<?php get_footer(); ?>