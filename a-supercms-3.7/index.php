<?php get_header(); ?>
<div id="primary">

<?php if(is_home() && get_option('T_slider') == "On") { get_template_part('slider'); } ?>

<!--版头 by wolf-->
<!--<div id="friendLinks">
    <div class="links-type">图站</div>
    <div class="link-ctn">
        <a class="links-item" href="http://www.furaffinity.net/" target="_blank">FA</a>
        <a class="links-item" href="http://www.pixiv.net/" target="_blank">PIXIV</a>
        <a class="links-item" href="https://inkbunny.net/" target="_blank">inkbunny</a>
        <a class="links-item" href="http://www.sofurry.com/" target="_blank">SOFURRY</a>
    </div>
    <div class="links-type">论坛</div>
    <div class="link-ctn">
        <a class="links-item" href="http://chunghwa-furry.deviantart.com/" target="_blank">中华兽群</a>
        <a class="links-item" href="http://yinglong.org/forum/" target="_blank">鳞目界域</a>
        <a class="links-item" href="http://www.boku-star.net/bbs/portal.php" target="_blank">少年唐</a>
        <a class="links-item" href="http://www.hu67.com/" target="_blank">虎67</a>
        <a class="links-item" href="http://www.cnfurry.com/forum.php" target="_blank">兽向中文网</a>
        <a class="links-item" href="http://wolfbbs.net/forum.php" target="_blank">狼之乐园</a>
        <a class="links-item" href="http://www.dnaxcat.net/" target="_blank">九藏喵窝</a>
        <a class="links-item" href="http://www.furagon.com/" target="_blank">龙兽帝国</a>
        <a class="links-item" href="http://www.fursarcar.com/" target="_blank">兽人世家</a>
        <a class="links-item" href="http://www.wilddream.net/" target="_blank">WildDream</a>
    </div>
    <div class="links-type">其它</div>
    <div class="link-ctn">
        <a class="links-item" href="https://www.tumblr.com/" target="_blank">汤不热</a>
        <a class="links-item" href="http://www.twitter.com " target="_blank">推特</a>
        <a class="links-item" href="http://www.plurk.com/top/" target="_blank">噗浪</a>
        <a class="links-item" href="http://weibo.com/" target="_blank">新浪</a>
        <a class="links-item" href="http://blog.yam.com/msg/sto751025#yamBox_REG_END" target="_blank">BL兽兽区</a>
    </div>
</div>-->







<h3 class="sub">
<?php if (is_home()) { ?>
最新分享
<?php } elseif (is_category()) { ?>
分类 “<?php single_cat_title(); ?>” 下的文章
<?php } elseif( is_tag() ) { ?><?php $tag_obj = $wp_query->get_queried_object() ?>
标签为 “<?php single_tag_title(); ?>” 的文章
<?php } elseif (is_search()) { ?>
关键词 “<?php echo esc_html(get_search_query()); ?>” 的搜索结果
<?php } elseif (is_day()) { ?>
发表于 “<?php the_time('F jS, Y'); ?>” 的文章
<?php } elseif (is_month()) { ?>
发表于 “<?php the_time('F, Y'); ?>” 的文章
<?php } elseif (is_year()) { ?>
发表于 “<?php the_time('Y'); ?>” 的文章
<?php } elseif (is_author()) { ?>
该作者发表的文章
<?php } elseif (isset($_GET['paged']) && !empty($_GET['paged'])) { ?>
文章列表
<?php } ?>
</h3>
<ul class="list">
<?php if (have_posts()) : while (have_posts()) : the_post();
global $post, $posts; 
$first_img = ""; 
ob_start(); 
ob_end_clean(); 
$output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $post->post_content, $matches); 
$first_img = $matches [1] [0];
?>
<li>
<?php if ( has_post_thumbnail() ) { ?><div class="thumbnail"><div class="cat"><?php the_category(' &middot; '); ?></div><div class="img"><a href="<?php the_permalink(); ?>" title="<?php the_title() ?>"><?php the_post_thumbnail('thumbnail-list', array( 'alt' => trim(strip_tags( $post->post_title )), 'title'	=> trim(strip_tags( $post->post_title )), )); ?></a></div></div><?php } elseif(!empty($first_img)) {?>
<div class="thumbnail"><div class="cat"><?php the_category(' &middot; '); ?></div><div class="img"><a href="<?php the_permalink(); ?>" title="<?php the_title() ?>"><img src="<?php echo $first_img ?>" /></a></div></div><?php }?>
<div class="content <?php if ( !has_post_thumbnail() && empty($first_img) ) { ?>none<?php }?>"><?php if ( !has_post_thumbnail() && empty($first_img) ) { ?><div class="nothumbnail"><?php the_category(' &middot; '); ?></div><?php }?><h2><?php if( is_sticky() ) echo '<strong style="font-weight:normal;color:green;">[置顶]</strong>'; ?> <a href="<?php the_permalink(); ?>" title="<?php the_title() ?>"><?php echo mb_strimwidth(get_the_title(),0,60,' …'); ?></a></h2>
<div class="info"><?php childtheme_post_header(); ?> <?php the_time('Y.m.d'); ?> &middot; <?php comments_popup_link('发表评论', '1 条评论', '% 条评论'); ?></div>
<p><?php if(has_excerpt()){ the_excerpt();} else { echo mb_strimwidth(strip_tags($post->post_content),0,200,'...');}?></p><p class="link"><a href="<?php the_permalink(); ?>">阅读全文...</a></p>
</div>
</li>
<?php endwhile; ?>
</ul>
<div class="clear"></div>
<div><?php if (function_exists('pagenavi')): ?><div class="pagenavi"><?php pagenavi(); ?></div><?php else : ?><div class="pagenavi"><div class="left"><?php previous_posts_link('上一页'); ?></div><div class="right"><?php next_posts_link('下一页'); ?></div></div><?php endif; ?></div>
<?php else : ?>
<h3 class="sub">温馨提示</h3>
<p>抱歉，没有符合条件的内容。</p>
<?php endif; ?>
</div>

<?php get_sidebar(); ?>
<?php get_footer(); ?>