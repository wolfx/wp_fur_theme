<?php
$themename = "Theme";
$shortname = "T";
$options = array (
			    		
				array(	"name" => "功能设置",
						"type" => "title"),
            	
            	array(	"type" => "open"),
				
				array(	"name" => "文字LOGO开关",
						"desc" => "控制是否启用文字LOGO. 关闭为使用图片LOGO.",
			    		"id" => $shortname."_logo",
			    		"std" => "",
			    		"type" => "select",
			    		"options" => array("Off", "On")),
						
        		array(	"name" => "文章底部相关文章开关",
						"desc" => "开启文章底部将显示6条相关文章. 默认开启.",
			    		"id" => $shortname."_related",
			    		"std" => "",
			    		"type" => "select",
			    		"options" => array("Off", "On")),
						
				array(	"name" => "评论表情图片开关",
						"desc" => "控制评论框上面是否显示表情图片功能，更换表情图片只需替换主题文件夹内的smilies文件夹中的图片. 默认关闭.",
			    		"id" => $shortname."_smiley",
			    		"std" => "",
			    		"type" => "select",
			    		"options" => array("Off", "On")),
				
				array(	"name" => "百度分享按钮开关",
						"desc" => "百度分享按钮功能. 默认关闭.",
			    		"id" => $shortname."_share",
			    		"std" => "",
			    		"type" => "select",
			    		"options" => array("Off", "On")),
				
				array(	"name" => "右侧栏每月热评开关",
						"desc" => "在右侧栏显示每月评论最多的10篇文章. 默认开启.",
			    		"id" => $shortname."_hot",
			    		"std" => "",
			    		"type" => "select",
			    		"options" => array("Off", "On")),
				
				array(	"name" => "右侧栏最近评论开关",
						"desc" => "在右侧栏显示5条最近评论. 默认开启.",
			    		"id" => $shortname."_recentcomments",
			    		"std" => "",
			    		"type" => "select",
			    		"options" => array("Off", "On")),
				
				array(	"name" => "右侧栏推荐阅读开关",
						"desc" => "在右侧栏显示5篇随机推荐文章. 默认开启.",
			    		"id" => $shortname."_recommend",
			    		"std" => "",
			    		"type" => "select",
			    		"options" => array("Off", "On")),
				
				array(	"name" => "自动网页描述开关（SEO）",
						"desc" => "自动为页面增加网页描述. 默认关闭.",
			    		"id" => $shortname."_auto_description",
			    		"std" => "",
			    		"type" => "select",
			    		"options" => array("Off", "On")),
				
				array(	"name" => "自动网页关键词开关（SEO）",
						"desc" => "自动为页面增加关键词. 默认关闭.",
			    		"id" => $shortname."_auto_keywords",
			    		"std" => "",
			    		"type" => "select",
			    		"options" => array("Off", "On")),
						
				array(	"name" => "自定义首页网页描述（SEO）",
					    "id" => $shortname."_description",
              			"desc" => "SEO 可选项",
					    "std" => "杰夫工作室 [Jeff Studio] 原创 A-SuperCms 主题",
					    "type" => "text"),
						
				array(	"name" => "自定义首页关键词（SEO）",
					    "id" => $shortname."_keywords",
              			"desc" => "SEO 可选项",
					    "std" => "WordPress,主题,A-SuperCms",
					    "type" => "text"),
				
				array(	"name" => "自定义统计代码",
					    "id" => $shortname."_tongji",
              			"desc" => "输入你的统计代码，可不填。",
					    "std" => "",
					    "type" => "textarea"),
						
			    array(	"type" => "close"),
				
				array(	"name" => "图标设置",
						"type" => "title"),
            	
            	array(	"type" => "open"),
						
				array(	"name" => "网站订阅地址",
					    "id" => $shortname."_feed",
						"desc" => "请填入贵站订阅地址。默认显示。",
					    "std" => "",
					    "type" => "text"),
						
				array(	"name" => "邮件订阅地址或联系邮箱",
					    "id" => $shortname."_mail",
						"desc" => "留空则不显示。若填写联系邮箱，请使用 mailto:abc@qq.com 格式",
					    "std" => "",
					    "type" => "text"),
						
				array(	"name" => "新浪微博地址",
					    "id" => $shortname."_sina",
						"desc" => "留空则不显示",
					    "std" => "",
					    "type" => "text"),
						
				array(	"name" => "腾讯微博地址",
					    "id" => $shortname."_qq",
						"desc" => "留空则不显示",
					    "std" => "",
					    "type" => "text"),
						
				array(	"name" => "QQ空间地址",
					    "id" => $shortname."_qqzone",
						"desc" => "留空则不显示",
					    "std" => "",
					    "type" => "text"),
						
				array(	"name" => "淘宝网店地址",
					    "id" => $shortname."_taobao",
						"desc" => "留空则不显示",
					    "std" => "",
					    "type" => "text"),
						
				array(	"name" => "人人网地址",
					    "id" => $shortname."_renren",
						"desc" => "留空则不显示",
					    "std" => "",
					    "type" => "text"),
						
				array(	"name" => "豆瓣网地址",
					    "id" => $shortname."_douban",
						"desc" => "留空则不显示",
					    "std" => "",
					    "type" => "text"),
						
				array(	"name" => "Twitter 地址",
					    "id" => $shortname."_twitter",
						"desc" => "留空则不显示",
					    "std" => "",
					    "type" => "text"),
						
			    array(	"type" => "close"),
				
				array(	"name" => "幻灯片设置",
						"type" => "title"),
            	
            	array(	"type" => "open"),
            	
        		array(	"name" => "幻灯开关",
						"desc" => "控制首页幻灯片是否显示. 默认关闭.",
			    		"id" => $shortname."_slider",
			    		"std" => "",
			    		"type" => "select",
			    		"options" => array("Off", "On")),
						
			    array(	"type" => "close")
		  );

function mytheme_add_admin() {

    global $themename, $shortname, $options;

    if ( $_GET['page'] == basename(__FILE__) ) {
    
        if ( 'save' == $_REQUEST['action'] ) {

                foreach ($options as $value) {
                    update_option( $value['id'], $_REQUEST[ $value['id'] ] ); }

                foreach ($options as $value) {
                    if( isset( $_REQUEST[ $value['id'] ] ) ) { update_option( $value['id'], $_REQUEST[ $value['id'] ]  ); } else { delete_option( $value['id'] ); } }

                header("Location: themes.php?page=theme-options.php&saved=true");
                die;

        } else if( 'reset' == $_REQUEST['action'] ) {

            foreach ($options as $value) {
                delete_option( $value['id'] ); }

            header("Location: themes.php?page=theme-options.php&reset=true");
            die;

        }
    }

    add_theme_page("主题设置", "主题设置", 'edit_themes', basename(__FILE__), 'mytheme_admin');

}

//add_theme_page($themename . 'Header Options', 'Header Options', 'edit_themes', basename(__FILE__), 'headimage_admin');

function headimage_admin(){
	
}

function mytheme_admin() {

    global $themename, $shortname, $options;

    if ( $_REQUEST['saved'] ) echo '<div id="message" class="updated fade"><p><strong>设置已保存.</strong></p></div>';
    if ( $_REQUEST['reset'] ) echo '<div id="message" class="updated fade"><p><strong>设置已重置.</strong></p></div>';
    
?>
<div class="wrap">
<h2>[免费版] A-SuperCms Theme Settings</h2>
<p>您当前使用的是免费版。赞助我们，可获得高级版。【<strong><a href="http://demo.jeffstudio.net/?themedemo=a-supercms-pro" target="_blank">高级版预览</a></strong>】</p>
<p><strong>赞助说明：只需赞助费168.00元，即可获取主题高级版</strong>。通过支付宝赞助，赞助后请邮件联系我们，我们收到赞助费后会第一时间发送主题到您的邮箱。感谢您支持我们的持续发展，谢谢！</p>
<p><a href='http://me.alipay.com/imjeff' target="_blank" title="赞助我们"><img src='https://img.alipay.com/sys/personalprod/style/mc/btn-index.png' /></a></p>
<form method="post">

<div id="poststuff" class="dlm">

<?php foreach ($options as $value) { 
    
	switch ( $value['type'] ) {
	
		case "open":
		?>
		
        
		<?php break;
		
		case "close":
		?>
        </table></div></div>
        
        
		<?php break;
		
		case "title":
		?>
		<div class="postbox close">
		<h3><?php echo $value['name']; ?></h3>
		<div class="inside">
        
		<table width="100%" border="0" style="padding:5px 10px;"><tr>
        </tr>
                
        
		<?php break;

		case 'text':
		?>
        
        <tr>
            <td width="150px" rowspan="2" valign="middle"><strong><?php echo $value['name']; ?></strong></td>
            <td><input style="width:400px;" name="<?php echo $value['id']; ?>" id="<?php echo $value['id']; ?>" type="<?php echo $value['type']; ?>" value="<?php if ( get_option( $value['id'] ) != "") { echo stripslashes( get_option( $value['id'] ) ); } else { echo $value['std']; } ?>" /></td>
        </tr>

        <tr>
            <td><?php echo $value['desc']; ?></td>
        </tr><tr><td colspan="2" style="margin-bottom:5px;border-bottom:1px dotted #000000;">&nbsp;</td></tr><tr><td colspan="2">&nbsp;</td></tr>

		<?php 
		break;
		
		case 'textarea':
		?>
        
        <tr>
            <td width="150px" rowspan="2" valign="middle"><strong><?php echo $value['name']; ?></strong></td>
            <td><textarea name="<?php echo $value['id']; ?>" style="width:600px; height:200px;" type="<?php echo $value['type']; ?>" cols="" rows=""><?php if ( get_option( $value['id'] ) != "") { echo stripslashes( get_option(  $value['id'] ) ); } else { echo $value['std']; } ?></textarea></td>
            
        </tr>

        <tr>
            <td><?php echo $value['desc']; ?></td>
        </tr><tr><td colspan="2" style="margin-bottom:5px;border-bottom:1px dotted #000000;">&nbsp;</td></tr><tr><td colspan="2">&nbsp;</td></tr>

		<?php 
		break;
		
		case 'select':
		?>
        <tr>
            <td width="150px" rowspan="2" valign="middle"><strong><?php echo $value['name']; ?></strong></td>
            <td><select style="width:240px;" name="<?php echo $value['id']; ?>" id="<?php echo $value['id']; ?>"><?php foreach ($value['options'] as $option) { ?><option<?php if ( get_option( $value['id'] ) == $option) { echo ' selected="selected"'; } elseif ($option == $value['std']) { echo ' selected="selected"'; } ?>><?php echo $option; ?></option><?php } ?></select></td>
       </tr>
                
       <tr>
            <td><?php echo $value['desc']; ?></td>
       </tr><tr><td colspan="2" style="margin-bottom:5px;border-bottom:1px dotted #000000;">&nbsp;</td></tr><tr><td colspan="2">&nbsp;</td></tr>

		<?php
        break;
            
		case "checkbox":
		?>
            <tr>
            <td width="150px" rowspan="2" valign="middle"><strong><?php echo $value['name']; ?></strong></td>
                <td><?php if(get_option($value['id'])){ $checked = "checked=\"checked\""; }else{ $checked = ""; } ?>
                        <input type="checkbox" name="<?php echo $value['id']; ?>" id="<?php echo $value['id']; ?>" value="true" <?php echo $checked; ?> />
                        </td>
            </tr>
                        
            <tr>
                <td><?php echo $value['desc']; ?></td>
           </tr><tr><td colspan="2" style="margin-bottom:5px;border-bottom:1px dotted #000000;">&nbsp;</td></tr><tr><td colspan="2">&nbsp;</td></tr>
            
        <?php 		break;
	
 
} 
}
?>

</div>

<p class="submit">
<input name="save" type="submit" value="保存设置" />    
<input type="hidden" name="action" value="save" />
</p>
</form>
<form method="post">
<p class="submit">
<input name="reset" type="submit" value="重置" />
<input type="hidden" name="action" value="reset" />
</p>
</form>

			<script type="text/javascript">
			<!--
			jQuery('.postbox h3').prepend('<a class="togbox">+</a> ');
			jQuery('.postbox h3').click( function() { jQuery(jQuery(this).parent().get(0)).toggleClass('closed'); } );
			jQuery('.postbox.close').each(function(){ jQuery(this).addClass("closed"); });
			//-->
			</script>

<?php }  add_action('admin_menu', 'mytheme_add_admin'); ?>