<?php get_header(); ?>
<div id="primary">
<h1 class="sub"><?php the_title(); ?></h1>
<div class="content">
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
<div <?php if(function_exists('post_class')) : ?><?php post_class(); ?><?php else : ?>class="post post-<?php the_ID(); ?>"<?php endif; ?>>
<?php the_content('<br />[ More .......................................................... ]'); ?>
<?php wp_link_pages(array('before' => '<p><strong>Pages:</strong> ', 'after' => '</p>', 'next_or_number' => 'number')); ?>
</div>
<?php endwhile; ?>
<?php comments_template('', true); ?>

<?php endif; ?>
         
</div>
</div>

<?php get_sidebar(); ?>
<?php get_footer(); ?>