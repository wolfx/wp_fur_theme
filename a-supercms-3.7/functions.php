<?php
if ( function_exists( 'add_theme_support' ) ) {
	add_theme_support( 'post-thumbnails' );
	add_image_size( 'thumbnail-list', 200, '', true );
	add_image_size( 'thumbnail-wide', 650, 260, true );
}

if ( ! isset( $content_width ) ) $content_width = 650;

// Get Theme Options Page
if(is_admin()) :
require_once ('theme-options.php');
endif;

add_custom_background();
add_theme_support('automatic-feed-links');
remove_action('wp_head', 'wp_generator');
remove_filter('comment_text', 'make_clickable', 9);
remove_filter('the_content', 'wptexturize');
remove_filter('wp_head', 'CID_css');
add_action('wp_print_scripts',	'wp_disable_autosave' );
remove_action('pre_post_update','wp_save_post_revision' );
function wp_disable_autosave() { wp_deregister_script('autosave');}

add_theme_support('menus');
register_nav_menus(array(
	'nav' => 'Menu'
	)
);

if ( !is_nav_menu('Header')) {
    $menu_id = wp_create_nav_menu('Menu');
    wp_update_nav_menu_item($menu_id, 1);
}

add_filter('smilies_src','custom_smilies_src',1,10);
function custom_smilies_src ($img_src, $img, $siteurl){
return get_template_directory_uri() .'/images/smilies/'.$img;
};
function a(){echo base64_decode('PG1ldGEgbmFtZT0iY29weXJpZ2h0IiBjb250ZW50PSJDb3B5cmlnaHQgMjAwNC0yMDEyIEplZmYgU3R1ZGlvLCBBbGwgUmlnaHRzIFJlc2VydmVkLiIgLz4NCg==');}

// Sidebar Widget
if ( function_exists('register_sidebar') ){
register_sidebar(array(
'name' => 'Sidebar',
'before_widget' => '<div class="s-list">',
'after_widget' => '</div>',
'before_title' => '<h3 class="sub">',
'after_title' => '</h3>',
));
register_sidebar(array(
'name' => 'Sidebar2',
'before_widget' => '<div class="s-list">',
'after_widget' => '</div>',
'before_title' => '<h3 class="sub">',
'after_title' => '</h3>',
));
register_sidebar(array(
'name' => 'Footbar',
'before_widget' => '',
'after_widget' => '',
'before_title' => '<h3 class="sub">',
'after_title' => '</h3>',
));
};

/*
pagenavi
*/
function pagenavi( $p = 2 ) {
if ( is_singular() ) return;
global $wp_query, $paged;
$max_page = $wp_query->max_num_pages;
if ( $max_page == 1 ) return;
if ( empty( $paged ) ) $paged = 1;
echo '<span class="page-numbers">第 ' . $paged . ' 页 , 共 ' . $max_page . ' 页</span> ';
if ( $paged > $p + 1 ) p_link( 1, '最前页' );
if ( $paged > $p + 2 ) echo '<span class="page-numbers">...</span>';
for( $i = $paged - $p; $i <= $paged + $p; $i++ ) {
if ( $i > 0 && $i <= $max_page ) $i == $paged ? print "<span class='current'>{$i}</span> " : p_link( $i );
}
if ( $paged < $max_page - $p - 1 ) echo '<span class="page-numbers">...</span> ';
if ( $paged < $max_page - $p ) p_link( $max_page, '最末页' );
}
function p_link( $i, $title = '' ) {
if ( $title == '' ) $title = "第 {$i} 页";
echo "<a href='", esc_html( get_pagenum_link( $i ) ), "' title='{$title}'>{$i}</a> ";
};function hd(){echo base64_decode('PjxkaXYgaWQ9ImNvbnRhaW5lciI=');};

/*
auto keywords & description
*/
function auto_keywords() {
  global $s, $post;
  $keywords = '';
  if ( is_single() ) {
    if ( get_the_tags( $post->ID ) ) {
      foreach ( get_the_tags( $post->ID ) as $tag ) $keywords .= $tag->name . ', ';
    }
    foreach ( get_the_category( $post->ID ) as $category ) $keywords .= $category->cat_name . ', ';
    $keywords = substr_replace( $keywords , '' , -2);
  } elseif ( is_home () )    { $keywords = get_option('T_keywords');
  } elseif ( is_tag() )      { $keywords = single_tag_title('', false);
  } elseif ( is_category() ) { $keywords = single_cat_title('', false);
  } elseif ( is_search() )   { $keywords = esc_html( $s, 1 );
  } else { $keywords = trim( wp_title('', false) );
  }
  if ( $keywords ) {
    echo "<meta name=\"keywords\" content=\"$keywords\" />\n";
  }
}
function auto_description() {
  global $s, $post;
  $description = '';
  $blog_name = get_bloginfo('name');
  if ( is_singular() ) {
    if( !empty( $post->post_excerpt ) ) {
      $text = $post->post_excerpt;
    } else {
      $text = $post->post_content;
    }
    $description = trim( str_replace( array( "\r\n", "\r", "\n", "　", " "), " ", str_replace( "\"", "'", strip_tags( $text ) ) ) );
    if ( !( $description ) ) $description = $blog_name . " - " . trim( wp_title('', false) );
  } elseif ( is_home () )    { $description = $blog_name . " - " . get_option('T_description'); // 首頁要自己加
  } elseif ( is_tag() )      { $description = $blog_name . "有关 '" . single_tag_title('', false) . "' 的文章";
  } elseif ( is_category() ) { $description = $blog_name . "有关 '" . single_cat_title('', false) . "' 的文章";
  } elseif ( is_archive() )  { $description = $blog_name . "在: '" . trim( wp_title('', false) ) . "' 的文章";
  } elseif ( is_search() )   { $description = $blog_name . ": '" . esc_html( $s, 1 ) . "' 的搜索結果";
  } else { $description = $blog_name . "有关 '" . trim( wp_title('', false) ) . "' 的文章";
  }
  $description = mb_substr( $description, 0, 220, 'utf-8' ) . '..';
  echo "<meta name=\"description\" content=\"$description\" />\n";
}

/*
New Icon Images
*/
function childtheme_post_header(){
	if ( (time()-get_the_time('U')) <= (2*86400) ) {
		echo '<span class="new">NEW!</span> ';
	}
}

/* diy_most_comments */
function e(){echo base64_decode('UG93ZXJlZCBieSA8YSBocmVmPSdodHRwOi8vd29yZHByZXNzLm9yZy8nIHRpdGxlPSfph4fnlKggV29yZFByZXNzIOaehOW7uic+V29yZFByZXNzPC9hPiAmIA0KRGVzaWduZWQgYnkgPGEgaHJlZj0naHR0cDovL3d3dy5qZWZmc3R1ZGlvLm5ldC8nIHRpdGxlPSfnlLEg5p2w5aSr5bel5L2c5a6kIOiuvuiuoSc+SmVmZiBTdHVkaW88Lw==');}
function diy_most_comments($num = 10, $daynum = 30) {
	global $wpdb;
	$now = gmdate("Y-m-d H:i:s",time());
	$popularposts = "SELECT ID, post_title, post_date, comment_count, COUNT($wpdb->comments.comment_post_ID) AS 'popular' FROM $wpdb->posts, $wpdb->comments WHERE comment_approved = '1' AND $wpdb->posts.ID=$wpdb->comments.comment_post_ID AND post_status = 'publish' AND post_date > date_sub( NOW(), INTERVAL $daynum DAY ) AND comment_status = 'open' GROUP BY $wpdb->comments.comment_post_ID ORDER BY popular DESC LIMIT $num";
	$posts = $wpdb->get_results($popularposts);
	$popular = '';
	if($posts){
	foreach($posts as $post){
	$post_title = stripslashes($post->post_title);
	$post_date = stripslashes($post->post_date);
	$comments = stripslashes($post->comment_count);
	$guid = get_permalink($post->ID);
	$popular .= '<li><a href="'.$guid.'#comments" title="'.$post_title.'">'.$post_title.' <span>('.$comments.')</span></a></li>';
	}	
}else 
{
	$popular = '<li>coming soon...</li>'."\n";
}
	echo $popular;
};

add_action('widgets_init','unregister_search_widget');
function unregister_search_widget(){
    unregister_widget('WP_Widget_Search');
    unregister_widget('WP_Widget_Recent_Comments');
}

add_action('wp_head', 'diycolor');


function get_ssl_avatar($avatar) {
   $avatar = preg_replace('/.*\/avatar\/(.*)\?s=([\d]+)&.*/','<img src="https://secure.gravatar.com/avatar/$1?s=32" class="avatar avatar-32" height="32" width="32">',$avatar);
   return $avatar;
}
add_filter('get_avatar', 'get_ssl_avatar');


function izt_cdn_callback($buffer) {return str_replace('googleapis.com', 'useso.com', $buffer);}
function izt_buffer_start() {ob_start('izt_cdn_callback');}
function izt_buffer_end() {ob_end_flush();}
add_action('init', 'izt_buffer_start');
add_action('shutdown', 'izt_buffer_end');


function diycolor() {
if(get_option('T_menu_color') != "" or ('T_menu_hovercolor') != "" or ('T_footer_color') != "") {?>
<style type="text/css">
<?php if(get_option('T_menu_color') != "") {?>
#menu,#menu .menunav li,#searchform #searchsubmit,.list li .cat{background-color:<?php echo get_option('T_menu_color');?>;}
#searchform #s:hover,#searchform #s:focus{border-color:<?php echo get_option('T_menu_color');?>;}
<?php } ?>
<?php if(get_option('T_menu_hovercolor') != "") {?>
#menu .menunav li a:hover,#menu .menunav li:hover{background-color:<?php echo get_option('T_menu_hovercolor');?>;}
<?php } ?>
<?php if(get_option('T_footer_color') != "") {?>
#footer{background:<?php echo get_option('T_footer_color');?>;}
<?php } ?>
</style>
<?php }} ?>
