<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes( 'xhtml' ); ?>>
<head profile="http://gmpg.org/xfn/11">
<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
<title><?php wp_title( '-', true, 'right' ); echo esc_html( get_bloginfo('name'), 1 ); ?></title>
<?php if(get_option('T_auto_description') == "On"){ auto_description(); }?>
<?php if(get_option('T_auto_keywords') == "On"){ auto_keywords(); } echo a();?>
<meta name="viewport" content="width=device-width" />
<link rel="shortcut icon" href="<?php bloginfo('template_directory'); ?>/favicon.ico" type="image/x-icon">
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
<?php wp_head(); ?>
</head>
<body <?php body_class();echo hd();?>>
<div id="head">
<div id="logo">
<?php if(get_option('T_logo') == "On") {?>
<?php if ( is_single() || is_page() ){ ?><h2><a href="<?php bloginfo('url'); ?>" title="<?php bloginfo('name'); ?>"><?php bloginfo('name'); ?></a></h2><?php }else{ ?><h1><a href="<?php bloginfo('url'); ?>" title="<?php bloginfo('name'); ?>"><?php bloginfo('name'); ?></a></h1><?php } ?><?php bloginfo('description'); ?>
<?php } else {?>
<img src="<?php bloginfo('template_url'); ?>/images/logo.jpg" alt="<?php bloginfo('name'); ?>" /></a>
<?php } ?>
</div><?php get_search_form(); ?><div class="baiduBtn" id="baiduBtn"></div>
</div>
<div id="menu">
<?php wp_nav_menu(array('theme_location' => 'nav', 'depth' => 2, 'container' => 'ul', 'menu_id' => "menunav", 'menu_class' => 'menunav',)); ?>

</div>

<div id="container-inner" class="clear">
   
    
<div id="friendLinks-template" style="display:none;">
    <div class="links-type">图站</div>
    <a class="links-item" target="_blank" href="http://www.furaffinity.net/" alt="欧美Furry绘师站">FA</a>
    <a class="links-item" target="_blank" href="http://www.pixiv.net/" alt="日本画师站点">PIXIV</a>
    <a class="links-item" target="_blank" href="https://inkbunny.net/" alt="英文兽站">inkbunny</a>
    <a class="links-item" target="_blank" href="http://www.sofurry.com/" alt="英文兽站">SOFURRY</a>
    <a class="links-item" target="_blank" href="http://www.deviantart.com/" alt="英文兽站">deviantart</a>
    <div class="links-type">论坛</div>
    <a class="links-item" target="_blank" href="http://chunghwa-furry.deviantart.com/" alt="中华兽群">中华兽群</a>
    <a class="links-item" target="_blank" href="http://yinglong.org/forum/" alt="中文龙类主题站">鳞目界域</a>
    <a class="links-item" target="_blank" href="http://www.boku-star.net/bbs/portal.php" alt="正太少年控聚集地">少年唐</a>
    <a class="links-item" target="_blank" href="http://www.hu67.com/" alt="幻想生物 中文兽站">虎67</a>
    <a class="links-item" target="_blank" href="http://www.cnfurry.com/forum.php" alt="中文兽站">兽向中文网</a>
    <a class="links-item" target="_blank" href="http://wolfbbs.net/forum.php" alt="中文兽站之一">狼之乐园</a>
    <a class="links-item" target="_blank" href="http://www.dnaxcat.net/" alt="萌系兽站">九藏喵窝</a>
    <a class="links-item" target="_blank" href="http://www.furagon.com/" alt="中文龙类主题站">龙兽帝国</a>
    <a class="links-item" target="_blank" href="http://www.fursarcar.com/" alt="中文兽站">兽人世家</a>
    <a class="links-item" target="_blank" href="http://www.wilddream.net/" alt="中文兽站">WildDream</a>
    <a class="links-item" target="_blank" href="http://blog.yam.com/msg/sto751025#yamBox_REG_END">BL兽兽区</a>
    <div class="links-type">其它</div>
    <a class="links-item" target="_blank" href="https://www.tumblr.com/">汤不热</a>
    <a class="links-item" target="_blank" href="http://www.twitter.com " >推特</a>
    <a class="links-item" target="_blank" href="http://www.plurk.com/top/">噗浪</a>
    <a class="links-item" target="_blank" href="http://weibo.com/">新浪</a>
</div>