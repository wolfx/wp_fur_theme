<?php get_header(); ?>
<div id="primary">
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
<h1 class="sub"><?php the_title(); ?></h1>
<div class="postinfo"><div class="left">分类：<?php the_category(', ') ?> | 发表于 <?php the_time('Y年n月j日 l') ?> <?php the_time() ?></div><div class="right"><a href="#respond" title="发表评论"><?php comments_number('发表评论', '1 条评论', '% 条评论'); ?></a></div><div class="clear"></div></div>

<div id="content">
<?php the_content();?>
<?php wp_link_pages(array('before' => '<div class="clear"></div><p><strong>'.PAGES.':</strong> ', 'after' => '</p>', 'next_or_number' => 'number')); ?>
</div>

<?php if(get_option('T_share') == "On") {?>
<div class="bdsharebuttonbox"><a href="#" class="bds_more" data-cmd="more"></a><a href="#" class="bds_qzone" data-cmd="qzone" title="分享到QQ空间"></a><a href="#" class="bds_tsina" data-cmd="tsina" title="分享到新浪微博"></a><a href="#" class="bds_tqq" data-cmd="tqq" title="分享到腾讯微博"></a><a href="#" class="bds_renren" data-cmd="renren" title="分享到人人网"></a><a href="#" class="bds_weixin" data-cmd="weixin" title="分享到微信"></a><a href="#" class="bds_douban" data-cmd="douban" title="分享到豆瓣网"></a></div>
<?php } ?>
<div class="postmetadata">
本文来自：<a href="<?php bloginfo('url'); ?>" target="_blank" title="<?php bloginfo('name'); ?> - <?php bloginfo('description'); ?>"><strong><?php bloginfo('name'); ?></strong></a>，永久链接：<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title(); ?>" class="permalink"><strong><?php the_permalink() ?></strong></a><br />
<?php if(function_exists('wp_print')) { print '<span>'; print_link(); print '</span> | '; } ?> <span><?php the_tags( ' 标签：', ', ', ' ' ) ?><?php if(function_exists('the_views')) { " | " .the_views(); } ?></span>
</div>
<div class="clear"></div><br />

<?php $related = get_option('T_related'); if($related == "On" || $related == "") { ?>
<div class="postmetadata">
<h3 class="sub" style="padding:0 0 8px; margin:0 0 12px;">相关文章</h3>
<ul class="related">
<?php
$post_num = 6;
global $post;
$tmp_post = $post;
$tags = ''; $i = 0;
$exclude_id = $post->ID;
$posttags = get_the_tags();
if ( $posttags ) {
  foreach ( $posttags as $tag ) $tags .= $tag->name . ',';
  $tags = strtr(rtrim($tags, ','), ' ', '-');
  $myposts = get_posts('numberposts='.$post_num.'&tag='.$tags.'&exclude='.$exclude_id);
  foreach($myposts as $post) {
setup_postdata($post);
?>
<li><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a> (<?php comments_number('0', '1', '%', ''); ?>)</li>
<?php
$exclude_id .= ','.$post->ID; $i ++;
}
}
if ( $i < $post_num ) {
  $post = $tmp_post; setup_postdata($post);
  $cats = ''; $post_num -= $i;
  foreach ( get_the_category() as $cat ) $cats .= $cat->cat_ID . ',';
  $cats = strtr(rtrim($cats, ','), ' ', '-');
  $myposts = get_posts('numberposts='.$post_num.'&category='.$cats.'&exclude='.$exclude_id);
  foreach($myposts as $post) {
setup_postdata($post);
?>
<li><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a> (<?php comments_number('0', '1', '%', ''); ?>)</li>
<?php
}
}
$post = $tmp_post; setup_postdata($post);
?>
</ul>
<div class="clear"></div>
</div><?php } ?>

<?php comments_template('', true); ?>
<?php endwhile; endif; ?>

</div>
<?php get_sidebar(); ?>
<?php get_footer(); ?>